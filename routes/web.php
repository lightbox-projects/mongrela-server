<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::GET('/', function(){
        return redirect('/approval-trainer');
    })->name('index');

// Route::group(['middleware' => ['web']], function () { 
//     Route::GET('login', 'FE\LoginController@login')->name('login'); 
//     Route::GET('register', 'FE\LoginController@register'); 

//     Route::GET('companies', 'FE\CompanyController@index'); 
//     Route::GET('companies/{id}', 'FE\CompanyController@detail'); 

//     Route::GET('tenders', 'FE\TenderController@index'); 
//     Route::GET('tenders/{id}', 'FE\TenderController@detail'); 
//     Route::GET('tenders/{id}/bid/{pr_id?}', 'FE\BiddingController@index'); 

//     Route::get('bid/{id}/{uid}/{pr_id?}', 'FE\BiddingController@viewBid');

//     Route::GET('testimony', 'FE\TestimonyController@index'); 
//     Route::GET('category', 'FE\CategoryController@index'); 
//     Route::GET('product-category', 'FE\ProductCategoryController@index'); 
    
//     Route::GET('pricing', 'FE\PricingController@index');

//     Route::GET('news', 'FE\NewsController@index'); 
//     Route::GET('news/list', 'FE\NewsController@list'); 
//     Route::GET('news/detail/{id}', 'FE\NewsController@detail'); 

//     Route::GET('about-us', 'FE\OthersControllers@about');
//     Route::GET('faq', 'FE\OthersControllers@faq');
//     Route::GET('faq/list', 'FE\FaqController@index');

// });

// Route::group(['middleware' => ['web', 'oauth']], function () { 
//     Route::GET('users', 'FE\UserController@index'); 
//     Route::GET('users/profile/{id?}', 'FE\UserController@profile'); 
//     Route::GET('users/profile/{id}/company', 'FE\UserController@company'); 
    
//     Route::prefix('masterdata')->group(function () {
//         Route::prefix('products')->group(function () {
//             Route::get('/new', 'FE\Products@new');
//             Route::get('/', 'FE\Products@index');
//             Route::get('/{id}/form', 'FE\Products@form');
//         });
//     });
    
//     Route::prefix('tender-management')->group(function () {
//         Route::get('/new', 'FE\TenderManagement@new');
//         Route::get('/', 'FE\TenderManagement@index');
//         Route::get('/{id}/form', 'FE\TenderManagement@form');
//         Route::get('/{id}/form/products', 'FE\TenderManagement@products');
//         Route::get('/{id}/form/publish', 'FE\TenderManagement@publish');
//         Route::get('/{id}/form/bids', 'FE\TenderManagement@bids');
//         Route::get('/{id}/form/evaluation/{pr_id?}', 'FE\TenderManagement@evaluation');
//         Route::get('/{id}/form/final', 'FE\TenderManagement@final');
//     });
    
//     Route::GET('bids', 'FE\BidsController@index'); 
// });

// Route::prefix('')->group(function () {
    Route::get('/approval-trainer', 'Mongrela\ApprovalController@trainer');
    Route::get('/approval-shelter', 'Mongrela\ApprovalController@shelter');
    Route::get('/approval-store', 'Mongrela\ApprovalController@store');
    Route::get('/banners/page', 'Mongrela\BannerControllerAPI@banner');  
    Route::get('/ads/page', 'Mongrela\BannerControllerAPI@ads');  

    Route::GET('getimage/{file}', function ($file) {
        $f = urldecode(base64_decode($file));
        $headers = [
            'Content-Type'        => 'image/jpeg',
            'Content-Disposition' => 'attachment; filename="image"',
        ];
    
        return \Response::make(Storage::disk('local')->get($f), 200, $headers);
    });
// }); 
