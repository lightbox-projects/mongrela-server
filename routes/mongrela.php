<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('mongrela')->group(function () {
    // Route::get('/approval-trainer', 'Mongrela\ApprovalController@trainer');
    Route::post('/approval-trainer', 'Mongrela\ApprovalController@trainer_approval');
    Route::post('/approval-trainer/dt', 'Mongrela\ApprovalController@trainer_dt');
    
    // Route::get('/approval-shelter', 'Mongrela\ApprovalController@shelter');
    Route::post('/approval-shelter', 'Mongrela\ApprovalController@shelter_approval');
    Route::post('/approval-shelter/dt', 'Mongrela\ApprovalController@shelter_dt');
    
    // Route::get('/approval-store', 'Mongrela\ApprovalController@store');
    Route::post('/approval-store', 'Mongrela\ApprovalController@store_approval');
    Route::post('/approval-store/dt', 'Mongrela\ApprovalController@store_dt');

    // Route::get('/banners/page', 'Mongrela\BannerControllerAPI@banner');  
    Route::post('/banners/dt', 'Mongrela\BannerControllerAPI@banner_dt');  
    Route::post('/banners/save', 'Mongrela\BannerControllerAPI@banner_save');  
    Route::get('/banners', 'Mongrela\BannerControllerAPI@get_banners');  
    Route::post('/banners', 'Mongrela\BannerControllerAPI@banner_active');  
    Route::delete('/banners/{id}', 'Mongrela\BannerControllerAPI@banner_delete');  
    
    // Route::get('/ads/page', 'Mongrela\BannerControllerAPI@ads');  
    Route::get('/ads', 'Mongrela\BannerControllerAPI@get_ads');
    Route::post('/ads/dt', 'Mongrela\BannerControllerAPI@ads_dt');  
    Route::post('/ads/save', 'Mongrela\BannerControllerAPI@ads_save');   
    Route::post('/ads', 'Mongrela\BannerControllerAPI@ads_active');  
    Route::delete('/ads/{id}', 'Mongrela\BannerControllerAPI@ads_delete');   

    Route::prefix('users')->group(function () {
        Route::POST('/login', 'Mongrela\UserControllerAPI@login');  
        Route::POST('/', 'Mongrela\UserControllerAPI@save');  
        Route::GET('/{id}', 'Mongrela\UserControllerAPI@getById');  
        Route::DELETE('/{id}', 'Mongrela\UserControllerAPI@delete');  

        Route::POST('/register-as-trainer', 'Mongrela\UserControllerAPI@registerAsTrainer');  
        Route::POST('/trainer-list', 'Mongrela\UserControllerAPI@trainerList');  
        Route::GET('/trainer/{id}', 'Mongrela\UserControllerAPI@getTrainer');  
    });

    Route::prefix('pets')->group(function () {
        Route::POST('/list', 'Mongrela\PetControllerAPI@getPetList');  
        Route::POST('/', 'Mongrela\PetControllerAPI@save');  
        Route::GET('/{id}', 'Mongrela\PetControllerAPI@getById');  
        Route::DELETE('/{id}', 'Mongrela\PetControllerAPI@delete');  
    });

    Route::prefix('products')->group(function () {
        Route::POST('/list', 'Mongrela\ProductControllerAPI@list');  
        Route::POST('/', 'Mongrela\ProductControllerAPI@save');  
        Route::GET('/{id}', 'Mongrela\ProductControllerAPI@getById');  
        Route::DELETE('/{id}', 'Mongrela\ProductControllerAPI@delete');  
    });

    Route::prefix('chats')->group(function () {
        Route::POST('/', 'Mongrela\ChatControllerAPI@save');  
        Route::POST('/list', 'Mongrela\ChatControllerAPI@getChatList');   
        Route::POST('/room', 'Mongrela\ChatControllerAPI@getChatRoom');   
    });

    Route::POST('/my-activities', 'Mongrela\ActivitiesControllerAPI@get_my_activities'); 
    Route::POST('/pet-activities', 'Mongrela\ActivitiesControllerAPI@get_pet_activities'); 

    Route::prefix('training')->group(function () {
        Route::POST('/list', 'Mongrela\TrainingControllerAPI@list'); 
        Route::POST('/', 'Mongrela\TrainingControllerAPI@save');  
        Route::GET('/{id}', 'Mongrela\TrainingControllerAPI@getById');  
        Route::DELETE('/{id}', 'Mongrela\TrainingControllerAPI@delete');  
        
        Route::GET('/schedule/{id}', 'Mongrela\TrainingControllerAPI@getSchedule');  
        Route::POST('/schedule', 'Mongrela\TrainingControllerAPI@saveSchedule');  
        Route::DELETE('/schedule/{id}', 'Mongrela\TrainingControllerAPI@deleteSchedule');  

        Route::GET('/book/invoice/{id}', 'Mongrela\TrainingControllerAPI@getInvoice');  
        Route::POST('/book', 'Mongrela\TrainingControllerAPI@bookTraining');  
    });

    Route::prefix('walker')->group(function () {
        Route::POST('/', 'Mongrela\WalkerControllerAPI@save_walker');  
        Route::GET('/', 'Mongrela\WalkerControllerAPI@list_pets');  
        Route::GET('/{id}', 'Mongrela\WalkerControllerAPI@get_walker');  

        Route::GET('/note/{id}', 'Mongrela\WalkerControllerAPI@list_note');   
        Route::POST('/note', 'Mongrela\WalkerControllerAPI@save_note');   
        Route::DELETE('/note/{id}', 'Mongrela\WalkerControllerAPI@delete_note');

        Route::GET('/schedule/{id}', 'Mongrela\WalkerControllerAPI@get_schedule');   
        Route::POST('/schedule', 'Mongrela\WalkerControllerAPI@save_schedule');   
        Route::DELETE('/schedule/{id}', 'Mongrela\WalkerControllerAPI@delete_schedule');

        Route::GET('/book/{id}', 'Mongrela\WalkerControllerAPI@get_invoice');  
        Route::POST('/book', 'Mongrela\WalkerControllerAPI@book_walker');  
    });

    Route::prefix('store')->group(function () {
        Route::POST('/list', 'Mongrela\StoreControllerAPI@list');  
        Route::POST('/', 'Mongrela\StoreControllerAPI@save');  
        Route::GET('/{id}', 'Mongrela\StoreControllerAPI@getById');  
        Route::DELETE('/{id}', 'Mongrela\StoreControllerAPI@delete'); 
    });

    Route::prefix('shelter')->group(function () {
        Route::POST('/list', 'Mongrela\Shelter\ShelterControllerAPI@list');  
        Route::POST('/', 'Mongrela\Shelter\ShelterControllerAPI@save');  
        Route::GET('/{id}', 'Mongrela\Shelter\ShelterControllerAPI@getById');  
        Route::DELETE('/{id}', 'Mongrela\Shelter\ShelterControllerAPI@delete');  
        
        Route::prefix('gallery')->group(function () {
            Route::POST('/list', 'Mongrela\Shelter\GalleryControllerAPI@list');  
            Route::POST('/', 'Mongrela\Shelter\GalleryControllerAPI@save');  
            Route::GET('/{id}', 'Mongrela\Shelter\GalleryControllerAPI@getById');  
            Route::DELETE('/{id}', 'Mongrela\Shelter\GalleryControllerAPI@delete');  
        });
        
        Route::prefix('adoption')->group(function () {
            Route::POST('request-adoption', 'Mongrela\Shelter\AdoptionControllerAPI@request_adoption');  
            Route::POST('online-parent', 'Mongrela\Shelter\AdoptionControllerAPI@apply_online_parent');  

            Route::POST('/list', 'Mongrela\Shelter\AdoptionControllerAPI@list');  
            Route::POST('/', 'Mongrela\Shelter\AdoptionControllerAPI@save');  
            Route::GET('/{id}', 'Mongrela\Shelter\AdoptionControllerAPI@getById');  
            Route::DELETE('/{id}', 'Mongrela\Shelter\AdoptionControllerAPI@delete');  
        });
        
        Route::prefix('donation')->group(function () {
            Route::POST('apply', 'Mongrela\Shelter\DonationControllerAPI@apply_donation');  

            Route::POST('/list', 'Mongrela\Shelter\DonationControllerAPI@list');  
            Route::POST('/', 'Mongrela\Shelter\DonationControllerAPI@save');  
            Route::GET('/{id}', 'Mongrela\Shelter\DonationControllerAPI@getById');  
            Route::DELETE('/{id}', 'Mongrela\Shelter\DonationControllerAPI@delete');  
        });
        
        Route::prefix('volunteer')->group(function () {
            Route::POST('apply', 'Mongrela\Shelter\VolunteerControllerAPI@apply_volunteer');  

            Route::POST('/list', 'Mongrela\Shelter\VolunteerControllerAPI@list');  
            Route::POST('/', 'Mongrela\Shelter\VolunteerControllerAPI@save');  
            Route::GET('/{id}', 'Mongrela\Shelter\VolunteerControllerAPI@getById');  
            Route::DELETE('/{id}', 'Mongrela\Shelter\VolunteerControllerAPI@delete');  
        });
    }); 


    Route::GET('getimage/{file}', function ($file) {
        $f = urldecode(base64_decode($file));
        $headers = [
            'Content-Type'        => 'image/jpeg',
            'Content-Disposition' => 'attachment; filename="image"',
        ];
    
        return \Response::make(Storage::disk('local')->get($f), 200, $headers);
    });
});