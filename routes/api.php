<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BE\LoginControllerAPI;

Route::group(['middleware' => ['web']], function () { 
    Route::POST('/register', 'BE\LoginController@register');
    Route::POST('/login', 'BE\LoginController@exe')->name('loginExe');
    Route::GET('/logout', 'BE\LoginController@logout')->name('logout');
    
    Route::prefix('users')->group(function () {
        Route::POST('/dt', 'BE\UserControllerAPI@dt');
        Route::GET('/{id}', 'BE\UserControllerAPI@getById');
        Route::POST('/', 'BE\UserControllerAPI@save');
        Route::DELETE('/{id}', 'BE\UserControllerAPI@delete');

        Route::GET('/company/{id}', 'BE\UserControllerAPI@getByIdCompany');
        Route::POST('/company', 'BE\UserControllerAPI@saveCompany');
    });

    Route::prefix('news')->group(function () {
        Route::POST('/dt', 'BE\NewsControllerAPI@dt');
        Route::GET('/{id}', 'BE\NewsControllerAPI@getById');
        Route::POST('/', 'BE\NewsControllerAPI@save');
        Route::DELETE('/{id}', 'BE\NewsControllerAPI@delete');
    });

    Route::post('companies', 'FE\CompanyController@getList');
    Route::post('biddings', 'FE\BiddingController@saveF');
    Route::post('biddings/{id}', 'FE\BiddingController@submitBid');
    Route::get('biddings-original/{id}/{pr_id}', 'FE\BiddingController@getDataOriginal');
    Route::get('biddings/{id}/{pr_id}', 'FE\BiddingController@getData');

    Route::prefix('testimony')->group(function () {
        Route::POST('/dt', 'BE\TestimonyControllerAPI@dt');
        Route::GET('/{id}', 'BE\TestimonyControllerAPI@getById');
        Route::POST('/', 'BE\TestimonyControllerAPI@save');
        Route::DELETE('/{id}', 'BE\TestimonyControllerAPI@delete');
    });

    Route::prefix('category')->group(function () {
        Route::POST('/dt', 'BE\CategoryControllerAPI@dt');
        Route::GET('/{id}', 'BE\CategoryControllerAPI@getById');
        Route::POST('/', 'BE\CategoryControllerAPI@save');
        Route::DELETE('/{id}', 'BE\CategoryControllerAPI@delete');
    });

    Route::prefix('product-category')->group(function () {
        Route::POST('/dt', 'BE\ProductCategoryControllerAPI@dt');
        Route::GET('/{id}', 'BE\ProductCategoryControllerAPI@getById');
        Route::POST('/', 'BE\ProductCategoryControllerAPI@save');
        Route::DELETE('/{id}', 'BE\ProductCategoryControllerAPI@delete');
    });

    Route::prefix('tags')->group(function () {
        Route::POST('/dt', 'BE\TagsControllerAPI@dt');
        Route::GET('/{id}', 'BE\TagsControllerAPI@getById');
        Route::POST('/', 'BE\TagsControllerAPI@save');
        Route::DELETE('/{id}', 'BE\TagsControllerAPI@delete');
    });

    Route::prefix('faq')->group(function () {
        Route::POST('/dt', 'BE\FaqControllerAPI@dt');
        Route::GET('/{id}', 'BE\FaqControllerAPI@getById');
        Route::POST('/', 'BE\FaqControllerAPI@save');
        Route::DELETE('/{id}', 'BE\FaqControllerAPI@delete');
    });
    
    Route::prefix('masterdata')->group(function () {
        Route::prefix('products')->group(function () {
            Route::POST('/list', 'BE\ProductsAPI@list');
            Route::GET('/{id}', 'BE\ProductsAPI@getById');
            Route::DELETE('/{id}', 'BE\ProductsAPI@delF');
            Route::POST('/upload_image', 'BE\ProductsAPI@uploadImage');
            Route::POST('/', 'BE\ProductsAPI@saveF');
        });
    });

    Route::prefix('tender-management')->group(function () {
        Route::DELETE('/product/del/{id}', 'BE\TenderAPI@delFProdukOffered');
        Route::POST('/product/upd', 'BE\TenderAPI@saveFProdukOffered');
        Route::POST('/product', 'BE\TenderAPI@assignProduct');
        Route::POST('/list', 'BE\TenderAPI@list');
        Route::GET('/{id}', 'BE\TenderAPI@getById');
        Route::DELETE('/{id}', 'BE\TenderAPI@delF');
        Route::POST('/', 'BE\TenderAPI@saveF');
        Route::POST('/evaluate-bids', 'BE\TenderAPI@evaluateBids');
    });
});