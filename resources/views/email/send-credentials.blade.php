<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
    <div style="margin:50px auto;width:70%;padding:20px 0">
        <div style="border-bottom:1px solid #eee">
            <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Lightbox</a>
        </div>
        <p style="font-size:1.1em">Hi,</p>
        <p>
            {{-- This will be credentials that can be used to access the
            <a href="{{$url}}" style="text-decoration: underline;color:#00466A">following form</a>
            on Lightbox Application. --}}
            Hi please use this password to enter the tender platform <strong>{{$data->pst_security_email}}</strong> : <strong>{{$data->pst_security_passphrase}}</strong>
            on <a href="{{$url}}" style="text-decoration: underline;color:#00466A">following form</a>
        </p>
        {{-- <h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">
            {{$data->pst_security_passphrase}}
        </h2> --}}
        <p style="font-size:0.9em;">Regards,<br />Lightbox</p>
        <hr style="border:none;border-top:1px solid #eee" />
        <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
            <p>Lightbox</p>
            <p>Kewalram House, 8 Jln Kilang Timor, Singapore 159305</p>
            <p>Singapore</p>
        </div>
    </div>
</div>
