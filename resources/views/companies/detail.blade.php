@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    Companies Detail
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3">

        <div class="d-flex flex-wrap align-items-center justify-content-between mb-1" style="gap:10px">
            <a href="{{ url('companies') }}" class="btn btn-outline-primary" style="border-width:2px !important">
                Back to list
            </a>
            @if($next)
            <a href="{{ url('companies/'.$next) }}" class="btn btn-outline-primary" style="border-width:2px !important">
                Next Company
            </a>
            @endif
        </div>

        <div class="px-3 pb-3 pt-3 border-primary">

            <div class="py-2">
                <div class="d-flex">
                    <img src="{{ asset('lightbox.png') }}" class="w-100 mx-auto"
                        style="max-height:300px;max-width:300px;object-fit:contain" />
                </div>
                <h6 class="fw-bolder mb-0 text-secondary text-center mt-3 h1">{{ $company->company_name }}</h6>
                <h6 class="fw-bolder mb-1 text-secondary text-center text-center" style="font-size:13px">
                    {{ $company->company_slogan }}</h6>

                <h6 style="" class="text-secondary my-3">
                    {{ $company->company_description }}
                </h6>

                <div class="">
                    <table class="text-secondary fw-bolder">
                        <tr>
                            <td>Speciality</td>
                            <td style="width:50px" class="text-center">:</td>
                            <td>{{ $company->company_bussiness_category }}</td>
                        </tr>
                        <tr>
                            <td style="padding:.25em 0;">Address</td>
                            <td style="padding:.25em 0;" class="text-center">:</td>
                            <td style="padding:.25em 0;">{{ $company->company_city }}. {{ $company->company_state }}.
                                {{ $company->company_country }}. {{ $company->company_zipcode }}.</td>
                        </tr>
                        <tr>
                            <td>Contact Person</td>
                            <td class="text-center">:</td>
                            <td>{{ $company->company_phone }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <h1 class="fw-bolder text-secondary h2">Active Tenders</h1>
            @include('companies.tenders')

        </div>

        @include('home.post-a-project')
    </div> 
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
    </script>
@endsection
