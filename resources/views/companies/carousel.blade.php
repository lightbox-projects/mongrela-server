<div class="">
    <div class="">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                @foreach ($banner as $key => $item)
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{$key}}" class="{{$key == 0 ? 'active' : ''}}"
                    aria-current="true" aria-label="Slide 1"></button> 
                @endforeach
            </div>
            <div class="carousel-inner">
                @foreach ($banner as $key => $item)
                    <a href="{{url('/news/detail/'.$item->news_id)}}" class="carousel-item {{$key == 0 ? 'active' : ''}}" style="">
                        <img src="{{url('getimage/'.base64_encode($item->news_photo))}}" class="d-block w-100 rounded" style="height:400px;object-fit:cover"
                            alt="Image slide">
                    </a> 
                @endforeach
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                data-bs-slide="prev">
                <div class="bg-white rounded-circle d-flex btn-icon text-dark" style="height:30px;width:30px">
                    <i class="bx bx-chevron-left m-auto" style="font-size:20px"></i>
                </div>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                data-bs-slide="next">
                <div class="bg-white rounded-circle d-flex btn-icon text-dark" style="height:30px;width:30px">
                    <i class="bx bx-chevron-right m-auto" style="font-size:20px"></i>
                </div>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
</div>
