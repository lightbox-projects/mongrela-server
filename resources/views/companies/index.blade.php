@extends('layouts.layout1')

@section('css_section')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.5/pagination.css">
    <style>
        /* .select2 {
                border: 1px solid #F26122 !important;
                border-radius: 0 !important
            }  */

        .select2-container--classic .select2-selection--single,
        .select2-container--default .select2-selection--single {
            /* border: 1px solid #F26122; */
            border-top: 1px solid #F26122;
            border-bottom: 1px solid #F26122;
            border-left: 1px solid #F26122;
            border-radius: 0 !important;
        }

        .select2,
        .select2-selection__rendered {
            color: #F26122 !important;
        }
    </style>
    <style>
        .card-category {
            /* height: 100%; width: 100%; */
            /* display: flex; padding: 3em; */
            padding: 1.5em 1.5em;
            border: 2px solid #F26122
        }

        .card-category:hover {
            cursor: pointer;
            background: #D0CFD4;
            color: white !important
        }
    </style>
    <style>
        .paginationjs .paginationjs-pages li>a {
            border-radius: 5px !important;
            margin: 0 5px;
            height: 38px !important;
            width: 38px !important;
            display: flex;
            align-items: center;
            justify-content: center;
            font-weight: 700;
            font-size: 18px
        }

        .paginationjs .paginationjs-pages li {
            border: unset !important
        }

        .paginationjs .paginationjs-pages li.active>a {
            border: 1px solid #F26122 !important;
            background: unset !important;
            color: #F26122 !important
        }
    </style>

@endsection

@section('page_title')
    Browse Companies
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3">
        @include('companies.carousel')

        <hr class="my-2" />

        <div class="d-flex flex-wrap align-items-center my-2" style="gap:10px">
            <div class="text-primary fw-bolder h3 me-auto mb-0">
                Company List
            </div>

            {{-- <form action="{{url('companies')}}" method="GET">
                @csrf
                <div class="d-flex w-100" style="max-width:700px">
                    <div class="flex-fill">
                        <select class="select2" 
                            name="speciality" id="speciality-filter" 
                            @if(isset($request['speciality'])) 
                                value="{{$request['speciality']}}" 
                            @endif
                        >
                            <option value="">Speciality</option>
                        </select>
                    </div>
                    <div class="flex-fill">
                        <select class="select2 middle"
                            name="location" id="location-filter" 
                            @if(isset($request['location'])) 
                                value="{{$request['location']}}" 
                            @endif
                        >
                            <option value="">Location</option>
                        </select>
                    </div>
                    <div class="flex-fill">
                        <input class="form-control" placeholder="Tags"
                            name="tags" id="tags-filter" 
                            @if(isset($request['tags'])) 
                                value="{{$request['tags']}}" 
                            @endif
                            style="
                                                border:1px solid #F26122;
                                                border-right:unset !important;
                                                color:#F26122 !important;
                                                border-radius:unset !important
                                                " />
                    </div>
                    <button class="btn btn-outline-primary" style="border-radius: unset !important;border-size:2px">
                        Search Companies
                    </button>
                </div>
            </form> --}}
        </div>

        <hr class="my-2" />

        <div class="row pt-2" style="gap:20px 0" id="data-container"> 
        </div>

        <div class="d-flex my-3">
            <div class="mx-auto" id="pagination-container"></div>
        </div>

        @include('home.testimony')
        @include('home.post-a-project')
    </div> 

    <input type="hidden" id="total_active" value="{{$total_active}}" />

@endsection

@section('js_section')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.5/pagination.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment-with-locales.min.js" integrity="sha512-42PE0rd+wZ2hNXftlM78BSehIGzezNeQuzihiBCvUEB3CVxHvsShF86wBWwQORNxNINlBPuq7rG4WWhNiTVHFg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        var select = $('.select2')

        $('#pagination-container').pagination({
            dataSource: Array(parseInt($('#total_active').val())).fill(0).map((_, i) => i * i),
            pageSize: 9,
            callback: function(data, pagination) {
                $('#data-container').html('');
                let page = pagination.pageNumber
                let pageRange = pagination.pageSize

                let formData = new FormData()
                formData.append('page', page)
                formData.append('pageRange', pageRange)
                formData.append('offsetLatest', true) 
                formData.append('speciality', $('#speciality-filter').val())
                formData.append('location', $('#location-filter').val())
                formData.append('tags', $('#tags-filter').val())

                $.ajax({
                    url: '{{ url('api/companies') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        let data = e
                        data.map(item => {
                            let start = moment(item.prj_tender_start)
                            let end = moment(item.prj_tender_end)

                            let status = 'upcoming'
                            if(moment().diff(end, 'days') > 0) {
                                status = 'expired'
                            } else if(moment().diff(start, 'days') > 0) {
                                status = 'ongoing'
                            }

                            var html = `
                                <div class="col-lg-4">
                                    <a href="{{ url('companies/') }}/${item.company_id}">
                                        <div class="card-category" style="">
                                            <div class="">
                                                <div class="d-flex">
                                                    <img src="{{ url('getimage/') }}/${btoa(item.company_photo)}" class="w-100 mx-auto"
                                                        style="max-height:100px;max-width:100px;object-fit:contain" />
                                                </div>
                                                <h6 class="fw-bolder mb-0 text-secondary text-center mt-1 h4">${item.company_name}</h6>
                                                <h6 class="mb-1 text-secondary text-center text-center" style="font-size:13px">
                                                    ${item.company_slogan}</h6>

                                                <div class="">
                                                    <table class="text-secondary fw-bolder">
                                                        <tr>
                                                            <td>Speciality</td>
                                                            <td style="width:50px" class="text-center">:</td>
                                                            <td>${item.company_bussiness_category}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:.25em 0;">Location</td>
                                                            <td style="padding:.25em 0;" class="text-center">:</td>
                                                            <td style="padding:.25em 0;">${item.company_city},
                                                                ${item.company_country}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Contact Person</td>
                                                            <td class="text-center">:</td>
                                                            <td>${item.company_phone}</td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <h6 style="
                                                                    display: -webkit-box;
                                                                    -webkit-line-clamp: 4;
                                                                    -webkit-box-orient: vertical;
                                                                    overflow: hidden;
                                                                "
                                                    class="text-secondary mt-1">
                                                    ${item.company_description}
                                                </h6>

                                                <div class="d-flex flex-wrap mt-2 align-items-center text-secondary" style="gap:10px">
                                                    <div class="me-auto fw-bolder">
                                                        Tags : ${item.company_features}
                                                    </div>
                                                    <button class="btn btn-sm btn-icon btn-outline-primary rounded-circle">
                                                        <i class="bx bx-chevron-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </a> 
                                </div>
                            `;
                            $('#data-container').append(html);
                        })
                    }
                });

            }
        })
    </script>
@endsection
