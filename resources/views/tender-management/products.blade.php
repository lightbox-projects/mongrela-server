@extends('layouts.layout1')

@section('css_section')
    <style>
        .preview {
            overflow: hidden;
            width: 200px;
            height: 200px;
            border: 1px solid red;
        }
    </style>
    <style>
        input, textarea, select {
            border: unset !important;
            border-bottom: 1px solid #d8d6de !important;
            border-radius: unset !important
        }
        input[type=checkbox] {
            border: 1px solid #d8d6de !important;
        }
    </style> 
@endsection

@section('page_title')
    Tender Form
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    @include('tender-management.header')

    <div class="pb-3" style="min-height: 60vh">

        <div class=""> 

            <div style="gap:10px" class="d-flex align-items-center flex-wrap">
                <h4 class="mb-0 me-auto fw-bolder">Tender Products ({{count($products)}} Items)</h4>
                <button class="btn btn-secondary" onclick="searchProducts()">
                    Assign Product
                </button>
                <a class="btn btn-primary" href="{{url('tender-management/'.$id.'/form/publish')}}">
                    <i class="bx bx-save"></i> Save Form & Continue
                </a>
            </div>

            <div class="mt-2">
                <div class="accordion accordion-margin" id="accordionMargin" data-toggle-hover="true"> 
                    @foreach ($products as $item)
                        @php
                            $item = $item->product;
                        @endphp
                        <div class="accordion-item mb-1 shadow-lg">
                            <h2 class="accordion-header" id="heading{{$item->pr_id}}">
                                <button class="accordion-button px-3" type="button" data-bs-toggle="collapse" data-bs-target="#accordion{{$item->pr_id}}">
                                    <div class="d-flex align-items-center flex-wrap w-100" style="gap:20px">
                                        @php 
                                            $url = url('getimage/'.base64_encode($item->pr_main_photo));
                                            $is_owner = session()->get('userId') == $item->original->owner->user_id;
                                        @endphp
                                        <img src="{{$url}}" class="rounded" style="height:70px;width:70px" />
                                        <div>
                                            <a class="text-primary fw-bolder"><strong>
                                                {{$item->pr_name}}
                                                @if(!$is_owner)
                                                    <div class="badge bg-primary">
                                                        {{$item->original->owner->user_name}} is the Owner
                                                    </div>
                                                @endif
                                            </strong></a>
                                            <div>{{$item->pr_model}}</div>
                                        </div> 
                                        <div class="ms-auto" style="
                                            max-width: 400px;
                                            display: -webkit-box;
                                            -webkit-line-clamp: 2;
                                            -webkit-box-orient: vertical;
                                            overflow: hidden;
                                        ">
                                            <div>{{$item->pr_note}}.</div> 
                                        </div>
                                    </div>
                                </button>
                            </h2>
                
                            <div id="accordion{{$item->pr_id}}" class="accordion-collapse collapse" aria-labelledby="heading{{$item->pr_id}}">
                                <div class="accordion-body px-2">
                                    <form id="frm-produk-{{$item->pr_id}}" class="mt-2">
                                        @csrf
                                        <input type="hidden" id="pr_id" name="id" value="{{$item->pr_id}}" />
                                        <div class="row" style="gap: 20px 0">
                                            <div class="col-lg-3">
                                                <label class="form-label">Product Name</label>
                                                <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_name]"
                                                    id="pr_name" value="{{$item->pr_name}}">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="form-label">Product Application</label>
                                                <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_application]"
                                                    id="pr_application" value="{{$item->pr_application}}">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="form-label">Product Brand</label>
                                                <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_brand]"
                                                    id="pr_brand" value="{{$item->pr_brand}}">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="form-label">Product Model</label>
                                                <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_model]"
                                                    id="pr_model" value="{{$item->pr_model}}">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="form-label">Product Accessories</label>
                                                <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_accessories]"
                                                    id="pr_accessories" value="{{$item->pr_accessories}}">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="form-label">Product Materials</label>
                                                <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_materials]"
                                                    id="pr_materials" value="{{$item->pr_materials}}">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="form-label">Product Finishing</label>
                                                <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_finishing]"
                                                    id="pr_finishing" value="{{$item->pr_finishing}}">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="form-label">Product Dimension</label>
                                                <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_dimension]"
                                                    id="pr_dimension" value="{{$item->pr_dimension}}">
                                            </div> 
                                            <div class="col-lg-3">
                                                <label class="form-label">Main Photo</label>
                                                <input type="file" class="form-control" name="pr_main_photo" accept=".png, .jpg">
                                                @if($item->pr_main_photo)
                                                <a href="{{$url}}" target="_blank" class="form-text text-success" id="pr_main_photo">Already uploaded.</a>
                                                @endif
                                            </div>
                        
                                            <div class="col-lg-9">
                                                <label class="form-label">Note</label>
                                                <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_note]"
                                                    id="pr_note" value="{{$item->pr_note}}">
                                            </div>
                        
                                            <div class="col-lg-12">
                                                <label class="form-label">Product Description</label>
                                                <textarea type="text" placeholder='Input Someting' class="form-control" name="inp[pr_description]"
                                                    id="pr_description">{{$item->pr_description}}</textarea>
                                            </div> 

                                            <div class="col-lg-3">
                                                <label class="form-label">Quantity</label>
                                                <div class="d-flex align-items-center" style="gap:10px">
                                                    <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_quantity]"
                                                        id="pr_quantity" value="{{$item->pr_quantity}}">
                                                    <span>Items</span>
                                                </div>
                                            </div>
                                        </div> 
                
                                        <div class="d-flex flex-wrap mt-1" style="gap:10px">
                                            <button type="button" class="btn btn-relief-danger btn-sm me-auto" onclick="delFormProduk('{{$item->pr_id}}')"><i class="bx bx-trash"></i> Delete</button>
                                            <button type="button" class="btn btn-relief-primary btn-sm" onclick="saveFormProduk('frm-produk-{{$item->pr_id}}')"><i class="bx bx-save"></i> Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@endsection

@section('js_section')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://unpkg.com/cropperjs"></script>
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet" />
    <script>
        var select = $('.select2')

        function searchProducts(){
            $('#modal-search-product').modal('show')
        }

        function assign_product(){
            let selected_product = []
            $('input[name="selected_products[]"]').each(function(){
                if($(this).is(':checked')){
                    selected_product.push($(this).val())
                }
            })

            let formData = new FormData()
            formData.append('pr_ids', selected_product)
            formData.append('prj_id', '{{$id}}')
            $.ajax({
                url: '{{ url('api/tender-management/product') }}',
                type: 'post',
                data: formData,
                contentType: false, //untuk upload image
                processData: false, //untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    if (e.status == 'success') {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();
                        $('#modal-search-product').modal('hide')
                        setTimeout(function() {
                            location.reload()
                        }, 1000);
                    } else {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();;
                    }
                }
            });
        }

        function saveFormProduk(id) {
            if ($('#'+id).valid()) {
                var formData = new FormData($('#'+id)[0]);
                $.ajax({
                    url: '{{ url('api/tender-management/product/upd') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            setTimeout(function() {
                                location.reload()
                            }, 1000);
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        function delFormProduk(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: 'You will not be able to recover this data!',
                showCancelButton: true,
                confirmButtonText: 'Proceed',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ url("api/tender-management/product/del") }}/'+id,
                        type: 'post',
                        data: {
                            _method: 'delete'
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        success: function(e) {
                            if (e.status == 'success') {
                                new Noty({
                                    text: e.message,
                                    type: 'info',
                                    progressBar: true,
                                    timeout: 1000
                                }).show();
                                setTimeout(function() {
                                    location.reload()
                                }, 1000);
                            } else {
                                new Noty({
                                    text: e.message,
                                    type: 'info',
                                    progressBar: true,
                                    timeout: 1000
                                }).show();;
                            }
                        }
                    });
                }
            })
        }
    </script>
    @include('tender-management.modal-add-product')
@endsection
