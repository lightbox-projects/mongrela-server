@extends('layouts.layout1')

@section('css_section')
    <style>
        .preview {
            overflow: hidden;
            width: 200px;
            height: 200px;
            border: 1px solid red;
        }
    </style>
    <style>
        input,
        textarea,
        select {
            border: unset !important;
            border-bottom: 1px solid #d8d6de !important;
            border-radius: unset !important
        }

        input[type=checkbox] {
            border: 1px solid #d8d6de !important;
        }
    </style>
@endsection

@section('page_title')
    Tender Form
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    @include('tender-management.header')

    <div class="pb-3" style="min-height: 60vh">

        <div style="gap:10px" class="mb-2 d-flex align-items-center flex-wrap">
            <h4 class="mb-0 me-auto fw-bolder">Final Result</h4>
        </div>

        <div class="mt-2">
            @if(!$project->winner_bid_id)
            <div class="card card-body">
                <div class="d-flex flex-column">
                    <div class="m-auto">
                        <h1 class="fw-bolder text-center">Set a Winner</h1>
                        <p class="text-center my-1" style="max-width:700px">
                            Choose a bidders from the bidder list to be a final result.
                        </p>
                    </div>

                </div>
            </div>
            @endif

            <div class="row" style="gap:10px 0">
                @foreach ([1] as $item)
                    @foreach ($project->bids as $item)
                        <div class="col-lg-6" style="">
                            <div class="card card-body mb-0 {{$project->winner_bid_id == $item->bid_id ? 'bg-secondary' : ''}}">
                                <div class="d-flex align-items-center" style="gap:10px 30px">
                                    <div class="d-flex flex-column align-items-center justify-content-center">
                                        <h6 class="fw-bolder {{$project->winner_bid_id == $item->bid_id ? 'text-white' : ''}}">Evaluation Score</h6>
                                        <div class="d-flex align-items-end">
                                            <h1 class="mb-0 text-primary fw-bolder display-3">{{ $item->bid_score }}</h1>
                                            <h6 class="{{$project->winner_bid_id == $item->bid_id ? 'text-white' : 'text-secondary'}} h4 fw-bolder mb-0">
                                                /{{ count($item->products) * 7 }}</h6>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="fw-bolder h4 {{$project->winner_bid_id == $item->bid_id ? 'text-white' : ''}}">{{ $item->owner->user_name }}</div>   
                                        @if(!$project->winner_bid_id)
                                        <button class="btn btn-primary btn-sm"
                                            onclick="selectBid(`{{ $item->bid_id }}`, `{{ $item->prj_id }}`)">
                                            Select
                                        </button>
                                        @elseif($project->winner_bid_id == $item->bid_id)
                                        <button class="btn btn-primary btn-sm">
                                            Selected
                                        </button>
                                        @endif  
                                    </div>
                                    <div class="col-lg-auto ms-auto">
                                        <h6 class="fw-bolder {{$project->winner_bid_id == $item->bid_id ? 'text-white' : ''}}">Bid applied at</h6>
                                        <h6 class="mb-0 {{$project->winner_bid_id == $item->bid_id ? 'text-white' : ''}}">{{ date('Y-m-d', strtotime($item->created_at)) }}</h6>
                                    </div>

                                    <div class="col-lg-auto">
                                        <h6 class="fw-bolder {{$project->winner_bid_id == $item->bid_id ? 'text-white' : ''}}">Bid submited at</h6> 
                                        <h6 class="mb-0 {{$project->winner_bid_id == $item->bid_id ? 'text-white' : ''}}">{{ $item->submited_at }}</h6> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach
            </div>
        </div>

    </div>
@endsection

@section('js_section')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        function selectBid(bid_id, prj_id) {
            let formData = new FormData()
            formData.append('id', prj_id)
            formData.append('inp[winner_bid_id]', bid_id)

            Swal.fire({
                title: 'Are you sure?',
                text: 'You will not be able to revert this!',
                showCancelButton: true,
                confirmButtonText: 'Proceed',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ url('api/tender-management') }}',
                        type: 'post',
                        data: formData,
                        contentType: false, //untuk upload image
                        processData: false, //untuk upload image
                        timeout: 300000, // sets timeout to 3 seconds
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(e) {
                            if (e.status == 'success') {
                                new Noty({
                                    text: e.message,
                                    type: 'info',
                                    progressBar: true,
                                    timeout: 1000
                                }).show();
                                setTimeout(function() {
                                    location.reload()
                                }, 1000);
                            } else {
                                new Noty({
                                    text: e.message,
                                    type: 'info',
                                    progressBar: true,
                                    timeout: 1000
                                }).show();;
                            }
                        }
                    });
                }
            })
        }
    </script>
@endsection
