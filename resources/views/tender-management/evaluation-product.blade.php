
@php
    function isSimiliar($ori, $target){
        $percent = 0;
        similar_text($ori, $target, $percent);
        return $percent > 50;
    }
@endphp

<h5 class="mt-2">Product List</h5>
<div class="mt-1 row" style="gap:10px 0">
    <div class="col-lg-auto">
        <div class="card card-body mb-0 d-flex flex-column" style="gap:10px">
            @foreach ($project->products as $item)
                <div class="d-flex align-items-center" style="gap:10px">
                    <a href="{{ url('tender-management/' . $id . '/form/evaluation/' . $item->pr_id) }}"
                        class="btn flex-fill
                        {{ $item->pr_id == $active_product->pr_id ? 'btn-primary' : 'btn-custom-tender' }}
                        "
                        style="border:unset">
                        {{ $item->product->pr_name }}
                    </a> 
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-lg">
        <div class="p-1 bg-white rounded">
            <div class="p-1 rounded mb-2" style="background: #F4F4F4 !important">
                <h1 class="fw-bolder">{{ $active_product->pr_name }}</h1>
                <p>
                    {{ $active_product->pr_description }}
                </p>
                <div>
                    Required Quantity: {{ $active_product->pr_quantity }} items
                </div>
            </div> 

            <div>

                <form id="frm">
                    @csrf
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td style="background:#F4F4F4;width:35%">
                                        <div class="fw-bolder">By Host</div>
                                    </td>
                                    @foreach ($project->bids as $item)
                                    <td colspan="2" class="border-primary text-primary">
                                        <div class="fw-bolder">
                                            {{$item->owner->user_name}}
                                        </div>
                                    </td>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody> 
                                <tr>
                                    <td style="background:#F4F4F4;width:35%">
                                        <div class="fw-bolder">Brand</div>
                                        <div>{{ $active_product->pr_brand }}</div>
                                    </td>
                                    @foreach ($project->bids as $item)
                                        @php
                                            $product = $item->products->where('pr_id_original', $pr_id)->where('bid_id', $item->bid_id)->first();
                                            if(!$product) dd($product, $item);
                                        @endphp
                                        <td> 
                                            {{$product->pr_brand}}
                                        </td>
                                        <td style="width:50px">
                                            <input type="checkbox" class="form-check-input border-primary bidder-{{$product->bid_id}}"
                                            {{isSimiliar($active_product->pr_brand, $product->pr_brand) ? 'checked' : ''}}
                                                name="checked[{{$product->bid_id}}][brand]" />
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="background:#F4F4F4;width:35%">
                                        <div class="fw-bolder">Model</div>
                                        <div>{{ $active_product->pr_model }}</div>
                                    </td>
                                    @foreach ($project->bids as $item)
                                        @php
                                            $product = $item->products->where('pr_id_original', $pr_id)->where('bid_id', $item->bid_id)->first();
                                            if(!$product) dd($product, $item);
                                        @endphp
                                        <td> 
                                            {{$product->pr_model}}
                                        </td>
                                        <td style="width:50px">
                                            <input type="checkbox" class="form-check-input border-primary bidder-{{$product->bid_id}}" 
                                                {{isSimiliar($active_product->pr_model, $product->pr_model) ? 'checked' : ''}}
                                                name="checked[{{$product->bid_id}}][model]" />
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="background:#F4F4F4;width:35%">
                                        <div class="fw-bolder">Accessories</div>
                                        <div>{{ $active_product->pr_accessories }}</div>
                                    </td>
                                    @foreach ($project->bids as $item)
                                        @php
                                            $product = $item->products->where('pr_id_original', $pr_id)->where('bid_id', $item->bid_id)->first();
                                            if(!$product) dd($product, $item);
                                        @endphp
                                        <td> 
                                            {{$product->pr_accessories}}
                                        </td>
                                        <td style="width:50px">
                                            <input type="checkbox" class="form-check-input border-primary bidder-{{$product->bid_id}}" 
                                                {{isSimiliar($active_product->pr_accessories, $product->pr_accessories) ? 'checked' : ''}}
                                                name="checked[{{$product->bid_id}}][accessories]" />
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="background:#F4F4F4;width:35%">
                                        <div class="fw-bolder">Materials</div>
                                        <div>{{ $active_product->pr_materials }}</div>
                                    </td>
                                    @foreach ($project->bids as $item)
                                        @php
                                            $product = $item->products->where('pr_id_original', $pr_id)->where('bid_id', $item->bid_id)->first();
                                            if(!$product) dd($product, $item);
                                        @endphp
                                        <td> 
                                            {{$product->pr_materials}}
                                        </td>
                                        <td style="width:50px">
                                            <input type="checkbox" class="form-check-input border-primary bidder-{{$product->bid_id}}" 
                                                {{isSimiliar($active_product->pr_materials, $product->pr_materials) ? 'checked' : ''}}
                                                name="checked[{{$product->bid_id}}][materials]" />
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="background:#F4F4F4;width:35%">
                                        <div class="fw-bolder">Finishing</div>
                                        <div>{{ $active_product->pr_finishing }}</div>
                                    </td>
                                    @foreach ($project->bids as $item)
                                        @php
                                            $product = $item->products->where('pr_id_original', $pr_id)->where('bid_id', $item->bid_id)->first();
                                            if(!$product) dd($product, $item);
                                        @endphp
                                        <td> 
                                            {{$product->pr_finishing}}
                                        </td>
                                        <td style="width:50px">
                                            <input type="checkbox" class="form-check-input border-primary bidder-{{$product->bid_id}}" 
                                                {{isSimiliar($active_product->pr_finishing, $product->pr_finishing) ? 'checked' : ''}}
                                                name="checked[{{$product->bid_id}}][finishing]" />
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="background:#F4F4F4;width:35%">
                                        <div class="fw-bolder">Dimension</div>
                                        <div>{{ $active_product->pr_dimension }}</div>
                                    </td>
                                    @foreach ($project->bids as $item)
                                        @php
                                            $product = $item->products->where('pr_id_original', $pr_id)->where('bid_id', $item->bid_id)->first();
                                            if(!$product) dd($product, $item);
                                        @endphp
                                        <td> 
                                            {{$product->pr_dimension}}
                                        </td>
                                        <td style="width:50px">
                                            <input type="checkbox" class="form-check-input border-primary bidder-{{$product->bid_id}}" 
                                                {{isSimiliar($active_product->pr_dimension, $product->pr_dimension) ? 'checked' : ''}}
                                                name="checked[{{$product->bid_id}}][dimension]" />
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="background:#F4F4F4;width:35%">
                                        <div class="fw-bolder">Total Price</div>
                                    </td>
                                    @foreach ($project->bids as $item)
                                        @php
                                            $product = $item->products->where('pr_id_original', $pr_id)->where('bid_id', $item->bid_id)->first();
                                            if(!$product) dd($product, $item);
                                        @endphp
                                        <td> 
                                            S${{($product->pr_price * $product->pr_quantity)}} | S${{$product->pr_price}}/items | {{$product->pr_quantity}} items provided.
                                        </td>
                                        <td style="width:50px">
                                            <input type="checkbox" class="form-check-input border-primary bidder-{{$product->bid_id}}" name="checked[{{$product->bid_id}}][price]" />
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="background:#F4F4F4;width:35%">
                                        <div class="fw-bolder">Score</div>
                                    </td>
                                    @foreach ($project->bids as $item)
                                        @php
                                            $product = $item->products->where('pr_id_original', $pr_id)->where('bid_id', $item->bid_id)->first();
                                            if(!$product) dd($product, $item);
                                        @endphp
                                        <td class="border-primary" colspan="2">
                                            <h1 class="fw-bolder text-primary mb-0 d-flex align-items-end">
                                                <span id="{{$product->bid_id}}-bidder">0</span>
                                                <span class="h4 mb-0 text-primary">/7</span>
                                            </h1>
                                        </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <input type="hidden" name="prj_id" value="{{ $id }}" required />
                    <input type="hidden" name="pr_id" value="{{ $pr_id }}" required />
                </form> 

                <div class="d-flex mt-1">
                    @if ($next)
                        <button type="button" onclick="onSaveNNext()" class="ms-auto btn btn-primary">
                            <i class="bx bx-save"></i>
                            Save and Go to Next Product
                        </button>
                    @else
                        <button type="button" onclick="onSave()" class="ms-auto btn btn-primary">
                            <i class="bx bx-save"></i>
                            Save
                        </button>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>

<script>
function onSave() {
        if ($("#frm").valid()) {
            var formData = new FormData($('#frm')[0]);
            $.ajax({
                url: '{{ url('api/tender-management/evaluate-bids') }}',
                type: 'post',
                data: formData,
                contentType: false, //untuk upload image
                processData: false, //untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType: 'json',
                success: function(e) {
                    if (e.status == 'success') {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();
                        setTimeout(function() {
                            location.reload()
                        }, 1000);
                    } else {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();;
                    }
                }
            });
        }
    }

    function onSaveNNext() {
        if ($("#frm").valid()) {
            var formData = new FormData($('#frm')[0]);
            $.ajax({
                url: '{{ url('api/tender-management/evaluate-bids') }}',
                type: 'post',
                data: formData,
                contentType: false, //untuk upload image
                processData: false, //untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType: 'json',
                success: function(e) {
                    if (e.status == 'success') {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();
                        setTimeout(function() {
                            window.location.href =
                                "{{ url('tender-management/' . $id . '/form/evaluation/' . $next) }}"
                        }, 1000);
                    } else {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();;
                    }
                }
            });
        }
    }

    $(function(){
        $(`.form-check-input`).change(function(){
            let name = $(this).attr('name').split("[")[1].split("]")[0] 
            $(`#${name}-bidder`).html($(`.bidder-${name}[checked]`).length)
        })

        $(`.form-check-input`).trigger('change')
    })
</script>