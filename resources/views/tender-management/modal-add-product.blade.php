<div class="modal fade" id="modal-search-product" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl mx-auto" style="max-width:1366px">
        <div class="modal-content">
            <div class="modal-header bg-transparent">
                <div class="modal-title"><i data-feather="filter"></i> <span id="file-title">Add Products</span></div>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body px-0 position-relative">
                <table class="table table-striped" id="table">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th>PRODUCT NAME</th>
                            <th>APPLICATION</th>
                            <th>FINISHING</th>
                            <th>BRAND</th>
                            <th>MODEL</th>
                            <th>ACCESSORIES</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    let select_all_products = false
    let dTable

    $(function() {
        dTable = $('#table').DataTable({
            ajax: {
                url: "{{ url('api/masterdata/products/list') }}",
                type: 'post',
                data: function(d) {
                    d.all_products = select_all_products 
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            dom: '<"d-flex justify-content-between align-items-center header-actions mx-2 row mt-75"' +
                '<"col-sm-12 col-lg d-flex justify-content-center justify-content-lg-start custom-button">' +
                '<"col-sm-12 col-lg-auto ps-xl-75 ps-0"<"dt-action-buttons d-flex align-items-center justify-content-center justify-content-lg-end flex-lg-nowrap flex-wrap"<"me-1"f>l B>>' +
                '>t' +
                '<"d-flex justify-content-between mx-2 row mb-1"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                '>',
            columns: [{
                    data: 'checkbox'
                },
                {
                    data: 'pr_name'
                },
                {
                    data: 'pr_application'
                },
                {
                    data: 'pr_finishing'
                },
                {
                    data: 'pr_brand'
                },
                {
                    data: 'pr_model'
                },
                {
                    data: 'pr_accessories'
                },
            ],
            order: [
                [1, 'desc']
            ],
        }); 

        $('.custom-button').append(`
            <div class="btn btn-primary me-1" onclick="assign_product()">
                Assign Product
            </div>
            <div class="btn btn-secondary" id="toggle_select_all_products">
                Select From Other Tender
            </div>
        `)
        
        $('#toggle_select_all_products').click(function(){
            if(select_all_products){
                select_all_products = false
                $(this).removeClass('btn-success').addClass('btn-secondary')
            }
            else {
                select_all_products = true
                $(this).removeClass('btn-secondary').addClass('btn-success')
            }
        
            dTable.draw()
        }) 
        
        $('#table_filter input').attr('placeholder', 'Search Anything')
        // $('#table_filter').hide()
    })

</script>
