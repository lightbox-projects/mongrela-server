<div class="" style="">
    <h5>Bids Performance</h5>
    <div class="mt-1" id="chart-3"></div>

    <p class="mt-2 text-primary">Warning !! Score only will be updated after you click save.</p>
</div>

<script>
    let chart_3 = Highcharts.chart('chart-3', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f} scores'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },
        series: [
            @foreach($project->bids as $item)
            {
                name: "{{$item->owner->user_name}}",
                color: '#F26122',
                data: [{
                    name: "{{$item->owner->user_name}}",
                    y: {{$item->bid_score}},
                }]
            }, 
            @endforeach
        ],
    });
</script>
