<div class="bs-stepper wizard-modern modern-wizard-example mb-2">
    <div class="bs-stepper-header d-flex flex-nowrap" style="padding:unset !important;padding-bottom:.5em !important;overflow-x:auto !important">
        <div class="step {{ request()->path() == 'tender-management/'.$id.'/form' ? 'active' : '' }}" data-target="#account-details-modern" role="tab" id="account-details-modern-trigger">
            <a href="{{url('tender-management/'.$id.'/form')}}" type="button" class="step-trigger" aria-selected="true">
                <span class="bs-stepper-box">
                    <i class="bx bx-book-content"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Tender Information</span>
                    <span class="bs-stepper-subtitle">Setup Tender Details</span>
                </span>
            </a>
        </div>
        <div class="line">
            <i class="bx bx-chevron-right"></i>
        </div>
        <div class="step {{ str_contains(request()->path(), 'tender-management/'.$id.'/form/products') ? 'active' : '' }}" data-target="#personal-info-modern" role="tab" id="personal-info-modern-trigger">
            <a href="{{url('tender-management/'.$id.'/form/products')}}" type="button" class="step-trigger" aria-selected="false">
                <span class="bs-stepper-box">
                    <i class="bx bx-package"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Products</span>
                    <span class="bs-stepper-subtitle">Assign Product to Project</span>
                </span>
            </a>
        </div> 
        <div class="line">
            <i class="bx bx-chevron-right"></i>
        </div>
        <div class="step {{ str_contains(request()->path(), 'tender-management/'.$id.'/form/publish') ? 'active' : '' }}" data-target="#personal-info-modern" role="tab" id="personal-info-modern-trigger">
            <a href="{{url('tender-management/'.$id.'/form/publish')}}" type="button" class="step-trigger" aria-selected="false">
                <span class="bs-stepper-box">
                    <i class="bx bx-show"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Publish</span>
                    <span class="bs-stepper-subtitle">Publishing Tender to Client</span>
                </span>
            </a>
        </div> 
        <div class="line">
            <i class="bx bx-chevron-right"></i>
        </div>
        <div class="step {{ str_contains(request()->path(), 'bid') ? 'active' : '' }}" data-target="#personal-info-modern" role="tab" id="personal-info-modern-trigger">
            <a href="{{url('tender-management/'.$id.'/form/bids')}}" type="button" class="step-trigger" aria-selected="false">
                <span class="bs-stepper-box">
                    <i class="bx bx-dollar"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Bids</span>
                    <span class="bs-stepper-subtitle">Bidder applied in the tender.</span>
                </span>
            </a>
        </div> 
        <div class="line">
            <i class="bx bx-chevron-right"></i>
        </div>
        <div class="step {{ str_contains(request()->path(), 'tender-management/'.$id.'/form/evaluation') ? 'active' : '' }}" data-target="#personal-info-modern" role="tab" id="personal-info-modern-trigger">
            <a href="{{url('tender-management/'.$id.'/form/evaluation')}}" type="button" class="step-trigger" aria-selected="false">
                <span class="bs-stepper-box">
                    <i class="bx bx-receipt"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Evaluation</span>
                    <span class="bs-stepper-subtitle">Evaluation to the submited bids.</span>
                </span>
            </a>
        </div> 
        <div class="line">
            <i class="bx bx-chevron-right"></i>
        </div>
        <div class="step {{ str_contains(request()->path(), 'tender-management/'.$id.'/form/final') ? 'active' : '' }}" data-target="#personal-info-modern" role="tab" id="personal-info-modern-trigger">
            <a href="{{url('tender-management/'.$id.'/form/final')}}" type="button" class="step-trigger" aria-selected="false">
                <span class="bs-stepper-box">
                    <i class="bx bx-bar-chart-alt-2"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Final Result</span>
                    <span class="bs-stepper-subtitle">Result of tender event.</span>
                </span>
            </a>
        </div> 
    </div>
</div>

@if(isset($project) && $project->winner_bid_id)
<div class="align-items-center justify-content-between alert mb-2 bg-primary p-1 text-white d-flex flex-wrap" style="gap:10px">
    Tender Winner {{$project->winner_bid->owner->user_name}} on {{$project->winner_elected_date}}
    <a class="btn btn-light btn-sm" href="{{url('bid/'.$project->prj_id.'/'.$project->winner_bid->user_id)}}">
        View Bid
    </a>
</div>
@endif