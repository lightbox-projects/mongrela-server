@extends('layouts.layout1')

@section('css_section')
    <style>
        .preview {
            overflow: hidden;
            width: 200px;
            height: 200px;
            border: 1px solid red;
        }
    </style>
    <style>
        input,
        textarea,
        select {
            border: unset !important;
            border-bottom: 1px solid #d8d6de !important;
            border-radius: unset !important
        }

        input[type=checkbox] {
            border: 1px solid #d8d6de !important;
        }
    </style>
@endsection

@section('page_title')
    Tender Form
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    @include('tender-management.header')

    <div class="pb-3" style="min-height: 60vh">

        <div class="">

            <div style="gap:10px" class="d-flex align-items-center flex-wrap">
                <h4 class="mb-0 me-auto fw-bolder">Tender Publishing</h4>
            </div>

            <div class="mt-2">
                <div class="card card-body">
                    <div class="d-flex" style="min-height:300px">
                        <div class="m-auto">
                            <h1 class="fw-bolder text-center">Set Tender Visibility</h1>
                            <p class="text-center my-1" style="max-width:700px">
                                Tender visibility affect the access to the tender. <strong
                                    class="text-primary">Public</strong> means that everyone
                                can see and access your tender through the tender browser,
                                <strong class="text-secondary">Private</strong> means that the tender will not shown in the
                                tender browser
                                and can only be accessed by the url shared by you. You change this anytime.
                            </p>
                            <div class="d-flex flex-wrap justify-content-center" style="gap:10px">
                                @if ($raw->prj_visibility)
                                    <button class="btn btn-{{ $raw->prj_visibility == 'private' ? 'success' : 'muted' }}"
                                        onclick="setVisibility('private')">
                                        <i class="bx bx-hide"></i>
                                        Private</button>
                                    <button class="btn btn-{{ $raw->prj_visibility == 'public' ? 'success' : 'muted' }}"
                                        onclick="setVisibility('public')">
                                        <i class="bx bx-show"></i>
                                        Public</button>
                                @else
                                    <button class="btn btn-secondary" onclick="setVisibility('private')">
                                        <i class="bx bx-hide"></i>
                                        Private</button>
                                    <button class="btn btn-primary" onclick="setVisibility('public')">
                                        <i class="bx bx-show"></i>
                                        Public</button>
                                @endif
                            </div>
                            @if ($raw->prj_visibility)
                                <p class="text-center mt-2" style="max-width:700px">
                                    You have selected <strong class="text-primary">{{ $raw->prj_visibility }}</strong>
                                    access.
                                    {{-- Click to
                                    <strong class="text-primary" onclick="setVisibility(null)" style="cursor:pointer">Reset access</strong>. --}}
                                </p>

                                @if ($raw->prj_visibility == 'private')
                                    <div class="d-flex flex-wrap justify-content-center mb-1" style="gap:10px">
                                        <button onclick="copyLink()" class="btn btn-secondary btn-sm">
                                            <i class="bx bx-link"></i>
                                            Copy Private Link
                                        </button>
                                    </div>
                                @endif

                                @if ($raw->prj_published_date)
                                    <p class="text-center">First published on {{ $raw->prj_published_date }}. Changing the
                                        visibility wont reset the published date.</p>
                                @endif

                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal" id="frmbox" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Terms And Condition</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    {!! $terms_and_condition->setting_value ?? '' !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">I Disagree</button>
                    <button type="button" class="btn btn-primary" onclick="proceedSave()">I Agree</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_section')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://unpkg.com/cropperjs"></script>
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet" />
    <script>
        var select = $('.select2')
        let formData = new FormData()

        function setVisibility(visibility) {
            formData = new FormData()
            formData.append('id', '{{ $id }}')
            formData.append('inp[prj_visibility]', visibility)

            $('#frmbox').modal('show')
        }

        function proceedSave(){
            $.ajax({
                url: '{{ url('api/tender-management') }}',
                type: 'post',
                data: formData,
                contentType: false, //untuk upload image
                processData: false, //untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    if (e.status == 'success') {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();
                        $('#modal-search-product').modal('hide')
                        setTimeout(function() {
                            location.reload()
                        }, 1000);
                    } else {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();;
                    }
                }
            });
        }

        function copyLink() {
            copyTextToClipboard('{{ url('/tenders/' . $id) }}');
            new Noty({
                text: 'Success copy submission url',
                type: 'info',
                progressBar: true,
                timeout: 1000
            }).show();
        }

        function copyTextToClipboard(text) {
            if (!navigator.clipboard) {
                fallbackCopyTextToClipboard(text);
                return;
            }
            navigator.clipboard.writeText(text).then(function() {
                console.log('Async: Copying to clipboard was successful!');
            }, function(err) {
                console.error('Async: Could not copy text: ', err);
            });
        }
    </script>
@endsection
