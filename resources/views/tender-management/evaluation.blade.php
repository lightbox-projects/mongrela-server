@extends('layouts.layout1')

@section('css_section')
    <style>
        .preview {
            overflow: hidden;
            width: 200px;
            height: 200px;
            border: 1px solid red;
        }
    </style> 
    <style>
        .btn-custom-tender {
            background: #F3F3F3 !important;
            text-align: left !important;
        }
    
        .btn-custom-tender:hover {
            color: white !important;
            background: #F26122 !important
        }
    
        input,
        textarea,
        select {
            border: unset !important;
            border-bottom: 1px solid #d8d6de !important;
            border-radius: unset !important
        }
    
        .form-check-input:checked {
            background: #F26122 !important
        }
        .border-primary {
            border-width: 1px !important
        }
    </style>
@endsection

@section('page_title')
    Tender Form
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/maps/modules/map.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
@section('content')
    @include('tender-management.header')

    <div class="pb-3" style="min-height: 60vh"> 

        <div style="gap:10px" class="mb-2 d-flex align-items-center flex-wrap">
            <h4 class="mb-0 me-auto fw-bolder">Tender Evaluation</h4>
        </div>
        
        <div class="">
            @include('tender-management.evaluation-performance')
        </div>

        @include('tender-management.evaluation-product')
        
        <div class="mt-3">
            <h2 class="text-secondary fw-bolder">This suppose to be a guide</h2>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
        </div>
         
    </div>
@endsection

@section('js_section') 

    <script>
        let select
    </script>
@endsection