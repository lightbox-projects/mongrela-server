@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    Tender List
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3" style="min-height:60vh">

        <div class="d-flex justify-content-between flex-wrap align-items-center mb-4 mt-2" style="gap:10px">
            <div>
                <h3 class="fw-bolder">Tender Management</h3>
                <p class="mb-0" style="max-width:800px">
                    Tender management involves handling bids and tenders from start to finish, including opportunity identification, document preparation, submission, and evaluation. It maximizes the chances of winning contracts through streamlined and competitive bid processes.
                </p>
                @if(!$company)
                <p class="mt-2">*You need to fill in the 
                    <a class="text-primary fw-bolder" href="{{url('/users/profile/'.session()->get('userId').'/company')}}">company profile</a> information.
                </p>
                @endif
            </div>
        </div> 

        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped" id="table">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th>PROJECT NAME</th>
                            <th>PROJECT PERIOD</th>
                            <th>EXPECTED DELIVERIES</th>
                            <th>ITEMS NEEDED</th> 
                            <th>BIDS</th> 
                            <th>VISIBILITY</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> 
@endsection

@section('js_section')
    <script>
        var dTable = $('#table'),
            select = $('.select2')

        // List datatable
        $(function() {
            dTable = $('#table').DataTable({
                ajax: {
                    url: "{{ url('api/tender-management/list') }}",
                    type: 'post',
                    data: function(d) {
                        d.model = $('#model-filter').val()
                        d.brand = $('#brand-filter').val()
                        d.application = $('#application-filter').val()
                        d.name = $('#name-filter').val()
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                dom: '<"d-flex justify-content-between align-items-center header-actions mx-2 row mt-75"' +
                    '<"col-sm-12 col-lg d-flex justify-content-center justify-content-lg-start custom-button">' +
                    '<"col-sm-12 col-lg-auto ps-xl-75 ps-0"<"dt-action-buttons d-flex align-items-center justify-content-center justify-content-lg-end flex-lg-nowrap flex-wrap"<"me-1"f>l B>>' +
                    '>t' +
                    '<"d-flex justify-content-between mx-2 row mb-1"' +
                    '<"col-sm-12 col-md-6"i>' +
                    '<"col-sm-12 col-md-6"p>' +
                    '>',
                columns: [{
                        data: 'action'
                    },
                    {
                        data: 'prj_name'
                    },
                    {
                        data: 'periode'
                    },
                    {
                        data: 'prj_expected_completion_date'
                    },
                    {
                        data: 'items'
                    }, 
                    {
                        data: 'bids'
                    }, 
                    {
                        data: 'prj_visibility'
                    }, 
                ],
                order: [
                    [1, 'desc']
                ],
            });

            @if($company)
            $('.custom-button').append(`
                <div class="d-flex flex-wrap" style="gap:10px"> 
                    <button class="btn btn-primary font-weight-semibold text-nowrap" onclick="addnew()">
                        <i class="bx bx-plus"></i> <span class="d-none d-lg-inline-block">Create New Tender</span>
                    </button>
                </div>
            `)
            @endif

            $('#table_filter input').attr('placeholder', 'Search Anything')
            // $('#table_filter').hide()
        })

        function addnew() {
            window.location.href = "{{ url('tender-management/new') }}";
        }

        function edit(code) {
            window.location.href = "{{ url('tender-management/') }}/" + code + '/form';
        } 
    </script>
@endsection
