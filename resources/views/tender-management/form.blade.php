@extends('layouts.layout1')

@section('css_section')
    <style>
        .preview {
            overflow: hidden;
            width: 200px;
            height: 200px;
            border: 1px solid red;
        }
    </style>
    <style>
        input,
        textarea,
        select {
            border: unset !important;
            border-bottom: 1px solid #d8d6de !important;
            border-radius: unset !important
        }
    </style>
@endsection

@section('page_title')
    Tender Form
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    @include('tender-management.header')

    <div class="pb-3">

        <div style="gap:10px" class="d-flex align-items-center flex-wrap">
            <h4 class="mb-0 me-auto fw-bolder">Tender Information</h4>
            @if ($id)
                <button class="btn btn-danger" onclick="delF()">
                    <i class="bx bx-save"></i> Delete Product
                </button>
            @endif
            <button class="btn btn-primary" onclick="save()">
                <i class="bx bx-save"></i> Save Form & Continue
            </button>
        </div>

        <div class="card card-body mt-2">
            <form id="frm" class="mt-2">
                @csrf
                <input type="hidden" id="prj_id" name="id" />
                <div class="row" style="gap: 10px 0">
                    <div class="col-lg-12">
                        <div style="height:300px" class="w-100 bg-secondary rounded mb-1" id="col_prj_photo">
                            <img id="prj_photo_preview" class="w-100 rounded" style="object-fit: cover;height:300px" />
                        </div>
                        <label class="form-label">Project Banner</label>
                        <input type="file" class="form-control" name="prj_photo" accept=".png, .jpg">
                        <a class="form-text text-success" id="prj_photo">Already uploaded.</a>
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Project Name</label>
                        <input type="text" placeholder='Input Someting' class="form-control" name="inp[prj_name]"
                            id="prj_name">
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Project Tags</label>
                        {{-- <input type="text" placeholder='Input Someting' class="form-control" name="inp[prj_tags]"
                            id="prj_tags"> --}}
                        <select name="tags[]" multiple id="prj_tags" class="select2-tags">
                            @foreach ($tags as $item)
                                <option value="{{ $item->tag_name }}">
                                    {{ $item->tag_name }}
                                </option>
                            @endforeach
                        </select>
                        <label>
                            *You can create new by typing new tag without (#) if not exist.
                        </label>
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Project Category</label>
                        {{-- <input type="text" placeholder='Input Someting' class="form-control" name="inp[prj_category]"
                            id="prj_category"> --}}
                        <select name="inp[prj_category]" id="prj_category" class="select2">
                            @foreach ($category as $item)
                                <option value="{{ $item->category_id }}">
                                    {{ $item->category_name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Tender Start</label>
                        <input type="date" placeholder='Input Someting' class="form-control" name="inp[prj_tender_start]"
                            id="prj_tender_start">
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Tender End</label>
                        <input type="date" placeholder='Input Someting' class="form-control" name="inp[prj_tender_end]"
                            id="prj_tender_end">
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Expected Deliveries</label>
                        <input type="date" placeholder='Input Someting' class="form-control"
                            name="inp[prj_expected_completion_date]" id="prj_expected_completion_date">
                    </div>
                    <div class="col-lg-12">
                        <label class="form-label">Product Description</label>
                        <textarea type="text" placeholder='Input Someting' class="form-control" name="inp[prj_description]"
                            id="prj_description"></textarea>
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection

@section('js_section')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://unpkg.com/cropperjs"></script>
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet" />
    <script>
        var select = $('.select2')

        $(".select2-tags").select2({
            tags: true,
            createTag: function(params) {
                return {
                    id: params.term,
                    text: params.term,
                    newOption: true
                }
            }
        });

        @if ($id)
            edit('{{ $id }}')
        @endif

        function edit(id) {
            $.ajax({
                url: '{{ url('api/tender-management/') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if(key == 'prj_tags'){
                            let temp = value.replaceAll('#', '').trim().split(' ')
                            console.log(temp)
                            $('#' + key).val(temp).trigger('change');
                        }
                        else if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                        'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            $('#' + key).val(value);
                        }
                    });
                    if (e.prj_photo) {
                        $('#prj_photo').show();
                        $('#prj_photo').attr('href', '{{ url('getimage') }}/' + btoa(e.prj_photo));
                        $('#col_prj_photo ').show();
                        $('#prj_photo_preview').attr('src', '{{ url('getimage') }}/' + btoa(e.prj_photo));
                    } else {
                        $('#prj_photo').hide();
                        $('#col_prj_photo').hide();
                    }
                }
            });
        }

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);
                $.ajax({
                    url: '{{ url('api/tender-management') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            setTimeout(function() {
                                window.location.href =
                                    '{{ url('tender-management/' . $id . '/form/products') }}';
                            }, 1000);
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        function delF() {
            Swal.fire({
                title: 'Are you sure?',
                text: 'You will not be able to recover this data!',
                showCancelButton: true,
                confirmButtonText: 'Proceed',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ url("api/tender-management/$id") }}',
                        type: 'post',
                        data: {
                            _method: 'delete'
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        success: function(e) {
                            if (e.status == 'success') {
                                new Noty({
                                    text: e.message,
                                    type: 'info',
                                    progressBar: true,
                                    timeout: 1000
                                }).show();
                                setTimeout(function() {
                                    window.location.href = '{{ url('tender-management') }}';
                                }, 1000);
                            } else {
                                new Noty({
                                    text: e.message,
                                    type: 'info',
                                    progressBar: true,
                                    timeout: 1000
                                }).show();;
                            }
                        }
                    });
                }
            })

        }
    </script>

@endsection
