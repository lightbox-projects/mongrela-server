@extends('layouts.layout1')

@section('css_section')
    <style>
        .preview {
            overflow: hidden;
            width: 200px;
            height: 200px;
            border: 1px solid red;
        }
    </style>
    <style>
        input,
        textarea,
        select {
            border: unset !important;
            border-bottom: 1px solid #d8d6de !important;
            border-radius: unset !important
        }

        input[type=checkbox] {
            border: 1px solid #d8d6de !important;
        }
    </style>
@endsection

@section('page_title')
    Tender Form
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    @include('tender-management.header')

    <div class="pb-3" style="min-height: 60vh"> 

        <div style="gap:10px" class="mb-2 d-flex align-items-center flex-wrap">
            <h4 class="mb-0 me-auto fw-bolder">Tender Bids ({{count($project->bids)}} bids)</h4>
        </div>
        
        @foreach ($project->bids as $item)
            <div class="card card-body mb-1">
                <div class="d-flex flex-wrap align-items-center" style="gap:10px">
                    <div class="col-lg-6">
                        <div class="fw-bolder h4">{{$item->owner->user_name}}</div>
                        @if($item->bid_status == 'submited')
                            <h5 class="mb-0 text-success h6">Bid Submited</h5>
                        @else
                            <h5 class="mb-0 h6">Bid Unsubmited</h5>
                        @endif
                    </div>

                    <div class="col-lg">
                        <h6 class="fw-bolder">Bid applied at</h6>
                        <h6 class="mb-0">{{date('Y-m-d', strtotime($item->created_at))}}</h6>
                    </div>
                    
                    <div class="col-lg">
                        <h6 class="fw-bolder">Bid submited at</h6>
                        @if($item->submited_at)
                            <h6 class="mb-0">{{$item->submited_at}}</h6>
                        @else
                            <h6 class="mb-0">-</h6>
                        @endif
                    </div>
                    
                    <div class="col-lg-auto ms-auto">
                        <a class="btn btn-primary" href="{{url('bid/'.$id.'/'.$item->user_id)}}">
                            View Bid
                        </a>
                    </div>

                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('js_section') 
    <script>
    </script>
@endsection
