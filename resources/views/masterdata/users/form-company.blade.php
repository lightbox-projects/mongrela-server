@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    User Profile
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    @include('masterdata.users.form-header')

    <div class="card">
        <form id="frm">
            @csrf
            <div class="card-body">
                <input type="hidden" name="user_id" value="{{$id}}" />
                <div class="row" style="gap:10px 0">
                    <div class="col-lg-3">
                        <label>Company Name</label>
                        <input class="form-control" name="inp[company_name]" id="company_name" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-3">
                        <label>Company Slogan</label>
                        <input class="form-control" name="inp[company_slogan]" id="company_slogan" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-6">
                        <label>Bussiness Speciality</label>
                        <input class="form-control" name="inp[company_bussiness_category]" id="company_bussiness_category" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-12">
                        <label>Description</label>
                        <textarea class="form-control" name="inp[company_description]" id="company_description" placeholder="Input Something"></textarea>
                    </div>
                    <div class="col-lg-4">
                        <label>Website</label>
                        <input class="form-control" name="inp[company_website]" id="company_website" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-4">
                        <label>Registered address</label>
                        <input class="form-control" name="inp[company_address]" id="company_address" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-4">
                        <label>Mailing address</label>
                        <input class="form-control" name="inp[company_mailing_address]" id="company_mailing_address" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-4">
                        <label>City</label>
                        <input class="form-control" name="inp[company_city]" id="company_city" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-4">
                        <label>State</label>
                        <input class="form-control" name="inp[company_state]" id="company_state" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-4">
                        <label>Country</label>
                        <input class="form-control" name="inp[company_country]" id="company_country" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-4">
                        <label>Zipcode</label>
                        <input class="form-control" name="inp[company_zipcode]" id="company_zipcode" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-4">
                        <label>Phone</label>
                        <input class="form-control" name="inp[company_phone]" id="company_phone" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-4">
                        <label>Email</label>
                        <input class="form-control" name="inp[company_email]" id="company_email" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-4">
                        <label>Fax</label>
                        <input class="form-control" name="inp[company_fax]" id="company_fax" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-8">
                        <label>Company Features</label>
                        <input class="form-control" name="inp[company_features]" id="company_features" placeholder="Input Something" />
                    </div>
                    <div class="col-lg-4">
                        <label>Profile Photo</label>
                        <input class="form-control" type="file" accept="image/*" name="company_photo" placeholder="Input Something" />
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="d-flex justify-content-end pb-2">
        <button class="btn btn-primary" onclick="save()">
            <i class="bx bx-save"></i>
            Save
        </button>
    </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);

                $.ajax({
                    url: '{{ url('api/users/company') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            $("#frmbox").modal('hide');
                            //dTable.draw();
                            window.location.reload()
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        edit('{{ $id }}')

        function edit(id) {
            $.ajax({
                url: '{{ url('api/users/company') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[n  ame='inp[" + key + "][]'][value='" + temp[i] + "']")
                                        .prop(
                                            'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            $('#' + key).val(value);
                        }
                    });

                    $("#frmbox").modal('show');
                }
            });
        }
    </script>
@endsection
