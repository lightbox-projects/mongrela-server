@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    User Management
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="content-body">
        <h3 class="fw-bolder">Roles List</h3>
        <p class="mb-2">
            A role provided access to predefined menus and features so that depending <br>
            on assigned role an administrator can have access to what he need
        </p>

        <!-- Role cards -->
        <div class="row mb-2">
            @foreach ($roles as $role)
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="card h-100 mb-1">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <span>Total {{ $role->users->count() }} users</span>
                                <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                                    @php
                                        $total_user = $role->users->count() ?? 0;
                                        if ($total_user > 4) {
                                            $total_user = 4;
                                        }
                                    @endphp
                                    @for ($i = 0; $i < $total_user; $i++)
                                        <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                            title="" class="avatar avatar-sm pull-up"
                                            data-bs-original-title="Vinnie Mostowy">
                                            <img class="rounded-circle"
                                                src="{{ asset('themes/vuexy/images/portrait/small/avatar-s-11.jpg') }}"
                                                alt="Avatar">
                                        </li>
                                    @endfor
                                </ul>
                            </div>
                            <div class="d-flex justify-content-between align-items-end mt-1 pt-25">
                                <div class="role-heading">
                                    <h4 class="fw-bolder">{{ $role->role_name }}</h4>
                                    <small class="text-muted">{{ $role->role_desc }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!--/ Role cards -->

        <h3 class="mt-50">Total users with their roles</h3>
        <p class="mb-2">Find all of your company’s administrator accounts and their associate roles.</p>
        <!-- table -->
        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped" id="table">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="frmbox" tabindex="-1" aria-labelledby="frmbox-title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <h5 class="modal-title">Add New User</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2">
                    <!-- form -->
                    <form id="frm" class="row gy-1 gx-2">
                        @csrf
                        <input type="hidden" name="id" id="user_id">
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">User Name</label>
                            <div class="input-group input-group-merge">
                                <input id="user_name" name="inp[user_name]" placeholder="User Name"
                                    class="form-control add-credit-card-mask" type="text" />
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">User Email</label>
                            <div class="input-group input-group-merge">
                                <input id="user_username" name="inp[user_username]" placeholder="john@email.com"
                                    class="form-control add-credit-card-mask" type="text" />
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">User Password</label>
                            <div class="input-group input-group-merge">
                                <input id="user_raw_password" name="inp[user_raw_password]" placeholder="User Password"
                                    class="form-control add-credit-card-mask" type="password" />
                            </div>
                        </div>
                        <div class="col-12">
                            <label class="form-label" for="modalAddCardNumber">User Role</label>
                            <select id="user_role" name="inp[user_role]" class="select2">
                                <option value="tender">Tender</option>
                                <option value="designer">Designer</option>
                                <option value="superadmin">Super Admin</option>
                            </select>
                        </div>
                        <div class="col-12">
                            <a class="btn btn-primary me-1 mt-1" onclick="save()">Submit</a>
                            <button type="reset" class="btn btn-outline-secondary mt-1" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_section')
    <script>
        var dTable = $('#table'),
            select = $('.select2')

        // List datatable
        $(function() {
            dTable = $('#table').DataTable({
                ajax: {
                    url: "{{ url('api/users/dt') }}",
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: false,
                        className: 'text-center'
                    },
                    {
                        data: 'user_name',
                    },
                    {
                        data: 'user_username',
                    },
                    {
                        data: 'user_role',
                    },
                ],
                order: [
                    [1, 'desc']
                ],
                buttons: [{
                    text: 'Add New',
                    className: 'add-new btn btn-primary',
                    action: function(e, dt, node, config) {
                        addnew()
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }],
                "bFilter": false,
            });
            $('.dataTables_filter input[type=search]').attr('placeholder', 'Search').attr('class', 'form-control');
            $('.dataTables_filter select[name=table_length]').attr('class', 'form-select form-select-sm');
        })

        function addnew() {
            $('#frmbox').modal('show');
            $('#frm')[0].reset();
            $('#frm input[name="inp[ms_brand_id]"]').val('');
        }

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);

                $.ajax({
                    url: '{{ url('api/users') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            $("#frmbox").modal('hide');
                            //dTable.draw();
                            window.location.reload()
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        function edit(id) {
            $.ajax({
                url: '{{ url('api/users/') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[n  ame='inp[" + key + "][]'][value='" + temp[i] + "']")
                                        .prop(
                                            'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            $('#' + key).val(value);
                        }
                    });

                    $("#frmbox").modal('show');
                }
            });
        }

        function del(id) {
            if (confirm('Are you sure to delete this data?')) {
                $.ajax({
                    url: '{{ url('api/users/') }}' + '/' + id,
                    type: 'post',
                    data: {
                        _method: 'delete'
                    },
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            //dTable.draw();
                            window.location.reload()
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }
    </script>
@endsection
