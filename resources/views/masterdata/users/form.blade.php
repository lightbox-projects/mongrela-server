@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    User Profile
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    @include('masterdata.users.form-header')

    <form id="frm">
        @csrf
        <div class="card">
            <div class="card-body">
                <input type="hidden" id="user_id" name="id" />
                <div class="row" style="gap:10px 0">
                    <div class="col-lg-4">
                        <label>Account Name</label>
                        <input class="form-control" name="inp[user_name]" id="user_name" placeholder="Input Something" />
                    </div> 
                    <div class="col-lg-4">
                        <label>Email</label>
                        <input class="form-control" name="inp[user_username]" id="user_username" placeholder="Input Something" />
                    </div> 
                    <div class="col-lg-4">
                        <label>Phone</label>
                        <input class="form-control" name="inp[user_phone]" id="user_phone" placeholder="Input Something" />
                    </div> 
                    <div class="col-lg-8">
                        <label>Address</label>
                        <input class="form-control" name="inp[user_address]" id="user_address" placeholder="Input Something" />
                    </div> 
                    <div class="col-lg-4">
                        <label>City</label>
                        <input class="form-control" name="inp[user_city]" id="user_city" placeholder="Input Something" />
                    </div> 
                    <div class="col-lg-4">
                        <label>State</label>
                        <input class="form-control" name="inp[user_state]" id="user_state" placeholder="Input Something" />
                    </div> 
                    <div class="col-lg-4">
                        <label>Country</label>
                        <input class="form-control" name="inp[user_country]" id="user_country" placeholder="Input Something" />
                    </div> 
                    <div class="col-lg-4">
                        <label>Zipcode</label>
                        <input class="form-control" name="inp[user_zipcode]" id="user_zipcode" placeholder="Input Something" />
                    </div> 
                    <div class="col-lg-4">
                        <label>Profile Photo</label>
                        <input type="file" accept="image/*" class="form-control" name="user_photo" placeholder="Input Something" />
                    </div> 
                </div>
            </div>
        </div>
    </form>
    
        <div class="d-flex justify-content-end pb-2">
            <button class="btn btn-primary" onclick="save()">
                <i class="bx bx-save"></i>
                Save
            </button>
        </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);

                $.ajax({
                    url: '{{ url('api/users') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            $("#frmbox").modal('hide');
                            //dTable.draw();
                            window.location.reload()
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        edit('{{$id}}')

        function edit(id) {
            $.ajax({
                url: '{{ url('api/users/') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[n  ame='inp[" + key + "][]'][value='" + temp[i] + "']")
                                        .prop(
                                            'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            $('#' + key).val(value);
                        }
                    });

                    $("#frmbox").modal('show');
                }
            });
        }
    </script>
@endsection
