<style>
    input, textarea, select {
        border: unset !important;
        border-bottom: 1px solid #d8d6de !important;
        border-radius: unset !important
    }
</style>

<div class="card w-100">
    <div class="card-body">
        <div class="row align-items-center " style="gap:10px 30px">
            <div class="col-lg-auto">
                <img src="{{url('getimage/'.base64_encode($user->user_photo))}}" class="rounded-circle" style="height:100px;width:100px;object-fit:cover" />
            </div>
            <div class="col-lg-auto">
                <h1 class="fw-bolder">{{$user->user_name}}</h1>
                <p class="mb-0 text-muted">
                    {{$user->user_address}}. {{$user->user_city}}. {{$user->user_state}}. {{$user->user_country}}. {{$user->user_zipcode}}.
                </p>
            </div>
            @if($user->company)
            <div class="col-lg">
                <h5 class="text-muted" style="font-size:13px">Company Profile</h5>
                <h5 class="fw-bolder">{{$user->company->company_name}} ({{$user->company->company_website}})</h5>
                <p class="mb-0 text-muted" style="font-size:12px">
                    {{$user->company->company_slogan}} <br/>
                    <strong>
                        {{$user->company->company_address}}. {{$user->company->company_city}}. {{$user->company->company_state}}. {{$user->company->company_country}}. {{$user->company->company_zipcode}}.    
                    </strong><br />
                </p>
            </div>
            @endif
        </div>
    </div>
</div>

<ul class="nav nav-pills ">
    <li class="nav-item">
        <a class="nav-link {{ request()->path() == 'users/profile/'.$id || request()->path() == 'users/profile' ? 'active' : '' }}" 
            aria-current="page" href="{{url('/users/profile/'.$id)}}">User Profile</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ request()->path() == 'users/profile/'.$id.'/company' ? 'active' : '' }}"
             href="{{url('/users/profile/'.$id.'/company')}}">Company Profile</a>
    </li> 
</ul>