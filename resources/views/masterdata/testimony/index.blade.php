@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    Testimony
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3" style="min-height:60vh">

        <div class="d-flex justify-content-between flex-wrap align-items-center" style="gap:10px">
            <div>
                <h3 class="fw-bolder">Testimony</h3>
                <p class="mb-0">
                    A role provided access to predefined menus and features so that depending <br>
                    on assigned role an administrator can have access to what he need
                </p>
            </div>
        </div>

        <hr class="my-2" />

        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped" id="table">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Quoted By</th>
                            <th>Published</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="modal fade" id="frmbox" tabindex="-1" aria-labelledby="frmbox-title" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-transparent">
                        <h5 class="modal-title">Form Testimony</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body px-2 pb-2">
                        <!-- form -->
                        <form id="frm" class="row gy-1 gx-2">
                            @csrf
                            <input type="hidden" name="id" id="testimony_id">
                            <div class="col-12">
                                <label class="form-label" for="modalAddCardNumber">Quoted By</label>
                                <div class="input-group input-group-merge">
                                    <input id="testimony_quoted" name="inp[testimony_quoted]" class="form-control add-credit-card-mask"
                                        type="text" />
                                </div>
                            </div> 
                            <div class="col-12">
                                <label class="form-label" for="modalAddCardNumber">Content</label>
                                <div id="full-wrapper">
                                    <div id="full-container">
                                        <div id="editor" style="min-height:200px">
                                            {{-- {!! $text !!} --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <a class="btn btn-primary me-1 mt-1" onclick="save()">Submit</a>
                                <button type="reset" class="btn btn-outline-secondary mt-1" data-bs-dismiss="modal"
                                    aria-label="Close">
                                    Cancel
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
        let dTable = $('#table')

        var fullEditor = new Quill('#full-container #editor', {
            bounds: '#full-container #editor',
            modules: {
                formula: true,
                syntax: true,
                toolbar: [
                    [{
                        font: ["sans-serif", "serif", "monospace", "poppins",'inter']
                        },
                        {
                            size: []
                        }
                    ],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                        color: [
                                "#000000", "#e60000", "#ff9900", "#ffff00", "#008a00", "#0066cc", "#9933ff",
                                "#ffffff", "#facccc", "#ffebcc", "#ffffcc", "#cce8cc", "#cce0f5", "#ebd6ff",
                                "#bbbbbb", "#f06666", "#ffc266", "#ffff66", "#66b966", "#66a3e0", "#c285ff",
                                "#888888", "#a10000", "#b26b00", "#b2b200", "#006100", "#0047b2", "#6b24b2",
                                "#444444", "#5c0000", "#663d00", "#666600", "#003700", "#002966", "#3d1466"
                            ]
                        },
                        {
                            background: []
                        }
                    ],
                    [{
                            script: 'super'
                        },
                        {
                            script: 'sub'
                        }
                    ],
                    [{
                            header: '1'
                        },
                        {
                            header: '2'
                        },
                        'blockquote',
                        'code-block'
                    ],
                    [{
                            list: 'ordered'
                        },
                        {
                            list: 'bullet'
                        },
                        {
                            indent: '-1'
                        },
                        {
                            indent: '+1'
                        }
                    ],
                    [
                        'direction',
                        {
                            align: []
                        }
                    ],
                    ['link', 'image', 'video', 'formula'],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        $(function() {
            dTable = $('#table').DataTable({
                ajax: {
                    url: "{{ url('api/testimony/dt') }}",
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: false,
                        className: 'text-center'
                    }, 
                    {
                        data: 'testimony_quoted',
                        name: 'testimony_quoted'
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at'
                    }, 
                ],
                buttons: [{
                    text: 'Add New',
                    className: 'add-new btn btn-primary',
                    action: function(e, dt, node, config) {
                        addnew()
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }],
                order: [
                    [1, 'desc']
                ],  
            });

            $('.dataTables_filter input[type=search]').attr('placeholder', 'Search').attr('class',
                'form-control');
            $('.dataTables_filter select[name=table_length]').attr('class', 'form-select form-select-sm');
        })

        function addnew() {
            $('#frmbox').modal('show');
            $('#frm')[0].reset();
            $('#testimony_id').val(null)
            fullEditor.root.innerHTML = ''
        }

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);
                formData.append('inp[testimony_content]', fullEditor.root.innerHTML)
                $.ajax({
                    url: '{{ url('api/testimony') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            $("#frmbox").modal('hide');
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        function edit(id) {
            $.ajax({
                url: '{{ url('api/testimony/') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                        'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            $('#' + key).val(value);
                        }
                    });
                    if(e.testimony_content) fullEditor.root.innerHTML = e.testimony_content;
                    $("#frmbox").modal('show');
                }
            });
        }

        function del(id) {
            if (confirm('Are you sure to delete this data?')) {
                $.ajax({
                    url: '{{ url('api/testimony/') }}' + '/' + id,
                    type: 'delete',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }
    </script>
@endsection
