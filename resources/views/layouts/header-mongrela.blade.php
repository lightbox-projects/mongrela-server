@php
    $url = url()->current();
    $exp = explode('/', $url);
    $url = array_slice($exp, 3);
@endphp

<nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow" style="top:0">
    {{-- <div class="navbar-header d-xl-block d-none">
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <a href="{{url('/')}}">
                    <img src="{{asset('images/Logo 1.png')}}" class="w-100" style="max-height:30px" />
                </a>
            </li>
        </ul>
    </div> --}}
    <div class="navbar-container d-flex content" style="min-height: 60px;border-radius:10px">
        <div class="bookmark-wrapper d-flex mx-auto align-items-center">
            <a href="{{url('/')}}">
                <img src="{{asset('images/Logo 1.png')}}" class="w-100 me-2" style="object-fit:contain;height:40px" />
            </a>
            {{-- <ul class="nav navbar-nav">  --}}
                {{-- <li class="nav-item {{ request()->path() == 'companies' ? 'active' : '' }} d-none d-lg-block">
                    <a class="nav-link d-flex aling-items-center" style="gap:10px" href="{{ url('/companies') }}">
                        <i class="bx bx-buildings" style="font-size:18px"></i>
                        Browse Companies    
                    </a>
                </li>
                <li class="nav-item {{ request()->path() == 'tenders' ? 'active' : '' }} d-none d-lg-block">
                    <a class="nav-link d-flex aling-items-center" style="gap:10px" href="{{ url('/tenders') }}">
                        <i class="bx bxs-component" style="font-size:18px"></i>
                        Browse Tenders
                    </a>
                </li>
                <li class="nav-item {{ request()->path() == 'pricing' ? 'active' : '' }} d-none d-lg-block">
                    <a class="nav-link d-flex aling-items-center" style="gap:10px" href="{{ url('/pricing') }}">
                        <i class="bx bx-dollar" style="font-size:18px"></i>
                        Pricing
                    </a>
                </li> --}}
            {{-- </ul> --}}
        </div>
        {{-- <ul class="nav navbar-nav align-items-center ms-auto">
            
        </ul> --}}
    </div>
</nav>
