@php
    $url = url()->current();
    $exp = explode('/', $url);
    $url = array_slice($exp, 3);
@endphp

<nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow" style="top:0">
    {{-- <div class="navbar-header d-xl-block d-none">
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <a href="{{url('/')}}">
                    <img src="{{asset('images/Logo 1.png')}}" class="w-100" style="max-height:30px" />
                </a>
            </li>
        </ul>
    </div> --}}
    <div class="navbar-container d-flex content" style="min-height: 60px;border-radius:10px">
        <div class="bookmark-wrapper d-flex align-items-center">
            <a href="{{url('/')}}">
                <img src="{{asset('images/Logo 1.png')}}" class="w-100 me-2" style="max-height:20px;object-fit:contain" />
            </a>
            <ul class="nav navbar-nav"> 
                <li class="nav-item {{ request()->path() == 'tenders' ? 'active' : '' }} d-none d-lg-block">
                    <a class="nav-link d-flex aling-items-center" style="gap:10px" href="{{ url('/tenders') }}">
                        {{-- <i class="bx bxs-component" style="font-size:18px"></i> --}}
                        Tenders
                    </a>
                </li>
                <li class="nav-item {{ request()->path() == 'companies' ? 'active' : '' }} d-none d-lg-block">
                    <a class="nav-link d-flex aling-items-center" style="gap:10px" href="{{ url('/companies') }}">
                        {{-- <i class="bx bx-buildings" style="font-size:18px"></i> --}}
                        Companies    
                    </a>
                </li>
                <li class="nav-item {{ request()->path() == 'pricing' ? 'active' : '' }} d-none d-lg-block">
                    <a class="nav-link d-flex aling-items-center" style="gap:10px" href="{{ url('/pricing') }}">
                        {{-- <i class="bx bx-dollar" style="font-size:18px"></i> --}}
                        Pricing
                    </a>
                </li>
                <li class="nav-item {{ request()->path() == 'about-us' ? 'active' : '' }} d-none d-lg-block">
                    <a class="nav-link d-flex aling-items-center" style="gap:10px" href="{{ url('/about-us') }}">
                        {{-- <i class="bx bx-dollar" style="font-size:18px"></i> --}}
                        About Us
                    </a>
                </li>
                <li class="nav-item {{ request()->path() == 'news' ? 'active' : '' }} d-none d-lg-block">
                    <a class="nav-link d-flex aling-items-center" style="gap:10px" href="{{ url('/news') }}">
                        {{-- <i class="bx bx-dollar" style="font-size:18px"></i> --}}
                        Blogs
                    </a>
                </li>
                <li class="nav-item {{ request()->path() == '/faq' ? 'active' : '' }} d-none d-lg-block">
                    <a class="nav-link d-flex aling-items-center" style="gap:10px" href="{{ url('/faq') }}">
                        {{-- <i class="bx bx-dollar" style="font-size:18px"></i> --}}
                        FAQ
                    </a>
                </li>
            </ul>
        </div>
        <ul class="nav navbar-nav align-items-center ms-auto">
            {{-- <li class="nav-item {{ request()->path() == 'news' ? 'active' : '' }} d-none d-lg-block">
                <a class="nav-link d-flex aling-items-center" style="gap:10px" href="{{ url('/news') }}">
                    <i class="bx bx-news" style="font-size:18px" title="News & Update"></i>
                </a>
            </li> --}}
            @if(session()->get('userFullname'))
            <li class="nav-item dropdown dropdown-notification me-25">
                <a class="nav-link " href="#" data-bs-toggle="dropdown" aria-expanded="true">
                    <i class="bx bx-bell" style="font-size:18px"></i>
                    <span class="badge rounded-pill bg-danger badge-up">5</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end" data-bs-popper="none">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex">
                            <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                            <div class="badge rounded-pill badge-light-primary">6 New</div>
                        </div>
                    </li>
                    <li class="scrollable-container media-list ps"><a class="d-flex" href="#">
                            <div class="list-item d-flex align-items-start">
                                <div class="me-1">
                                    <div class="avatar"><img
                                            src="../../../app-assets/images/portrait/small/avatar-s-15.jpg"
                                            alt="avatar" width="32" height="32"></div>
                                </div>
                                <div class="list-item-body flex-grow-1">
                                    <p class="media-heading"><span class="fw-bolder">Congratulation Sam 🎉</span>winner!
                                    </p><small class="notification-text"> Won the monthly best seller badge.</small>
                                </div>
                            </div>
                        </a>
                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                        </div>
                        <div class="ps__rail-y" style="top: 0px; right: 0px;">
                            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                        </div>
                    </li>
                    <li class="dropdown-menu-footer"><a
                            class="btn btn-primary w-100 waves-effect waves-float waves-light" href="#">Read all
                            notifications</a></li>
                </ul>
            </li> 
            <li class="nav-item dropdown dropdown-user"><a class="nav-link dropdown-toggle dropdown-user-link"
                    id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="user-nav d-sm-flex d-none"><span
                            class="user-name fw-bolder">{{ session()->get('userFullname') }}</span><span
                            class="user-status">{{ session()->get('userRole') }}</span></div><span class="avatar"><img
                            class="round" src="{{ url('getimage/' . base64_encode(session()->get('userPhoto'))) }} }}"
                            alt="avatar" height="40" width="40"><span
                            class="avatar-status-online"></span></span>
                </a>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                    <a class="dropdown-item" href="{{ url('api/logout') }}"><i class="me-50" data-feather="power"></i>
                        Logout</a>
                </div>
            </li>
            @else
            <li class="nav-item {{ request()->path() == 'login' ? 'active' : '' }} my-auto">
                <a class="btn btn-secondary p-1" style="gap:10px;color:white !important" href="{{ url('/login') }}">
                    {{-- <i class="bx bx-arrow-from-left" style="font-size:18px"></i> --}}
                    Login
                </a>
            </li>
            @endif
        </ul>
    </div>
</nav>
