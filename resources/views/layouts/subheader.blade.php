<style>
    #main-menu-navigation li a {
        color: black !important
    }
</style>

<div class="horizontal-menu-wrapper" style="position:unset;margin-top:6em">
    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-shadow menu-border container-xxl"
        role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav" style="position:unset">
        <div class="navbar-container main-menu-content" data-menu="menu-container" style="background:#F8F8F8">
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                {{-- <li class="dropdown nav-item" data-menu="dropdown"><a
                        class="dropdown-toggle nav-link d-flex align-items-center" href="index.html"
                        data-bs-toggle="dropdown">
                        <i class="bx bx-home"></i>
                    <span data-i18n="Dashboards">Dashboards</span></a>
                    <ul class="dropdown-menu" data-bs-popper="none">
                        <li data-menu=""><a class="dropdown-item d-flex align-items-center"
                                href="dashboard-analytics.html" data-bs-toggle="" data-i18n="Analytics">
                            <i class="bx bx-cart"></i>    
                            <span data-i18n="Analytics">Analytics</span></a>
                        </li> 
                    </ul>
                </li> --}}
                <li class="nav-item {{ str_contains(request()->path(), 'users/profile') ? 'active' : '' }}">
                    <a class="d-flex align-items-center " href="{{ url('/users/profile') }}">
                        <i class="bx bx-user-circle"></i>
                        <span class="menu-title text-truncate">Account</span>
                    </a>
                </li> 
                <li class="nav-item {{ str_contains(request()->path(),'masterdata/products') ? 'active' : '' }}">
                    <a class="d-flex align-items-center " href="{{ url('/masterdata/products') }}">
                        <i data-feather="box"></i>
                        <span class="menu-title text-truncate">Product Management</span>
                    </a>
                </li> 
                <li class="nav-item {{ str_contains(request()->path(),'tender-management') ? 'active' : '' }}">
                    <a class="d-flex align-items-center " href="{{ url('/tender-management') }}">
                        <i data-feather="box"></i>
                        <span class="menu-title text-truncate">Tender Management</span>
                    </a>
                </li> 
                <li class="nav-item {{ request()->path() == 'bids' ? 'active' : '' }}">
                    <a class="d-flex align-items-center " href="{{ url('/bids') }}">
                        <i class="bx bx-dollar-circle"></i>
                        <span class="menu-title text-truncate">Bids</span>
                    </a>
                </li>   

                <li class="dropdown nav-item ms-auto" data-menu="dropdown"><a
                        class="dropdown-toggle nav-link d-flex align-items-center" href="index.html"
                        data-bs-toggle="dropdown">
                        <i class="bx bx-home"></i>
                    <span data-i18n="Dashboards">Admin Management</span></a>
                    <ul class="dropdown-menu" data-bs-popper="none">
                        <li data-menu="">
                            <a class="dropdown-item d-flex align-items-center"
                                href="{{ url('/users') }}" data-bs-toggle="" data-i18n="Analytics">
                                <i class="bx bx-user-plus"></i>    
                                <span data-i18n="Analytics">User Management</span>
                            </a>
                        </li> 
                        <li data-menu="">
                            <a class="dropdown-item d-flex align-items-center"
                                href="{{ url('/news/list') }}" data-bs-toggle="" data-i18n="Analytics">
                                <i class="bx bx-news"></i>    
                                <span data-i18n="Analytics">News Management</span>
                            </a>
                        </li> 
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>