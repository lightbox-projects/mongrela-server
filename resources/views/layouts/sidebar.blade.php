@if (!(session()->get('login')))
    <style>
        .main-menu {
            display: none !important
        }

        .vertical-layout.vertical-menu-modern.menu-collapsed .app-content, .vertical-layout.vertical-menu-modern.menu-collapsed .footer {
            margin-left: unset !important
        }

        .header-navbar {
            margin-left: unset !important
        }
    </style>
@endif

<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow  @yield('sidebar-size')" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand"
                    href="../../../html/ltr/vertical-menu-template/index.html">
                    <span class="brand-logo">
                        {{-- <img src="{{ asset('logo-color.png') }}" /> --}}
                    </span>
                    <h2 class="brand-text">
                        <div class="d-flex w-100 align-items-center justify-content-between" style="margin-left: -50px">
                            {{-- <img src="{{ asset('logo-color.png') }}" style="width:100%;max-width:40px" class="me-1" /> --}}
                            {{-- <h6 class="mb-0 text-wrap fw-bolder"><span style="color:#0F6CB5">Human</span><span style="color:#F26E22">Techno</span></h6> --}}
                        </div>
                    </h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                        class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i
                        class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc"
                        data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        @if (session()->get('login'))
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="navigation-header"><span data-i18n="Apps &amp; Pages">Dashboard</span><i
                    data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ str_contains(request()->path(), 'users/profile') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/users/profile') }}">
                    <i class="bx bx-user-circle"></i>
                    <span class="menu-title text-truncate">Account</span>
                </a>
            </li> 
            <li class="nav-item {{ str_contains(request()->path(), 'masterdata/products') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/masterdata/products') }}">
                    <i data-feather="box"></i>
                    <span class="menu-title text-truncate">Product Management</span>
                </a>
            </li>
            <li class="nav-item {{ str_contains(request()->path(), 'tender-management') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/tender-management') }}">
                    <i class="bx bx-package"></i>
                    <span class="menu-title text-truncate">Tender Management</span>
                </a>
            </li> 
            <li class="nav-item {{ request()->path() == 'bids' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/bids') }}">
                    <i class="bx bx-dollar-circle"></i>
                    <span class="menu-title text-truncate">My Bids</span>
                </a>
            </li>  
            
            @if (session()->get('userRole') == 'superadmin')
            <li class="navigation-header"><span data-i18n="Apps &amp; Pages">Admin</span><i
                    data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ request()->path() == 'users' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/users') }}">
                    <i class="bx bx-user-plus"></i>
                    <span class="menu-title text-truncate">User Management</span>
                </a>
            </li>
            <li class="nav-item {{ request()->path() == 'news/list' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/news/list') }}">
                    <i class="bx bx-news"></i>
                    <span class="menu-title text-truncate">News Management</span>
                </a>
            </li>
            <li class="nav-item {{ request()->path() == 'testimony' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/testimony') }}">
                    <i class="bx bx-group"></i>
                    <span class="menu-title text-truncate">Testimony</span>
                </a>
            </li>
            <li class="nav-item {{ request()->path() == 'faq/list' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/faq/list') }}">
                    <i class="bx bx-group"></i>
                    <span class="menu-title text-truncate">FAQ</span>
                </a>
            </li>
            <li class="navigation-header"><span data-i18n="Apps &amp; Pages">Masterdata</span><i
                    data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ request()->path() == 'category' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/category') }}">
                    <i class="bx bx-package"></i>
                    <span class="menu-title text-truncate">Tender Categories</span>
                </a>
            </li>
            <li class="nav-item {{ request()->path() == 'product-category' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/product-category') }}">
                    <i class="bx bx-package"></i>
                    <span class="menu-title text-truncate">Product Category</span>
                </a>
            </li>
            @endif
        </ul>
        @endif
    </div>
</div>
