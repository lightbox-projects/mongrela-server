<style>
    html body {
        height: unset !important;
    }

    .navigation-main .nav-item.active i {
        color: white !important
    }
    
    .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
        background: #F26122 !important; 
    }
    .nav-pills .nav-link.active {
        border: unset !important
    }
    .text-primary {
        color:#F26122 !important;
    }
    .nav-tabs .nav-link.active {
        color:#F26122 !important;
    }
    .nav-tabs .nav-link {
        color:#666666 !important;
    }

    .nav-tabs .nav-link:after {
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 3px;
        background: #cccccc !important;
        transition: transform 0.3s;
        transform: translate3d(0, 150%, 0);
    }
    .nav-tabs .nav-link.active:after {
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 3px;
        background: #F26122 !important;
        transition: transform 0.3s;
        transform: translate3d(0, 0, 0);
    }
    .nav-tabs .nav-link:after {
        transform: translate3d(0, 0, 0);
    }

    .border-primary {
        border: 2px solid #F26122 !important;
        border-radius: 5px !important
    }
    
    .bg-primary {
        background: #F26122 !important;
    }
    .btn-primary {
        background: #F26122 !important;
        border-color: #F26122 !important;
    }

    .btn-muted {
        background: #6C757D !important;
        color: white !important
    }
    
    .page-item.active .page-link {
        background-color: #F26122 !important;
    }

    .btn-outline-primary {
        border-color: #F26122 !important;
        color:#F26122 !important;
    }

    .bs-stepper .bs-stepper-header .step.active .step-trigger .bs-stepper-box {
        background-color: #F26122 !important
    }
    .bs-stepper .bs-stepper-header .step.active .step-trigger .bs-stepper-label .bs-stepper-title {
        color: #F26122 !important 
    }

    .form-check-primary .form-check-input:checked {
        background-color: #F26122 !important
    }

    ::-webkit-scrollbar {
        width: 10px;
        height: 5px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        background: #f1f1f1;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #F26122;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: #555;
    }

    .navbar-container {
        background-color: #F26122
    }

    .navbar-container a {
        color: white !important
    }

    .navbar-container li:hover a, .navbar-container li.active a, .dropdown-menu a {
        color: black !important
    }
 
    .btn-relief-primary {
        background-color: #F26122 !important;
        
    }

    .text-secondary {
        color: #391A0D !important
    }

    .bg-secondary {
        background: #391A0D !important;
    }
    .btn-secondary {
        background: #391A0D !important;
        border-color: #391A0D !important;
    }

    .bg-dark {
        background: #2B2F33 !important
    }
    .bg-muted {
        background: #969696 !important
    }

    .horizontal-menu .header-navbar.navbar-horizontal ul#main-menu-navigation > li.active > a {
        background: #F26122 !important;
        color: white !important;
        border: unset !important;
        box-shadow: unset !important
    }
    
    .main-menu.menu-light .navigation > li.active > a {
        background: #F26122 !important;
        box-shadow: unset !important;

    }

    /* body .app-content.content {
        background: #391A0D !important
    } */
</style>
