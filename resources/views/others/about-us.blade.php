@extends('layouts.layout1-no-container')

@section('css_section')

@endsection

@section('page_title')
    About Us
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')

    <div class="">

        <div>
            <h1 class="text-center my-2 fw-bolder" style="color:#F26122">About Us</h1>
        </div>

        <img src="{{ asset('images/Banner 2 1.png') }}" class="w-100" />

        <div class="container-xxl mt-3">
            <h1 class="text-center fw-bolder" style="color:#F26122">Welcome to Qompair</h1>
            <h5 class="text-center fw-bolder">Building Connections, Creating Opportunities</h5>

            <p class="fw-bold mt-5">
                Qompair is a platform to collaborate between project host and bidder. <br />
                With a wide variation of functions such as product list, tender system, bid review submission and many more,
                <br />
                we aim to be the easiest method as a 3rd party to connecting between a project host and bidder thus creating
                a deal and cooperation <br />
            </p>

            <h1 class="fw-bolder mt-3" style="color:#F26122">Why Qompair</h1>
            <p class="fw-bold mt-1">
                Because Qompair wants to bring best value to your business for both
                owners and service / product providers. From post project requirements,
                review submissions, comparing price and quality, all within 3 quick easy steps. All online to make easy
                connection
            </p>

            <h1 class="fw-bolder mt-5" style="color:#F26122">Our Keypoints</h1>

            <h3 class="fw-bolder mt-2">Enhanced Collaboration</h3>
            <p class="fw-bold">
                Qompair brings project hosts and bidders together on a single platform, promoting synergy and collaboration
                between the two parties.
                This collaborative environment streamlines the entire process of initiating and managing projects.
            </p>

            <h3 class="fw-bolder mt-4">Diverse Functionality</h3>
            <p class="fw-bold">
                We take pride in offering a wide range of features designed to cater to all aspects of the project
                lifecycle.
                From comprehensive product listings to an efficient tender system, our platform empowers users with the
                tools they need for seamless project execution.
            </p>

            <h3 class="fw-bolder mt-4">Simplified Bid Review Process</h3>
            <p class="fw-bold">
                With Qompair, bid review submission becomes a breeze. Our intuitive interface and straightforward process
                make it effortless for bidders to submit their
                proposals and for project hosts to evaluate them efficiently.
            </p>

            <h3 class="fw-bolder mt-4">Trusted Third-Party Intermediary</h3>
            <p class="fw-bold">
                Trust and security are paramount to us. As a reliable third-party intermediary, Qompair ensures fairness,
                transparency,
                and confidentiality throughout every project collaboration.
            </p>

            <h3 class="fw-bolder mt-4">User-Centric Approach</h3>
            <p class="fw-bold">
                We are committed to providing the best user experience possible. Our platform is designed with a
                user-centric approach, focusing on ease of use,
                responsiveness, and an intuitive layout.
            </p>

            <h3 class="fw-bolder mt-4">Empowering Cooperation</h3>
            <p class="fw-bold">
                By connecting project hosts and bidders seamlessly, Qompair empowers users to forge valuable partnerships,
                leading to successful project outcomes and long-lasting cooperation.
            </p>

            <h3 class="fw-bolder mt-4">Data Security and Privacy</h3>
            <p class="fw-bold">
                The security of your data is of utmost importance to us. Qompair implements robust security measures to
                safeguard all sensitive information,
                giving users peace of mind.
            </p>

            <h3 class="fw-bolder mt-4">Responsive Support</h3>
            <p class="fw-bold">
                Our dedicated support team is always ready to assist users with any queries or concerns they may have,
                ensuring a smooth
                and hassle-free experience on our platform.
            </p>


            <p class="fw-bolder py-4 mt-5 text-center">
                Qompair is not just another collaboration platform; it's a catalyst for growth, efficiency, and success.
                Whether you're a project host looking for the perfect bidder or a bidder seeking exciting opportunities,
                Qompair is your trusted partner in the journey of creating exceptional deals and fostering fruitful
                cooperation. Join us today and unlock the true potential of collaboration!
            </p>

        </div>

        @include('home.post-a-project')

    </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
    </script>
@endsection
