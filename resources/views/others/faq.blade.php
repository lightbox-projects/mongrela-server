@extends('layouts.layout1-no-container')

@section('css_section')

@endsection

@section('page_title')
    About Us
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')

    <div class="">

        <div>
            <h1 class="text-center my-5 py-4 fw-bolder" style="color:#F26122">Frequently Asked Question</h1>
        </div>

        <div class="container-xxl mt-3">
            <h1 class="text-center fw-bolder" style="color:#F26122">Welcome to Qompair</h1>
            <h5 class="text-center fw-bolder">Building Connections, Creating Opportunities</h5>

            <p class="fw-bolder py-4 mt-5 text-center">
                Qompair is not just another collaboration platform; it's a catalyst for growth, efficiency, and success.
                Whether you're a project host looking for the perfect bidder or a bidder seeking exciting opportunities,
                Qompair is your trusted partner in the journey of creating exceptional deals and fostering fruitful
                cooperation. Join us today and unlock the true potential of collaboration!
            </p>

            <div class="accordion pb-5" id="accordionExample">
                @foreach ($faq as $key => $item)
                <div class="accordion-item" style="border: 1px solid #F26122;{{ $key == 0 ? '' : 'border-top:unset !important' }}">
                    <h2 class="accordion-header" id="heading{{$key}}">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
                            {{$item->faq_title}}
                        </button>
                    </h2>
                    <div id="collapse{{$key}}" class="accordion-collapse collapse" aria-labelledby="heading{{$key}}"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            {{$item->faq_description}}
                        </div>
                    </div>
                </div> 
                @endforeach
            </div>

        </div>

    </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
    </script>
@endsection
