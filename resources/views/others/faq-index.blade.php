@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    FAQ Management
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3" style="min-height:60vh">

        <div class="d-flex justify-content-between flex-wrap align-items-center" style="gap:10px">
            <div>
                <h3 class="fw-bolder">Frequently Asked Question</h3>
                <p class="mb-0">
                    A role provided access to predefined menus and features so that depending <br>
                    on assigned role an administrator can have access to what he need
                </p>
            </div>
        </div>

        <hr class="my-2" />

        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped" id="table">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Title</th>
                            <th>Description</th>
                            {{-- <th>Published</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="modal fade" id="frmbox" tabindex="-1" aria-labelledby="frmbox-title" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-transparent">
                        <h5 class="modal-title">Form FAQ</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body px-2 pb-2">
                        <!-- form -->
                        <form id="frm" class="row gy-1 gx-2">
                            @csrf
                            <input type="hidden" name="id" id="faq_id">
                            <div class="col-12">
                                <label class="form-label" for="modalAddCardNumber">Title</label>
                                <div class="input-group input-group-merge">
                                    <input id="faq_title" name="inp[faq_title]" class="form-control add-credit-card-mask"
                                        type="text" />
                                </div>
                            </div>
                            <div class="col-12">
                                <label class="form-label" for="modalAddCardNumber">Description</label>
                                <div class="input-group input-group-merge">
                                    <textarea id="faq_description" name="inp[faq_description]" rows="3" class="form-control add-credit-card-mask"
                                        type="text" >
                                    </textarea>
                                </div>
                            </div> 
                            <div class="col-12 text-center">
                                <a class="btn btn-primary me-1 mt-1" onclick="save()">Submit</a>
                                <button type="reset" class="btn btn-outline-secondary mt-1" data-bs-dismiss="modal"
                                    aria-label="Close">
                                    Cancel
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
        let dTable = $('#table')


        $(function() {
            dTable = $('#table').DataTable({
                ajax: {
                    url: "{{ url('api/faq/dt') }}",
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: false,
                        className: 'text-center'
                    }, 
                    {
                        data: 'faq_title',
                        name: 'faq_title'
                    },
                    {
                        data: 'faq_description',
                        name: 'faq_description'
                    }, 
                ],
                buttons: [{
                    text: 'Add New',
                    className: 'add-new btn btn-primary',
                    action: function(e, dt, node, config) {
                        addnew()
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }],
                order: [
                    [1, 'desc']
                ],  
            });

            $('.dataTables_filter input[type=search]').attr('placeholder', 'Search').attr('class',
                'form-control');
            $('.dataTables_filter select[name=table_length]').attr('class', 'form-select form-select-sm');
        })

        function addnew() {
            $('#frmbox').modal('show');
            $('#frm')[0].reset();
            $('#faq_id').val(null)
        }

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);
                $.ajax({
                    url: '{{ url('api/faq') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            $("#frmbox").modal('hide');
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        function edit(id) {
            $.ajax({
                url: '{{ url('api/faq/') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                        'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            $('#' + key).val(value);
                        }
                    });
                    if(e.news_content) fullEditor.root.innerHTML = e.news_content;
                    $("#frmbox").modal('show');
                }
            });
        }

        function del(id) {
            if (confirm('Are you sure to delete this data?')) {
                $.ajax({
                    url: '{{ url('api/faq/') }}' + '/' + id,
                    type: 'delete',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }
    </script>
@endsection
