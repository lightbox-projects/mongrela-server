@extends('layouts.layout1')

@section('css_section')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.5/pagination.css">
<style>
    .paginationjs .paginationjs-pages li>a {
        border-radius: 5px !important;
        margin: 0 5px;
        height: 38px !important;
        width: 38px !important;
        display: flex;
        align-items: center;
        justify-content: center;
        font-weight: 700;
        font-size: 18px
    }

    .paginationjs .paginationjs-pages li {
        border: unset !important
    }

    .paginationjs .paginationjs-pages li.active>a {
        border: 1px solid #F26122 !important;
        background: unset !important;
        color: #F26122 !important
    }
</style>
@endsection

@section('page_title')
    Bids
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3">
        <div class="d-flex justify-content-between flex-wrap align-items-center" style="gap:10px">
            <div>
                <h3 class="fw-bolder">Bids</h3>
                <p class="mb-0">
                    Tender that you have applied will be shown here.
                </p>
            </div>
        </div> 

        <div class="mt-2" style="min-height:50vh">
            <div class="row" style="gap:20px 0;" id="data-container">
            </div>
            <div class="d-flex mt-3">
                <div class="mx-auto" id="pagination-container"></div>
            </div>
        </div>
        
    </div>

    <input type="hidden" id="total_active" value="{{$total_active}}" />

@endsection

@section('js_section')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.5/pagination.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment-with-locales.min.js" integrity="sha512-42PE0rd+wZ2hNXftlM78BSehIGzezNeQuzihiBCvUEB3CVxHvsShF86wBWwQORNxNINlBPuq7rG4WWhNiTVHFg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        var select = $('.select2')

        $('#pagination-container').pagination({
            dataSource: Array(parseInt($('#total_active').val())).fill(0).map((_, i) => i * i),
            pageSize: 9,
            callback: function(data, pagination) {
                $('#data-container').html('');
                let page = pagination.pageNumber
                let pageRange = pagination.pageSize

                let formData = new FormData()
                formData.append('page', page)
                formData.append('pageRange', pageRange) 
                formData.append('tender_bid', true) 

                $.ajax({
                    url: '{{ url('api/tender-management/list') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        let data = e.data
                        data.map(item => {
                            let start = moment(item.prj_tender_start)
                            let end = moment(item.prj_tender_end)

                            let status = 'upcoming'
                            if(moment().diff(end, 'days') > 0) {
                                status = 'expired'
                            } else if(moment().diff(start, 'days') > 0) {
                                status = 'ongoing'
                            }

                            var html = `
                                <div class="col-lg-4">
                                    <div class="card mb-0">
                                        <div class="card-body ">
                                            ${item.owner.company ? `
                                                <div class="d-flex align-items-center" style="gap:10px">
                                                    <img src="${`{{ url('getimage') }}/` + btoa(item.owner.company.company_photo)}" 
                                                        class="w-100" style="max-height:30px;max-width:50px;object-fit:contain" />
                                                    <h3 class="mb-0 fw-bolder text-secondary">${item.owner.company.company_name}</h3>
                                                </div>
                                            ` : ''}
                                            <div class="mt-2">
                                                <h1 class="fw-bolder text-secondary h3">${item.prj_name}</h1>
                                                <h6 style="
                                                    display: -webkit-box;
                                                    -webkit-line-clamp: 4;
                                                    -webkit-box-orient: vertical;
                                                    overflow: hidden;
                                                ">
                                                    ${item.prj_description}
                                                </h6>
                                                <h6 class="mb-0 mt-2 text-secondary">
                                                    Tags: ${item.prj_tags}
                                                </h6>
                                            </div>

                                            <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                                                <i class="bx bx-package" style="font-size:30px"></i>
                                                <div>
                                                    <h6 style="margin-bottom: .25em" class="fw-bolder">Category</h6>
                                                    <div class="badge bg-primary">
                                                        ${item.category.category_name}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                                                        <i class="bx bx-calendar" style="font-size:30px"></i>
                                                        <div>
                                                            <h6 style="margin-bottom: .25em" class="fw-bolder">Starts on</h6>
                                                            <h6 class="fw-bolder text-dark">
                                                                ${item.prj_tender_start}
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                                                        <i class="bx bx-calendar" style="font-size:30px"></i>
                                                        <div>
                                                            <h6 style="margin-bottom: .25em" class="fw-bolder">Ends on</h6>
                                                            <h6 class="fw-bolder text-dark">
                                                                ${item.prj_tender_end}
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @php
                                                $date_now = date("Y-m-d");
                                            @endphp

                                            <div class="d-flex flex-wrap align-items-center mt-1" style="gap:10px">
                                                ${status == 'ongoing' ? `
                                                    <button class="btn btn-success">
                                                        Tender Ongoing
                                                    </button>
                                                ` : ''}
                                                ${status == 'expired' ? `
                                                    <button class="btn btn-muted">
                                                        Tender Expired
                                                    </button>
                                                ` : ''}
                                                ${status == 'upcoming' ? `
                                                    <button class="btn btn-muted">
                                                        Tender Upcoming
                                                    </button>
                                                ` : ''}
                                                <a class="btn btn-primary" href="{{url('tenders/')}}/${item.prj_id}">
                                                    View
                                                </a>
                                                <a class="btn btn-primary" href="{{url('tenders/')}}/${item.prj_id}/bid">
                                                    My Bid
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                            $('#data-container').append(html);
                        })
                    }
                });

            }
        })
    </script>
@endsection
