@extends('layouts.layout1')

@section('page_title')
    Register
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="d-flex" style="">
        <div class="m-auto">
            <div class="auth-inner row m-0">
                <!-- Left Text-->
                <div class="d-none d-lg-flex col-lg align-items-center p-5">
                    <div class="w-100 d-lg-flex align-items-center justify-content-center px-5"><img class="img-fluid"
                            src="{{ asset('themes/vuexy/images/pages/login-v2.svg') }}" alt="Login V2" /></div>
                </div>
                <!-- /Left Text-->
                <!-- Login-->
                <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                        <h2 class="card-title fw-bold mb-1">Register to Qompair</h2>
                        <p class="card-text mb-2">Please fill in the form to register new account.</p>
                        <form class="auth-login-form mt-2" action="{{ url('api/register') }}" method="POST">
                            @csrf
                            <div class="mb-1">
                                <label class="form-label" for="username">Fullname</label>
                                <input class="form-control" placeholder="Fullname" id="user_name" type="text" name="user_name"
                                    aria-describedby="login-email" autofocus=""
                                    tabindex="1" />
                            </div>
                            <div class="mb-1">
                                <label class="form-label" for="email">Email</label>
                                <input class="form-control" id="user_email" type="text" name="user_email"
                                    placeholder="john@example.com" aria-describedby="login-email" autofocus=""
                                    tabindex="1" />
                            </div> 
                            <div class="mb-1">
                                <label class="form-label" for="email">Phone</label>
                                <input class="form-control" id="user_phone" type="text" name="user_phone"
                                    placeholder="john@example.com" aria-describedby="login-email" autofocus=""
                                    tabindex="1" />
                            </div>
                            <div class="mb-1">
                                <div class="d-flex justify-content-between">
                                    <label class="form-label" for="password">Password</label> 
                                </div>
                                <div class="input-group input-group-merge form-password-toggle">
                                    <input class="form-control form-control-merge" id="password" type="password"
                                        name="password" placeholder="············" aria-describedby="login-password"
                                        tabindex="2" /><span class="input-group-text cursor-pointer"><i
                                            data-feather="eye"></i></span>
                                </div>
                            </div> 
                            <button class="btn btn-primary w-100" type="submit" tabindex="4">REGISTER</button>
                        </form>
                        {{-- <p class="text-center mt-2"><span>New on our platform?</span><a href="auth-register-cover.html"><span>&nbsp;Create an account</span></a></p>
                                <div class="divider my-2">
                                    <div class="divider-text">or</div>
                                </div>
                                <div class="auth-footer-btn d-flex justify-content-center"><a class="btn btn-facebook" href="#"><i data-feather="facebook"></i></a><a class="btn btn-twitter white" href="#"><i data-feather="twitter"></i></a><a class="btn btn-google" href="#"><i data-feather="mail"></i></a><a class="btn btn-github" href="#"><i data-feather="github"></i></a></div> --}}
                    </div>
                </div>
                <!-- /Login-->
            </div>
        </div>
    </div>
@endsection
