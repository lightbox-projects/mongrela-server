@extends('layouts.layout1')

@section('page_title')
    Login
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')

    <div class="d-flex" style="">
        <div class="m-auto">
            <div class="auth-inner row m-0">
                <!-- Left Text-->
                {{-- <div class="d-none d-lg-flex col-lg align-items-center p-5">
                    <div class="w-100 d-lg-flex align-items-center justify-content-center px-5"><img class="img-fluid"
                            src="{{ asset('themes/vuexy/images/pages/login-v2.svg') }}" alt="Login V2" /></div>
                </div> --}}
                <!-- /Left Text-->
                <!-- Login-->
                <div class="d-flex col-lg-12 align-items-center auth-bg px-2 p-lg-5" style="min-height: 500px">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                        <h2 class="card-title fw-bold mb-1">Welcome to Qompair</h2>
                        <p class="card-text mb-2">Please sign-in to your account and start the adventure</p>
                        <form class="auth-login-form mt-2" action="{{ url('api/login') }}" method="POST">
                            @csrf
                            <div class="mb-1">
                                <label class="form-label" for="username">Username</label>
                                <input class="form-control" id="username" type="text" name="username"
                                    placeholder="john@example.com" aria-describedby="login-email" autofocus=""
                                    tabindex="1" />
                            </div>
                            <div class="mb-1">
                                <div class="d-flex justify-content-between">
                                    <label class="form-label" for="password">Password</label>
                                    {{-- <a
                                        href="auth-forgot-password-cover.html">
                                        <small>Forgot Password?</small>
                                    </a> --}}
                                </div>
                                <div class="input-group input-group-merge form-password-toggle">
                                    <input class="form-control form-control-merge" id="password" type="password"
                                        name="password" placeholder="············" aria-describedby="login-password"
                                        tabindex="2" /><span class="input-group-text cursor-pointer"><i
                                            data-feather="eye"></i></span>
                                </div>
                            </div>
                            <div class="mb-1 d-flex flex-wrap align-items-center">
                                <div class="form-check">
                                    <input class="form-check-input" id="remember-me" type="checkbox" tabindex="3" />
                                    <label class="form-check-label" for="remember-me"> Remember Me</label>
                                </div>
                                <a class="ms-auto"
                                    href="{{url('/register')}}">
                                    <small>Don't have account?</small>
                                </a>
                            </div>
                            <button class="btn btn-primary w-100" type="submit" tabindex="4">LOGIN</button>
                        </form>
                        {{-- <p class="text-center mt-2"><span>New on our platform?</span><a href="auth-register-cover.html"><span>&nbsp;Create an account</span></a></p>
                                <div class="divider my-2">
                                    <div class="divider-text">or</div>
                                </div>
                                <div class="auth-footer-btn d-flex justify-content-center"><a class="btn btn-facebook" href="#"><i data-feather="facebook"></i></a><a class="btn btn-twitter white" href="#"><i data-feather="twitter"></i></a><a class="btn btn-google" href="#"><i data-feather="mail"></i></a><a class="btn btn-github" href="#"><i data-feather="github"></i></a></div> --}}
                    </div>
                </div>
                <!-- /Login-->
            </div>
        </div>
    </div>
@endsection
