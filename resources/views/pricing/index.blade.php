@extends('layouts.layout1')

@section('css_section')
<style>
    .card-pricing {
        display: flex;
        background: #F1F1F1;
        border: 1px solid grey;
        border-radius: 10px;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        height: 100%;
        padding: 7em 2em;
    }
    .card-pricing:hover {
        background: #F26122 !important;
        color: white !important;
    }
    .card-pricing:hover h1 {
        color: white !important;
    }
    .card-pricing:hover .btn-primary {
        color: #F26122 !important;
        background: white !important
    }
</style>
@endsection

@section('page_title')
    Pricing
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="py-5 mb-5">

        <div class="d-flex justify-content-between flex-wrap align-items-center mb-4" style="gap:10px">
            <div class="text-center m-auto">
                <h3 class="fw-bolder h1">Pricing</h3>
                <p class="mb-0">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, b
                </p>
            </div>
        </div>
        
        <div class="row" style="gap:10px 0">
            <div class="col-lg-3">
                <div class="card-pricing">
                    <div class="text-center">
                        <h1 class="fw-bolder display-5">Basic</h1>
                        <h1 class="fw-bolder">FREE</h1>
                        <p>
                            Package description role provided access to predefined menus and features so that depending
                        </p>
                    </div>

                    <button class="mt-1 btn btn-primary">
                        Order Now
                    </button>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card-pricing">
                    <div class="text-center">
                        <h1 class="fw-bolder display-5">Premium</h1>
                        <h1 class="fw-bolder">$12</h1>
                        <p>
                            Package description role provided access to predefined menus and features so that depending
                        </p>
                    </div>

                    <button class="mt-1 btn btn-primary">
                        Order Now
                    </button>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card-pricing">
                    <div class="text-center">
                        <h1 class="fw-bolder display-5">Standard</h1>
                        <h1 class="fw-bolder">$24</h1>
                        <p>
                            Package description role provided access to predefined menus and features so that depending
                        </p>
                    </div>

                    <button class="mt-1 btn btn-primary">
                        Order Now
                    </button>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card-pricing">
                    <div class="text-center">
                        <h1 class="fw-bolder display-5">Exclusive</h1>
                        <h1 class="fw-bolder">$36</h1>
                        <p>
                            Package description role provided access to predefined menus and features so that depending
                        </p>
                    </div>

                    <button class="mt-1 btn btn-primary">
                        Order Now
                    </button>
                </div>
            </div>
        </div>

        <p class="text-center mt-4">
            Where does it come from?
Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum
        </p>
        
    </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
    </script>
@endsection
