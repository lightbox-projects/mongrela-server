@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    Product List
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3" style="min-height:60vh">

        <div class="d-flex justify-content-between flex-wrap align-items-center mb-4 mt-2" style="gap:10px">
            <div>
                <h3 class="fw-bolder">Products Management</h3>
                <p class="mb-0" style="max-width:600px">
                    Product management feature facilitates centralized storage and management of product information
                    And can be used for tender/project setup.
                </p>
                @if(!$company)
                <p class="mt-2">*You need to fill in the 
                    <a class="text-primary fw-bolder" href="{{url('/users/profile/'.session()->get('userId').'/company')}}">company profile</a> information.
                </p>
                @endif
            </div>
        </div>

        <div class="card card-body">
            <h4 class="fw-bolder">Filters</h4>
            <div class="row" style="gap:10px 0">
                <div class="col-lg-3">
                    <label>Product Name</label>
                    <input class="form-control" placeholder="Name" id="name-filter" />
                </div> 
                <div class="col-lg-3">
                    <label>Product Model</label>
                    <input class="form-control" placeholder="Model" id="model-filter" />
                </div> 
                <div class="col-lg-3">
                    <label>Product Brand</label>
                    <input class="form-control" placeholder="Brand" id="brand-filter" />
                </div> 
                <div class="col-lg-3">
                    <label>Product Application</label>
                    <input class="form-control" placeholder="Application" id="application-filter" />
                </div> 
            </div>
        </div>

        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped" id="table">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th>PRODUCT NAME</th>
                            <th>APPLICATION</th>
                            <th>FINISHING</th>
                            <th>BRAND</th>
                            <th>MODEL</th>
                            <th>ACCESSORIES</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> 
@endsection

@section('js_section')
    <script>
        var dTable = $('#table'),
            select = $('.select2')

        // List datatable
        $(function() {
            dTable = $('#table').DataTable({
                ajax: {
                    url: "{{ url('api/masterdata/products/list') }}",
                    type: 'post',
                    data: function(d) {
                        d.model = $('#model-filter').val()
                        d.brand = $('#brand-filter').val()
                        d.application = $('#application-filter').val()
                        d.name = $('#name-filter').val()
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                dom: '<"d-flex justify-content-between align-items-center header-actions mx-2 row mt-75"' +
                    '<"col-sm-12 col-lg d-flex justify-content-center justify-content-lg-start custom-button">' +
                    '<"col-sm-12 col-lg-auto ps-xl-75 ps-0"<"dt-action-buttons d-flex align-items-center justify-content-center justify-content-lg-end flex-lg-nowrap flex-wrap"<"me-1"f>l B>>' +
                    '>t' +
                    '<"d-flex justify-content-between mx-2 row mb-1"' +
                    '<"col-sm-12 col-md-6"i>' +
                    '<"col-sm-12 col-md-6"p>' +
                    '>',
                columns: [{
                        data: 'action'
                    },
                    {
                        data: 'pr_name'
                    },
                    {
                        data: 'pr_application'
                    },
                    {
                        data: 'pr_finishing'
                    },
                    {
                        data: 'pr_brand'
                    },
                    {
                        data: 'pr_model'
                    },
                    {
                        data: 'pr_accessories'
                    },
                ],
                order: [
                    [1, 'desc']
                ],
            });

            @if($company)
            $('.custom-button').append(`
                <div class="d-flex flex-wrap" style="gap:10px"> 
                    <button class="btn btn-primary font-weight-semibold text-nowrap" onclick="addnew()">
                        <i class="bx bx-plus"></i> <span class="d-none d-lg-inline-block">Add Single Product</span>
                    </button>
                </div>
            `)
            @endif

            $('#table_filter input').attr('placeholder', 'Search Anything')
            // $('#table_filter').hide()
        })

        function addnew() {
            window.location.href = "{{ url('masterdata/products/new') }}";
        }

        function edit(code) {
            window.location.href = "{{ url('masterdata/products/') }}/" + code + '/form';
        }

        function delF(id) {
            if (confirm('Are you sure to delete this data?')) {
                $.ajax({
                    url: '{{ url('api/masterdata/products/') }}' + '/' + id,
                    type: 'delete',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            dTable.draw();
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        $('#model-filter').change(function() {
            dTable.draw()
        })
        $('#brand-filter').change(function() {
            dTable.draw()
        })
        $('#application-filter').change(function() {
            dTable.draw()
        })
        $('#name-filter').change(function() {
            dTable.draw()
        })
    </script>
@endsection
