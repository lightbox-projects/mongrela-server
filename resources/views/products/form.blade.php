@extends('layouts.layout1')

@section('css_section')
    <style>
        .preview {
            overflow: hidden;
            width: 200px;
            height: 200px;
            border: 1px solid red;
        }
    </style>
    <style>
        input, textarea, select {
            border: unset !important;
            border-bottom: 1px solid #d8d6de !important;
            border-radius: unset !important
        }
    </style>
@endsection

@section('page_title')
    Product Form
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3">

        <div class="card card-body">

            <div style="gap:10px" class="d-flex align-items-center flex-wrap">
                <h4 class="mb-0 me-auto">Product Form</h4>
                <button class="btn btn-primary" onclick="save()">
                    <i class="bx bx-save"></i> Save Form
                </button>
                @if ($id)
                    <button class="btn btn-danger" onclick="delF()">
                        <i class="bx bx-save"></i> Delete Product
                    </button>
                @endif
            </div>

            <form id="frm" class="mt-2">
                @csrf
                <input type="hidden" id="pr_id" name="id" />
                <div class="row" style="gap: 10px 0">
                    <div class="col-lg-4">
                        <label class="form-label">Product Name</label>
                        <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_name]"
                            id="pr_name">
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Product Application</label>
                        {{-- <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_application]"
                            id="pr_application"> --}}
                        <select id="pr_application" name="inp[pr_application]" class="select2">
                            @foreach ($category as $item)
                                <option value="{{$item->pr_category_name}}">{{$item->pr_category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Product Brand</label>
                        <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_brand]"
                            id="pr_brand">
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Product Model</label>
                        <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_model]"
                            id="pr_model">
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Product Accessories</label>
                        <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_accessories]"
                            id="pr_accessories">
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Product Materials</label>
                        <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_materials]"
                            id="pr_materials">
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Product Finishing</label>
                        <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_finishing]"
                            id="pr_finishing">
                    </div>
                    <div class="col-lg-8">
                        <label class="form-label">Product Dimension</label>
                        <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_dimension]"
                            id="pr_dimension">
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Main Photo</label>
                        <input type="file" class="form-control" name="pr_main_photo" accept=".png, .jpg">
                        <a class="form-text text-success" id="pr_main_photo">Already uploaded.</a>
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Dimension Photo</label>
                        <input type="file" class="form-control" name="pr_dimension_photo" accept=".png, .jpg">
                        <a class="form-text text-success" id="pr_dimension_photo">Already uploaded.</a>
                    </div>
                    <div class="col-lg-4">
                        <label class="form-label">Accessories Photo</label>
                        <input type="file" class="form-control" name="pr_accessories_photo" accept=".png, .jpg">
                        <a class="form-text text-success" id="pr_accessories_photo">Already uploaded.</a>
                    </div>

                    <div class="col-lg-12">
                        <label class="form-label">Note</label>
                        <input type="text" placeholder='Input Someting' class="form-control" name="inp[pr_note]"
                            id="pr_note">
                    </div>

                    <div class="col-lg-12">
                        <label class="form-label">Product Description</label>
                        <textarea type="text" placeholder='Input Someting' class="form-control" name="inp[pr_description]"
                            id="pr_description"></textarea>
                    </div>

                </div>
            </form>

        </div>
    </div>
@endsection

@section('js_section')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://unpkg.com/cropperjs"></script>
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet" />
    <script>
        var select = $('.select2')

        @if ($id)
            edit('{{ $id }}')
        @endif

        function edit(id) {
            $('#pr_main_photo').hide();
            $.ajax({
                url: '{{ url('api/masterdata/products/') }}' + '/' + id,
                type: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    $.each(e, function(key, value) {
                        if ($('#' + key).hasClass("select2")) {
                            $('#' + key).val(value).trigger('change');
                        } else if ($('input[type=radio]').hasClass(key)) {
                            if (value != "") {
                                $("input[name='inp[" + key + "]'][value='" + value + "']").prop(
                                    'checked', true);
                                $.uniform.update();
                            }
                        } else if ($('input[type=checkbox]').hasClass(key)) {
                            if (value != null) {
                                var temp = value.split('; ');
                                for (var i = 0; i < temp.length; i++) {
                                    $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                        'checked', true);
                                }
                                $.uniform.update();
                            }
                        } else {
                            $('#' + key).val(value);
                        }
                    });
                    if (e.pr_main_photo) {
                        $('#pr_main_photo').show();
                        $('#pr_main_photo').attr('href', '{{ url('getimage') }}/' + btoa(e.pr_main_photo));
                    } else {
                        $('#pr_main_photo').hide();
                    }
                    if (e.pr_dimension_photo) {
                        $('#pr_dimension_photo').show();
                        $('#pr_dimension_photo').attr('href', '{{ url('getimage') }}/' + btoa(e.pr_dimension_photo));
                    } else {
                        $('#pr_dimension_photo').hide();
                    }
                    if (e.pr_accessories_photo) {
                        $('#pr_accessories_photo').show();
                        $('#pr_accessories_photo').attr('href', '{{ url('getimage') }}/' + btoa(e.pr_accessories_photo));
                    } else {
                        $('#pr_accessories_photo').hide();
                    }
                }
            });
        }

        function save() {
            if ($("#frm").valid()) {
                var formData = new FormData($('#frm')[0]);
                $.ajax({
                    url: '{{ url('api/masterdata/products') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    success: function(e) {
                        if (e.status == 'success') {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();
                            // setTimeout(function() {
                            //     window.location.href = '{{ url('masterdata/products') }}';
                            // }, 1000);
                        } else {
                            new Noty({
                                text: e.message,
                                type: 'info',
                                progressBar: true,
                                timeout: 1000
                            }).show();;
                        }
                    }
                });
            }
        }

        function delF() {
            Swal.fire({
                title: 'Are you sure?',
                text: 'You will not be able to recover this data!',
                showCancelButton: true,
                confirmButtonText: 'Proceed',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ url("api/masterdata/products/$id") }}',
                        type: 'post',
                        data: {
                            _method: 'delete'
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        success: function(e) {
                            if (e.status == 'success') {
                                new Noty({
                                    text: e.message,
                                    type: 'info',
                                    progressBar: true,
                                    timeout: 1000
                                }).show();
                                setTimeout(function() {
                                    window.location.href = '{{ url('masterdata/products') }}';
                                }, 1000);
                            } else {
                                new Noty({
                                    text: e.message,
                                    type: 'info',
                                    progressBar: true,
                                    timeout: 1000
                                }).show();;
                            }
                        }
                    });
                }
            })

        }
    </script>

    @include('products.components.modal-pict-upload')
@endsection
