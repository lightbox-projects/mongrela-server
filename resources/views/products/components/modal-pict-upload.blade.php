<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image Before Upload</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-between">
                    <div class="col-lg-8">
                        <img src="" id="sample_image" class="rounded-lg h-100" style="max-width: 100%;" />
                    </div>
                    <div class="col-lg-4 d-flex flex-column align-items-center">
                        <h3 class="fw-bolder">Preview</h3>
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="crop" class="btn btn-primary">Crop</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        var $modal = $('#modal');
        var image = document.getElementById('sample_image');
        var cropper;
        let input_active = ''
        var _URL = window.URL || window.webkitURL;

        let width = 0
        let height = 0

        $('input.form-control:file').change(function(event) {
            input_active = $(this).attr('name')

            var files = event.target.files;

            var done = function(url) {
                image.src = url;
                $modal.modal('show');
            };

            if ((file = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file);
                img.onload = function () {
                    _URL.revokeObjectURL(objectUrl);
                    width = this.width
                    height = this.height
                    // if(this.width == this.height){}
                };
                img.src = objectUrl;
            }

            if (files && files.length > 0) {
                reader = new FileReader();
                reader.onload = function(event) {
                    done(reader.result);
                };
                reader.readAsDataURL(files[0]);
            }
        });

        $modal.on('shown.bs.modal', function() {
            cropper = new Cropper(image, {
                aspectRatio: 1,
                viewMode: 2,
                preview: '.preview',
            });
            console.log($('.cropper-bg'))
            $('.cropper-bg').attr('style', `width:${width}px !important;height:${height}px !important`)
        }).on('hidden.bs.modal', function() {
            cropper.destroy();
            cropper = null;
        });

        $('#crop').click(function() {
            canvas = cropper.getCroppedCanvas({
                width: 400,
                height: 400
            });

            canvas.toBlob(function(blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function() {
                    var base64data = reader.result;
                    $.ajax({
                        url: '{{url("api/masterdata/products/upload_image")}}',
                        method: 'POST',
                        data: {
                            image: base64data,
                            key: input_active, id: {{$id}}
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            $modal.modal('hide');
                            save()
                        }
                    });
                };
            });
        });

    });
</script>
