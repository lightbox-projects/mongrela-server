<style>
    .card-category {
        height: 100%; width: 100%;
        display: flex; padding: 3em;
        border:2px solid #F26122
    }
    .card-category:hover {
        cursor: pointer;
        background: #D0CFD4; 
        color: white !important
    }
</style>

<div class="mt-5">

    <h1 class="text-center fw-bolder text-secondary">Project Category</h1>
    <div class="row mt-2" style="gap:10px 0">
        @foreach ($categories as $item)
        <div class="col-lg-3">
            <a href="{{url('tenders')}}?category={{$item->category_id}}" class="card-category" style="">
                <div class="m-auto">
                    <img src="{{url('getimage/'.base64_encode($item->category_photo))}}" class="w-100" style="max-width:100px" />
                    <h6 class="fw-bolder mb-0 text-secondary text-center mt-1 h4">{{$item->category_name}}</h6>
                </div>
            </a>
        </div>
        @endforeach 
    </div>

</div>