<style>
    .elementToFadeInAndOut { 
        -webkit-animation: fadeinout 1s linear forwards;
        animation: fadeinout 1s linear forwards;
    }

    @-webkit-keyframes fadeinout {
        0%,100% { opacity: 1; }
        50% { opacity: 0; }
    }

    @keyframes fadeinout {
    0%,100% { opacity: 1; }
    50% { opacity: 0; }
    }
</style>

<div class="mt-3">
    <h1 class="text-secondary fw-bolder">Testimonials</h1>
    <div class="mt-1 position-relative border-primary p-2 d-flex rounded" style="min-height:250px">
        <div class="d-flex flex-column w-100 px-2 ql-editor" style="min-height: inherit" id="content-testimony">
            
        </div>
        <div class="d-flex justify-content-center flex-column " style="gap:10px" id="scroll-testimony">
            {{-- <div class="bg-light rounded-circle" style="height:15px;width:15px;cursor:pointer"></div>
            <div class="bg-muted rounded-circle" style="height:15px;width:15px;cursor:pointer"></div>
            <div class="bg-muted rounded-circle" style="height:15px;width:15px;cursor:pointer"></div> --}}
        </div>
    </div>
</div>


<script>
    
    let testimonies = [
        @foreach ($testimony as $item)
        `    
        <div class="my-auto">
            {!! $item->testimony_content !!}
        </div> 
        <h1 class="ms-auto text-dark h3">{{$item->testimony_quoted}}</h1>   
        `,
        @endforeach
    ]

    let testimony_idx = 0
    function loadTestimony(idx){
        $('#content-testimony').html(testimonies[idx])
        $('#content-testimony').addClass('elementToFadeInAndOut')
        setTimeout(() => {
            $('#content-testimony').removeClass('elementToFadeInAndOut')
        }, 1000);

        $('#scroll-testimony').empty()
        testimonies.forEach((item, i) => {
            if(i == idx){
                $('#scroll-testimony').append(
                    `<div onclick="loadTestimony('${i}')" class="bg-primary rounded-circle" style="height:15px;width:15px;cursor:pointer"></div>`
                )
            } else {
                $('#scroll-testimony').append(
                    `<div onclick="loadTestimony('${i}')" class="bg-muted rounded-circle" style="height:15px;width:15px;cursor:pointer"></div>`
                )
            }
        })

    }

    loadTestimony(testimony_idx)

</script>