<div class="rounded d-flex flex-column mt-2"
    style="
                background-image:url('{{ asset('images/image 11.png') }}');
                background-repeat:no-repeat;
                height:100vh;max-height:300px;
                background-size:cover;
            ">
    <h1 class="mx-auto mt-auto mb-1 m-2 text-white fw-bolder">
        Join Us and Build your Opportunities
    </h1>
    <a href="{{url('tender-management')}}" class="btn btn-primary mx-auto mb-auto">
        Post Your Project
    </a>
</div>
