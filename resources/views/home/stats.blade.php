<div class="bg- position-relative px-3 py-2 border-primary" style="border-radius: 0 0 10px 10px !important">
    <div class="row">
        <div class="col-lg-3">
            <div class="d-flex align-items-center justify-content-center" style="gap:10px 20px">
                <h1 class="fw-bolder mb-0 text-secondary display-3">24</h1>
                <h3 class="text-secondary">Active <br /> Bidders</h3>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="d-flex align-items-center justify-content-center" style="gap:10px 20px">
                <h1 class="fw-bolder mb-0 text-secondary display-3">24</h1>
                <h3 class="text-secondary">Active <br /> Tenders</h3>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="d-flex align-items-center justify-content-center" style="gap:10px 20px">
                <h1 class="fw-bolder mb-0 text-secondary display-3">24</h1>
                <h3 class="text-secondary">Project <br /> Goals</h3>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="d-flex align-items-center justify-content-center" style="gap:10px 20px">
                <h1 class="fw-bolder mb-0 text-secondary display-3">24</h1>
                <h3 class="text-secondary">Connection <br /> Builds</h3>
            </div>
        </div>
    </div>
    <img src="{{ asset('images/Logo 1.png') }}" class="position-absolute" style="height:15px;bottom:15px;right:15px" />
</div>
