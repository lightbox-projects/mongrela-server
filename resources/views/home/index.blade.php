@extends('layouts.layout1')

@section('css_section')
    <style>
        /* .select2 {
                        border: 1px solid #F26122 !important;
                        border-radius: 0 !important
                    }  */

        .select2-container--classic .select2-selection--single,
        .select2-container--default .select2-selection--single {
            /* border: 1px solid #F26122; */
            border-top: 1px solid #F26122;
            border-bottom: 1px solid #F26122;
            border-left: 1px solid #F26122;
            border-radius: 0 !important;
        }

        .select2,
        .select2-selection__rendered {
            color: #F26122 !important;
        }
    </style>
@endsection

@section('page_title')
    Home Page
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    {{-- @include('home.header') --}}
    <div class="pb-3">

        <div style="min-height: 90vh" class="d-flex">
            <div class="w-100 m-auto">
                <div class="d-flex mb-4">
                    <img style="max-width:700px" class="w-100 m-auto" src="{{ asset('images/Logo alt.svg') }}" />
                </div> 

                <div class="row mt-5 justify-content-center" style="gap:10px 0">
                    <div class="col-lg-3 text-center">
                        <a 
                            @if(session()->get('login'))
                            href="{{url('tender-management')}}"
                            @else
                            href="{{url('login')}}"
                            @endif
                        >
                            <div class="d-flex mb-1">
                                <div class="rounded-circle mx-auto d-flex feature-circle"
                                    style="width:90px;height:90px;background:#F3F3F3">
                                    <div class="rounded-circle m-auto d-flex feature-circle"
                                        style="width:80px;height:80px;background:#F3F3F3;border:1px solid #F2753E">
                                        <i class="bx bxs-buildings m-auto" style="font-size:40px;color:#F2753E"></i>
                                    </div>
                                </div>
                            </div>
                            <h4 class="fw-bolder mb-0" style="color:#F2753E">Post A Project</h4>
                            <div class="text-secondary">
                                SOME DESCRIPTION TEXT HERE
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 text-center">
                        <a href="{{url('tenders')}}">
                            <div class="d-flex mb-1">
                                <div class="rounded-circle mx-auto d-flex feature-circle"
                                    style="width:90px;height:90px;background:#F3F3F3">
                                    <div class="rounded-circle m-auto d-flex feature-circle"
                                        style="width:80px;height:80px;background:#F3F3F3;border:1px solid #F2753E">
                                        <i class="bx bx-bulb m-auto" style="font-size:40px;color:#F2753E"></i>
                                    </div>
                                </div>
                            </div>
                            <h4 class="fw-bolder mb-0" style="color:#F2753E">Browse Tenders</h4>
                            <div class="text-secondary">
                                SOME DESCRIPTION TEXT HERE
                            </div>
                        </a>
                    </div> 
                </div>

                <div class="mt-5 d-flex">
                    <div class="m-auto w-100" style="max-width: 800px">
                        @include('home.search-tender')
                    </div>
                </div>

            </div>
        </div>

        {{-- <div style="margin-bottom: 17em">
            @include('home.how-it-works')
            @include('home.stats')
        </div> --}}

        @include('home.tenders')
        @include('home.categories')
        <div style="margin-top:8em">
            @include('home.testimony')
        </div>
        @include('home.post-a-project')

    </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
    </script>
@endsection
