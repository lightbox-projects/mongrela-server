<form method="GET" action="{{url('tenders')}}">
    @csrf
    <div class="d-flex flex-wrap align-items-center " style="gap:10px">
        <div class="d-flex w-100" style="">
            <div class="flex-fill">
                <select class="select2" name="category" id="category-filter" @if(isset($request['category'])) value="{{$request['category']}}" @endif>
                    <option value="">Categories</option>
                    @foreach ($categories as $item)
                        <option value="{{$item->category_id}}" @if(isset($request['category']) && $request['category'] == $item->category_id) selected  @endif>
                            {{$item->category_name}}
                        </option>
                    @endforeach
                </select>
            </div>
            {{-- <div class="flex-fill">
                <select class="select2 middle" name="location" id="location-filter" @if(isset($request['location'])) value="{{$request['location']}}" @endif>
                    <option value="">Location</option>
                </select>
            </div> --}}
            <div class="flex-fill">
                <input class="form-control" placeholder="Tags" name="tag" id="tags-filter" @if(isset($request['tag'])) value="{{$request['tag']}}" @endif
                    style="
        border:1px solid #F26122;
        border-right:unset !important;
        color:#F26122 !important;
        border-radius:unset !important
    " />
            </div>
            {{-- <button class="btn btn-outline-primary bg-white" style="border-radius: unset !important;border-size:2px">
                From
            </button>
            <div class="flex-fill">
                <input class="form-control" type="date" name="start_date" id="start_date-filter" @if(isset($request['start_date'])) value="{{$request['start_date']}}" @endif
                    style="
        border:1px solid #F26122;
        border-left:unset !important;
        border-right:unset !important;
        color:#F26122 !important;
        border-radius:unset !important
    " />
            </div>
            <button class="btn btn-outline-primary bg-white" style="border-radius: unset !important;border-size:2px">
                To
            </button>
            <div class="flex-fill">
                <input class="form-control" type="date" name="end_date" id="end_date-filter" @if(isset($request['end_date'])) value="{{$request['end_date']}}" @endif
                    style="
        border:1px solid #F26122;
        border-left:unset !important;
        border-right:unset !important;
        color:#F26122 !important;
        border-radius:unset !important
    " />
            </div> --}}
            <button class="btn btn-outline-primary bg-white" style="border-radius: unset !important;border-size:2px">
                Search Tender
            </button>
        </div>
    </div>

</form>
