<style>
    .hide-how-it {
        padding: 0 !important;
        width:0% !important;
        overflow:hidden;
        -webkit-transition: width .5s ease-in-out !important;
        -moz-transition: width .5s ease-in-out !important;
        -o-transition: width .5s ease-in-out !important;
        transition: width .5s ease-in-out;
    }
    .how-it-active .how-it-content, .how-it-active .reset-how-it {
        display: block !important;
    }
    .how-it-active .image {
        display: none !important;
    }

</style>

<div class="d-flex flex-nowrap mt-2">
    <div class="flex-fill p-2 position-relative w-100" style="border-radius: 10px 0 0 0;background:linear-gradient(90deg, rgba(242,97,34,1) 40%, rgba(57,26,13,1) 100%);"
       id="how-it-tenders"
    >
        <div class="d-flex flex-wrap align-items-center" style="gap:10px">
            <div class="me-auto">
                <h3 class="fw-bolder text-white">Make it Simple with Qompair</h3>
                <h3 class="fw-bolder text-white">For Project Owner</h3>
                <button class="btn btn-secondary mb-auto image" onclick="howItWorks('tenders')">How It Works</button>
            </div>
            <div class="d-flex image">
                <img class="mx-auto w-100" src="{{asset('images/analyst 1.png')}}" style="max-width:150px" />
            </div>
            <button class="btn btn-secondary mb-auto reset-how-it d-none" onclick="resetHowIt()">How It Works</button>
        </div>
        <div class="d-none mt-2 how-it-content">
            <div class="row">
                <div class="col-lg-4">
                    <div class="d-flex flex-column" style="">
                        <img src="{{asset('images/to-do-list 1.png')}}" class="w-100 mx-auto" style="max-width:100px" />
                        <div class="mx-auto">
                            <h6 class="fw-bolder mb-0 text-white text-center mt-1 h4">1. List what you need</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="d-flex flex-column" style="">
                        <img src="{{asset('images/submit 1.png')}}" class="w-100 mx-auto" style="max-width:100px" />
                        <div class="mx-auto">
                            <h6 class="fw-bolder mb-0 text-white text-center mt-1 h4">2. Submit it on Qompair</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="d-flex flex-column" style="">
                        <img src="{{asset('images/recruitment 1.png')}}" class="w-100 mx-auto" style="max-width:100px" />
                        <div class="mx-auto">
                            <h6 class="fw-bolder mb-0 text-white text-center mt-1 h4">3. Find your suitable tender</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="w-100" style="max-width:20px" src="{{asset('images/Logo 3.png')}}" />
    </div>
    <div class="flex-fill p-2 position-relative bg-secondary w-100" style="border-radius: 0 10px 0 0"
        id="how-it-bidders"
    >
        <div class="d-flex flex-wrap align-items-center" style="gap:10px">
            <div class="me-auto">
                <h3 class="fw-bolder text-white">Make it Simple with Qompair</h3>
                <h3 class="fw-bolder text-white">For Bidders</h3>
                <button class="btn btn-primary mb-auto image" onclick="howItWorks('bidders')">How It Works</button>
            </div>
            <div class="d-flex image" onclick="howItWorks('bidders')" style="cursor: pointer" >
                <img class="mx-auto w-100" src="{{asset('images/workers 1.png')}}" style="max-width:150px" />
            </div>
            <button class="btn btn-primary mb-auto reset-how-it d-none" onclick="resetHowIt()">How It Works</button>
        </div>
        <div class="d-none mt-2 how-it-content">
            <div class="row">
                <div class="col-lg-4">
                    <div class="d-flex flex-column" style="">
                        <img src="{{asset('images/to-do-list 1.png')}}" class="w-100 mx-auto" style="max-width:100px" />
                        <div class="mx-auto">
                            <h6 class="fw-bolder mb-0 text-white text-center mt-1 h4">1. List what you offer</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="d-flex flex-column" style="">
                        <img src="{{asset('images/submit 1.png')}}" class="w-100 mx-auto" style="max-width:100px" />
                        <div class="mx-auto">
                            <h6 class="fw-bolder mb-0 text-white text-center mt-1 h4">2. Submit it on Qompair</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="d-flex flex-column" style="">
                        <img src="{{asset('images/recruitment 1.png')}}" class="w-100 mx-auto" style="max-width:100px" />
                        <div class="mx-auto">
                            <h6 class="fw-bolder mb-0 text-white text-center mt-1 h4">3. Find your suitable project</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="w-100" style="max-width:20px" src="{{asset('images/Logo 3.png')}}" />
    </div>
</div>

<script>
    let counter_elm = null
    let elm = null

    function howItWorks(id){ 
        if(id == 'tenders') {
            counter_elm = $(`#how-it-bidders`)
        } else {
            counter_elm = $('#how-it-tenders')
        }

        id = `#how-it-${id}`
        elm = $(id)

        elm.addClass('how-it-active')
        counter_elm.addClass('hide-how-it').removeClass('flex-fill')
    }

    function resetHowIt(){
        elm.removeClass('how-it-active')
        counter_elm.removeClass('hide-how-it').addClass('flex-fill')
    }
</script>