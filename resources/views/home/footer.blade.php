<style>
    .footer input, .footer textarea {
        border: 1px solid #F26122 !important;
        background: #2B2F33 !important;
        color: white !important
    }
</style>

<div>
    <div class="bg-dark footer">
        <div class="container-lg py-3">
            <div class="row align-items-center" style="gap:10px 0">
                <div class="col-lg-3 text-white">
                    <img src="{{asset('images/Logo 1.png')}}" class="w-100" style="max-width:100px" />
                    <div class="mt-1">
                        Building Connection, Creating Opportunities
                    </div>
                    <div class="my-1">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non ligula vitae nibh consequat ullamcorper sit amet vitae metus....
                    </div>
                    <div>
                        info@qompair.com
                    </div>
                </div>
                <div class="col-lg-auto" style="border-right: 1px solid white;border-left: 1px solid white">
                    <div class="d-flex flex-column" style="gap:10px">
                        <a href="#" class="text-white">About Us</a>
                        <a href="#" class="text-white">Tenders</a>
                        <a href="#" class="text-white">Companies</a>
                        <a href="#" class="text-white">News & Updates</a>
                        <a href="#" class="text-white">Pricing</a>
                        <a href="#" class="text-white">How It Works</a>
                        <a href="#" class="text-white">Help</a>
                        <a href="#" class="text-white">Terms & Condition</a>
                        <a href="#" class="text-white">Privacy Policy</a>
                    </div>
                </div>
                <div class="col-lg">
                    <h6 class="fw-bolder text-white">Got Question?</h6>
                    <div class="row" style="gap:10px 0">
                        <div class="col-lg-6">
                            <input class="form-control" placeholder="Name" />
                        </div>
                        <div class="col-lg-6">
                            <input class="form-control" placeholder="Emails" />
                        </div>
                        <div class="col-lg-12">
                            <textarea class="form-control" placeholder="Ask your question and send it right away to our inbox" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-primary text-center text-white fw-bolder" style="padding: .5em 1em">
        Copyright © 2023 Qompair.com
    </div>
</div>