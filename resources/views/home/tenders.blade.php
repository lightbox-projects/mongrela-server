<div class="mt-5">

    <h1 class="text-center fw-bolder text-secondary">Latest Tenders</h1>
    <div class="row mt-2" style="gap:10px 0">
        @foreach ($latest as $item)
            <div class="col-lg-4">
                <div class="card mb-0 h-100">
                    <div class="card-body d-flex flex-column">
                        @if($item->owner->company)
                        <div class="d-flex align-items-center" style="gap:10px">
                            <img src="{{url('getimage/'.base64_encode($item->owner->company->company_photo))}}" 
                                class="w-100" style="max-height:30px;max-width:50px;object-fit:contain" />
                            <h3 class="mb-0 fw-bolder text-secondary">{{$item->owner->company->company_name}}</h3>
                        </div>
                        @endif
                        <div class="mt-2 mb-1">
                            <h1 class="fw-bolder text-secondary h3">{{$item->prj_name}}</h1>
                            <h6 style="
                                display: -webkit-box;
                                -webkit-line-clamp: 4;
                                -webkit-box-orient: vertical;
                                overflow: hidden;
                            ">
                                {{$item->prj_description}}
                            </h6>
                            <h6 class="mb-0 mt-2 text-secondary">
                                Tags: {{$item->prj_tags}}
                            </h6>
                        </div>

                        <div class="d-flex align-items-center mt-auto" style="gap:10px 20px">
                            <i class="bx bx-package" style="font-size:30px"></i>
                            <div>
                                <h6 style="margin-bottom: .25em" class="fw-bolder">Category</h6>
                                <div class="badge bg-primary">
                                    {{$item->category->category_name ?? '-'}}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                                    <i class="bx bx-calendar" style="font-size:30px"></i>
                                    <div>
                                        <h6 style="margin-bottom: .25em" class="fw-bolder">Starts on</h6>
                                        <h6 class="fw-bolder text-dark">
                                            {{$item->prj_tender_start}}
                                        </h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                                    <i class="bx bx-calendar" style="font-size:30px"></i>
                                    <div>
                                        <h6 style="margin-bottom: .25em" class="fw-bolder">Ends on</h6>
                                        <h6 class="fw-bolder text-dark">
                                            {{$item->prj_tender_end}}
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @php
                             $date_now = date("Y-m-d");
                        @endphp

                        <div class="d-flex flex-wrap align-items-center mt-1" style="gap:10px">
                            @if($date_now > $item->prj_tender_end)
                            <button class="btn btn-muted">
                                Tender Expired
                            </button>
                            @elseif($date_now > $item->prj_tender_start)
                            <button class="btn btn-success">
                                Tender Ongoing
                            </button>
                            @else
                            <button class="btn btn-muted">
                                Tender Upcoming
                            </button>
                            @endif
                            <a class="btn btn-primary" href="{{url('tenders/'.$item->prj_id)}}">
                                View
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</div>