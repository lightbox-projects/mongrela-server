@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    News & Updates
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3">

        <div class="d-flex justify-content-between flex-wrap align-items-center" style="gap:10px">
            <div>
                <h3 class="fw-bolder">News & Updates</h3>
                <p class="mb-0">
                    A role provided access to predefined menus and features so that depending <br>
                    on assigned role an administrator can have access to what he need
                </p>
            </div>
        </div>

        <hr class="my-2" />

        <div class="row" style="gap:20px 0">
            @foreach ($news as $item)
                <div class="col-lg-3">
                    <div class="card h-100 mb-0">
                        <img class="w-100"
                            src="{{url('getimage/'.base64_encode($item->news_photo))}}" />
                        <div class="card-body">
                            <h5 class="fw-bolder text-dark">{{$item->news_title}}</h5>
                            <h6 class="text-muted">{{$item->news_date}}</h6>

                            <p style="
                                display: -webkit-box;
                                -webkit-line-clamp: 4;
                                -webkit-box-orient: vertical;
                                overflow: hidden;
                            ">
                                {{$item->news_short_desc}}
                            </p>

                            <a href="{{url('/news/detail/'.$item->news_id)}}" class="btn btn-sm btn-primary">
                                Read More
                            </a>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
    </script>
@endsection
