@extends('layouts.layout1')

@section('css_section')

@endsection

@section('page_title')
    News & Updates
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')

    <h1 class="fw-bold display-3 mb-0">{{$news->news_title}}</h1>
    <h5 class="text-muted">{{$news->news_date}}</h5>
    
    <hr/>
    <img class="w-100 rounded" src="{{url('getimage/'.base64_encode($news->news_photo))}}" style="max-height:300px;object-fit:cover" />
    <hr/>

    <div class="mt-2">
        {{$news->news_short_desc}}
    </div>

    <div class="mt-2">
        
        {!! $news->news_content !!}
    </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
    </script>
@endsection
