<div>
    <h3>List of products
        <strong>({{ count($bid->products) }}/{{ count($project->products) }} products filled)</strong>
    </h3>
</div>

<style>
    .btn-custom-tender {
        background: #F3F3F3 !important;
        text-align: left !important;
    }

    .btn-custom-tender:hover {
        color: white !important;
        background: #F26122 !important
    }

    input,
    textarea,
    select {
        border: unset !important;
        border-bottom: 1px solid #d8d6de !important;
        border-radius: unset !important
    }

    .form-check-input:checked {
        background: #F26122 !important
    }
</style>

<div class="mt-2 row" style="gap:10px 0">
    <div class="col-lg-auto">
        <div class="card card-body mb-0 d-flex flex-column" style="gap:10px">
            @foreach ($project->products as $item)
                <div class="d-flex align-items-center" style="gap:10px">
                    <a @if (isset($is_owner) && $is_owner) href="{{ url('bid/' . $id . '/' . $uid . '/' . $item->pr_id) }}"
                        @else
                        href="{{ url('tenders/' . $id . '/bid/' . $item->pr_id) }}" @endif
                        class="btn flex-fill
                        {{ $item->pr_id == $active_product->pr_id ? 'btn-primary' : 'btn-custom-tender' }}
                        "
                        style="border:unset">
                        {{ $item->product->pr_name }}
                    </a>
                    @if (in_array($item->pr_id, $bid_products->toArray()))
                        <button class="btn btn-success btn-icon btn-sm"><i class="bx bx-check"></i></button>
                    @else
                        <button class="btn btn-outline-danger btn-icon btn-sm"><i class="bx bx-x"></i></button>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-lg">
        <div class="p-1 bg-white rounded">
            <div class="p-1 rounded mb-2" style="background: #F4F4F4 !important">
                <h1 class="fw-bolder">{{ $active_product->pr_name }}</h1>
                <p>
                    {{ $active_product->pr_description }}
                </p>
                <div>
                    Required Quantity: {{ $active_product->pr_quantity }} items
                </div>
            </div>

            <div class="row my-2" style="gap:10px">
                @if ($active_product->pr_main_photo)
                    <div class="col-lg-4">
                        <img class="w-100 rounded" style="object-fit: scale-down"
                            src="{{ url('getimage/' . base64_encode($active_product->pr_main_photo)) }}">
                    </div>
                @endif
                @if ($active_product->pr_dimension_photo)
                    <div class="col-lg-4">
                        <img class="w-100 rounded" style="object-fit: scale-down"
                            src="{{ url('getimage/' . base64_encode($active_product->pr_dimension_photo)) }}">
                    </div>
                @endif
                @if ($active_product->pr_accessories_photo)
                    <div class="col-lg-4">
                        <img class="w-100 rounded" style="object-fit: scale-down"
                            src="{{ url('getimage/' . base64_encode($active_product->pr_accessories_photo)) }}">
                    </div>
                @endif
            </div>

            <div>

                <form id="frm">
                    @csrf
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="text-primary">Specification By Host</td>
                                <td>
                                    <div class="form-check">
                                        <input type="radio" name="type_specification" id="specification-default"
                                            value="default" required class="form-check-input type_specification"
                                            style="border:1px solid #F26122 !important" />
                                        <label class="form-check-label text-primary" for="specification-default">Offer
                                            as Specified</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-check">
                                        <input type="radio" name="type_specification" id="specification-alter"
                                            value="alternative" required checked
                                            class="form-check-input type_specification"
                                            style="border:1px solid #F26122 !important" />
                                        <label class="form-check-label text-primary"
                                            for="specification-alter">Alternative Offer</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="background:#F4F4F4;width:35%">
                                    <div class="fw-bolder">Brand</div>
                                    <div>{{ $active_product->pr_brand }}</div>
                                </td>
                                <td colspan="2">
                                    <input class="form-control" placeholder="Input Something" name="inp[pr_brand]"
                                        id="pr_brand" required />
                                </td>
                            </tr>
                            <tr>
                                <td style="background:#F4F4F4;width:35%">
                                    <div class="fw-bolder">Model</div>
                                    <div>{{ $active_product->pr_model }}</div>
                                </td>
                                <td colspan="2">
                                    <input class="form-control" placeholder="Input Something" name="inp[pr_model]"
                                        id="pr_model" required />
                                </td>
                            </tr>
                            <tr>
                                <td style="background:#F4F4F4;width:35%">
                                    <div class="fw-bolder">Accessories</div>
                                    <div>{{ $active_product->pr_accessories }}</div>
                                </td>
                                <td colspan="2">
                                    <input class="form-control" placeholder="Input Something" name="inp[pr_accessories]"
                                        id="pr_accessories" required />
                                </td>
                            </tr>
                            <tr>
                                <td style="background:#F4F4F4;width:35%">
                                    <div class="fw-bolder">Materials</div>
                                    <div>{{ $active_product->pr_materials }}</div>
                                </td>
                                <td colspan="2">
                                    <input class="form-control" placeholder="Input Something" name="inp[pr_materials]"
                                        id="pr_materials" required />
                                </td>
                            </tr>
                            <tr>
                                <td style="background:#F4F4F4;width:35%">
                                    <div class="fw-bolder">Finishing</div>
                                    <div>{{ $active_product->pr_finishing }}</div>
                                </td>
                                <td colspan="2">
                                    <input class="form-control" placeholder="Input Something" name="inp[pr_finishing]"
                                        id="pr_finishing" required />
                                </td>
                            </tr>
                            <tr>
                                <td style="background:#F4F4F4;width:35%">
                                    <div class="fw-bolder">Dimension</div>
                                    <div>{{ $active_product->pr_dimension }}</div>
                                </td>
                                <td colspan="2">
                                    <input class="form-control" placeholder="Input Something" name="inp[pr_dimension]"
                                        id="pr_dimension" required />
                                </td>
                            </tr>
                            <tr>
                                <td style="background:#F4F4F4;width:35%">
                                    <div class="fw-bolder">Total Price</div>
                                    <div id="total-price-preview" class="fw-bolder h4">S$ 0.00</div>
                                </td>
                                <td colspan="2">
                                    <small>Add Note</small><br/>
                                    <input class="form-control" placeholder="Input Something" name="inp[pr_note_bidder]"
                                        id="pr_note_bidder" required />
                                    <div class="row mt-1" style="gap:10px 0">
                                        <div class="col-lg">
                                            <div class="fw-bolder">Price offered (per unit) in S$</div>
                                            <input class="form-control" type="number" placeholder="Input Something" name="inp[pr_price]"
                                                id="pr_price" required />
                                        </div>
                                        <div class="col-lg">
                                            <div class="fw-bolder">Quantity offered</div>
                                            <input class="form-control" type="number" placeholder="Input Something" name="inp[pr_quantity]"
                                                id="pr_quantity" required />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <input type="hidden" name="bid_id" value="{{ $id }}" required />
                    <input type="hidden" name="pr_id_original" value="{{ $pr_id }}" required />
                </form>

                <div class="d-flex mt-2">
                    @if (isset($is_owner) && $is_owner)
                    @elseif ($next)
                        <button type="button" onclick="onSaveNNext()" class="ms-auto btn btn-primary">
                            <i class="bx bx-save"></i>
                            Save and Go to Next Product
                        </button>
                    @else
                        <button type="button" onclick="onSave()" class="ms-auto btn btn-primary">
                            <i class="bx bx-save"></i>
                            Save
                        </button>
                    @endif
                </div>


            </div>
        </div>
    </div>
</div>



<script>
    function onSave() {
        if ($("#frm").valid()) {
            var formData = new FormData($('#frm')[0]);
            $.ajax({
                url: '{{ url('api/biddings') }}',
                type: 'post',
                data: formData,
                contentType: false, //untuk upload image
                processData: false, //untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType: 'json',
                success: function(e) {
                    if (e.status == 'success') {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();
                        setTimeout(function() {
                            location.reload()
                        }, 1000);
                    } else {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();;
                    }
                }
            });
        }
    }

    function onSaveNNext() {
        if ($("#frm").valid()) {
            var formData = new FormData($('#frm')[0]);
            $.ajax({
                url: '{{ url('api/biddings') }}',
                type: 'post',
                data: formData,
                contentType: false, //untuk upload image
                processData: false, //untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType: 'json',
                success: function(e) {
                    if (e.status == 'success') {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();
                        setTimeout(function() {
                            window.location.href =
                                "{{ url('tenders/' . $id . '/bid/' . $next) }}"
                        }, 1000);
                    } else {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();;
                    }
                }
            });
        }
    }

    let quantity_offered = 0
    let price_offered = 0

    function reloadTotalPrice(){
        $('#total-price-preview').html(`S$ ${$('#pr_quantity').val() * $('#pr_price').val()}`)
    }

    $('#pr_quantity').change(function(){
        let quantity_offered = $('#pr_quantity').val()
        reloadTotalPrice()
    })

    $('#pr_price').change(function(){
        let price_offered = $('#pr_price').val()
        reloadTotalPrice()
    })

    $("input[name='type_specification']").change(function() {
        if ($(this).val() == 'default') {
            get_original()
        }
    })

    edit()

    function edit() {
        $.ajax({
            url: '{{ url('api/biddings/' . $id . '/' . $pr_id) }}',
            type: 'get',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                $.each(e, function(key, value) {
                    if ($('#' + key).hasClass("select2")) {
                        $('#' + key).val(value).trigger('change');
                    } else if ($('input[type=radio]').hasClass(key)) {
                        if (value != "") {
                            $("input[name='" + key + "'][value='" + value + "']").prop(
                                'checked', true);
                        }
                    } else if ($('input[type=checkbox]').hasClass(key)) {
                        if (value != null) {
                            var temp = value.split('; ');
                            for (var i = 0; i < temp.length; i++) {
                                $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                    'checked', true);
                            }
                        }
                    } else {
                        $('#' + key).val(value).trigger('change');
                    }
                });
            }
        });
    }

    function get_original() {
        $.ajax({
            url: '{{ url('api/biddings-original/' . $id . '/' . $pr_id) }}',
            type: 'get',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                $.each(e, function(key, value) {
                    if ($('#' + key).hasClass("select2")) {
                        $('#' + key).val(value).trigger('change');
                    } else if ($('input[type=radio]').hasClass(key)) {
                        if (value != "") {
                            $("input[name='" + key + "'][value='" + value + "']").prop(
                                'checked', true);
                            $.uniform.update();
                        }
                    } else if ($('input[type=checkbox]').hasClass(key)) {
                        if (value != null) {
                            var temp = value.split('; ');
                            for (var i = 0; i < temp.length; i++) {
                                $("input[name='inp[" + key + "][]'][value='" + temp[i] + "']").prop(
                                    'checked', true);
                            }
                            $.uniform.update();
                        }
                    } else {
                        $('#' + key).val(value);
                    }
                });
            }
        });
    }
</script>
