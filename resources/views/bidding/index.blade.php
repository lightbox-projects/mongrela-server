@extends('layouts.layout1')

@section('css_section')
    <style>
        /* .select2 {
                            border: 1px solid #F26122 !important;
                            border-radius: 0 !important
                        }  */

        .select2-container--classic .select2-selection--single,
        .select2-container--default .select2-selection--single {
            /* border: 1px solid #F26122; */
            border-top: 1px solid #F26122;
            border-bottom: 1px solid #F26122;
            border-left: 1px solid #F26122;
            border-radius: 0 !important;
        }

        .select2,
        .select2-selection__rendered {
            color: #F26122 !important;
        }
    </style>
@endsection

@section('page_title')
    Bidding
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    @if(isset($is_owner) && $is_owner)
        @include('tender-management.header')
    @endif

    <div class="pb-3">
        <div class="rounded text-primary fw-bolder h1">
            @if(isset($is_owner) && $is_owner)
            View {{$bid->owner->user_name}} Bid On Tender 
            @else
            View Your Bid On Tender 
            @endif
            : {{ $project->prj_name }}
        </div>
        <hr class="my-2" />
        @include('bidding.header')
        <hr class="my-2" />

        @include('bidding.products')

        <hr class="my-2" />

        <h5 class="fw-bolder text-secondary">Terms And Condition</h5>
        <div style="max-height:400px;overflow-y:scroll">
            {!! $terms_and_condition->setting_value ?? '' !!}
        </div>

        <hr class="my-2" />

        <div class="mt-2 d-flex justify-content-center align-items-center" style="gap:10px">
            @if (isset($is_owner) && $is_owner)
            <a href="{{ url('tender-management/'.$id.'/form/bids') }}" class="btn btn-muted">
                Back to Tender list
            </a>
            @else
            <a href="{{ url('tenders') }}" class="btn btn-muted">
                Back to Tender list
            </a>
            @endif

            @php
                $date_now = date("Y-m-d");
            @endphp

            @if(isset($is_owner) && $is_owner)
            @elseif($bid->bid_status == 'submited')
                <div class="text-success">
                    Bids have been submited.
                </div>
            @elseif($date_now > $project->prj_tender_end)
                <div class="text-secondary">
                    Bids have been closed.
                </div>
            @elseif($date_now < $project->prj_tender_start)
                <div class="text-secondary">
                    Bids isnt open yet.
                </div>
            @elseif (count($bid->products) == count($project->products))
                <button class="btn btn-primary" onclick="onSubmit()">
                    <i class="bx bx-check"></i>
                    Submit Bid
                </button>
            @else
                <button class="btn btn-primary" disabled>
                    <i class="bx bx-check"></i>
                    Submit Bid
                </button>
            @endif

        </div>

    </div>
@endsection

@section('js_section')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        var select = $('.select2')

        function onSubmit() {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to edit after submiting the form!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#F26122',
                cancelButtonColor: '#391A0D',
                confirmButtonText: 'Yes, proceed!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ url("api/biddings/".$id) }}',
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        success: function(e) {
                            if (e.status == 'success') {
                                new Noty({
                                    text: e.message,
                                    type: 'info',
                                    progressBar: true,
                                    timeout: 1000
                                }).show();
                                setTimeout(function() {
                                    location.reload()
                                }, 1000);
                            } else {
                                new Noty({
                                    text: e.message,
                                    type: 'info',
                                    progressBar: true,
                                    timeout: 1000
                                }).show();;
                            }
                        }
                    });
                }
            })
        }
    </script>
@endsection
