<div>
    <div class="mt-2 mb-1">
        <h1 class="fw-bolder text-secondary h3">{{$project->prj_name}}</h1>
        <h6 class="my-2">
            {{$project->prj_description}}
        </h6> 
    </div>

    <div class="d-flex align-items-center mt-4" style="gap:10px 20px">
        <i class="bx bx-package" style="font-size:30px"></i>
        <div>
            <h6 style="margin-bottom: .25em" class="fw-bolder">Category</h6>
            <div class="badge bg-primary">
                {{$project->category->category_name ?? '-'}}
            </div>
        </div>
    </div>

    <div class="row mt-1" style="gap:10px 0;max-width:900px">
        <div class="col-lg">
            <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                <i class="bx bx-calendar" style="font-size:30px"></i>
                <div>
                    <h6 style="margin-bottom: .25em" class="fw-bolder">Bids</h6>
                    <h6 class="fw-bolder text-dark">
                        {{count($project->bids)}} Bids
                    </h6>
                </div>
            </div>
        </div>
        <div class="col-lg">
            <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                <i class="bx bx-calendar" style="font-size:30px"></i>
                <div>
                    <h6 style="margin-bottom: .25em" class="fw-bolder">Starts on</h6>
                    <h6 class="fw-bolder text-dark">
                        {{$project->prj_tender_start}}
                    </h6>
                </div>
            </div>
        </div>
        <div class="col-lg">
            <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                <i class="bx bx-calendar" style="font-size:30px"></i>
                <div>
                    <h6 style="margin-bottom: .25em" class="fw-bolder">Ends on</h6>
                    <h6 class="fw-bolder text-dark">
                        {{$project->prj_tender_end}}
                    </h6>
                </div>
            </div>
        </div>
        <div class="col-lg">
            <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                <i class="bx bx-calendar" style="font-size:30px"></i>
                <div>
                    <h6 style="margin-bottom: .25em" class="fw-bolder">Expected Deliveries</h6>
                    <h6 class="fw-bolder text-dark">
                        {{$project->prj_expected_completion_date}}
                    </h6>
                </div>
            </div>
        </div>
    </div>
</div>