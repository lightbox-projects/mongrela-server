<div class="bs-stepper wizard-modern modern-wizard-example mb-2">
    <div class="bs-stepper-header d-flex flex-nowrap" style="padding:unset !important;padding-bottom:.5em !important;overflow-x:auto !important">
        <div class="step {{ request()->path() == 'approval-trainer' ? 'active' : '' }}" data-target="#account-details-modern" role="tab" id="account-details-modern-trigger">
            <a href="{{url('approval-trainer')}}" type="button" class="step-trigger" aria-selected="true">
                <span class="bs-stepper-box">
                    <i class="bx bx-book-content"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Trainer Approval</span>
                    <span class="bs-stepper-subtitle">Trainer Permission Management</span>
                </span>
            </a>
        </div>
        <div class="line">
            <i class="bx bx-chevron-right"></i>
        </div>
        <div class="step {{ str_contains(request()->path(), 'approval-shelter') ? 'active' : '' }}" data-target="#personal-info-modern" role="tab" id="personal-info-modern-trigger">
            <a href="{{url('approval-shelter')}}" type="button" class="step-trigger" aria-selected="false">
                <span class="bs-stepper-box">
                    <i class="bx bx-package"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Shelter Approval</span>
                    <span class="bs-stepper-subtitle">Shelter Permission Management</span>
                </span>
            </a>
        </div> 
        <div class="line">
            <i class="bx bx-chevron-right"></i>
        </div>
        <div class="step {{ str_contains(request()->path(), 'approval-store') ? 'active' : '' }}" data-target="#personal-info-modern" role="tab" id="personal-info-modern-trigger">
            <a href="{{url('approval-store')}}" type="button" class="step-trigger" aria-selected="false">
                <span class="bs-stepper-box">
                    <i class="bx bx-show"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Store Approval</span>
                    <span class="bs-stepper-subtitle">Store Permission Management</span>
                </span>
            </a>
        </div>   
        <div class="line">
            <i class="bx bx-chevron-right"></i>
        </div>
        <div class="step {{ str_contains(request()->path(), 'banners/page') ? 'active' : '' }}" data-target="#personal-info-modern" role="tab" id="personal-info-modern-trigger">
            <a href="{{url('banners/page')}}" type="button" class="step-trigger" aria-selected="false">
                <span class="bs-stepper-box">
                    <i class="bx bx-show"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Banners</span>
                    <span class="bs-stepper-subtitle">Banner Management</span>
                </span>
            </a>
        </div>   
        <div class="line">
            <i class="bx bx-chevron-right"></i>
        </div>
        <div class="step {{ str_contains(request()->path(), 'ads/page') ? 'active' : '' }}" data-target="#personal-info-modern" role="tab" id="personal-info-modern-trigger">
            <a href="{{url('ads/page')}}" type="button" class="step-trigger" aria-selected="false">
                <span class="bs-stepper-box">
                    <i class="bx bx-show"></i>
                </span>
                <span class="bs-stepper-label">
                    <span class="bs-stepper-title">Ads</span>
                    <span class="bs-stepper-subtitle">Ads Management</span>
                </span>
            </a>
        </div>   
    </div>
</div> 