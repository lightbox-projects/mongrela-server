@extends('layouts.layout1-mongrela')

@section('css_section')

@endsection

@section('page_title')
    Approval Trainer
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3" style="min-height:60vh">

        @include('_mongrela.menu')

        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped" id="table">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Trainer Name</th>
                            <th>Speciality</th>
                            <th>Description</th>
                            <th>Approval Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js_section')
    <script>
        var select = $('.select2')
        let dTable = $('#table')

        $(function() {
            dTable = $('#table').DataTable({
                ajax: {
                    url: "{{ url('mongrela/approval-trainer/dt') }}",
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: false,
                        className: 'text-center'
                    },
                    {
                        data: 'trainer_name',
                        name: 'trainer_name'
                    },
                    {
                        data: 'trainer_title',
                        name: 'trainer_title'
                    },
                    {
                        data: 'trainer_description',
                        name: 'trainer_description'
                    },
                    {
                        data: 'is_approved',
                        name: 'is_approved'
                    },
                ],
                buttons: [],
                order: [
                    [1, 'desc']
                ],
            });

            $('.dataTables_filter input[type=search]').attr('placeholder', 'Search').attr('class',
                'form-control');
            $('.dataTables_filter select[name=table_length]').attr('class', 'form-select form-select-sm');
        })

        function change_approval(id, approval) {
            $.ajax({
                url: '{{ url("mongrela/approval-trainer") }}',
                type: 'post',
                data: {
                    id, approval
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(e) {
                    if (e.status == 'success') {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();
                        dTable.draw();
                    } else {
                        new Noty({
                            text: e.message,
                            type: 'info',
                            progressBar: true,
                            timeout: 1000
                        }).show();;
                    }
                }
            });
        }
    </script>
@endsection
