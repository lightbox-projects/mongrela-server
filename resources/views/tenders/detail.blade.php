@extends('layouts.layout1')

@section('css_section')
    <style>
        .swiper-button-next:after,
        .swiper-rtl .swiper-button-prev:after,
        .swiper-button-prev:after,
        .swiper-rtl .swiper-button-next:after {
            content: unset !important
        }

        .swiper-slide-active img {
            height: 300px !important;
        }
    </style>
@endsection

@section('page_title')
    Tender Detail
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content') 
    <div class="pb-3">

        <div class="d-flex flex-wrap align-items-center justify-content-between mb-1" style="gap:10px">
            <a href="{{ url('tenders') }}" class="btn btn-outline-primary" style="border-width:2px !important">
                Back to list
            </a> 
            @if($next)
            <a href="{{ url('tenders/'.$next) }}" class="btn btn-outline-primary" style="border-width:2px !important">
                Next Tender
            </a>
            @endif
        </div>

        <div class="px-3 pb-2 pt-2 border-primary">

            <div class="py-2 px-2 mb-2">
                <div class="d-flex flex-wrap align-items-center" style="gap:10px 30px">
                    <img src="{{ asset('lightbox.png') }}" class=""
                        style="max-height:200px;max-width:200px;object-fit:contain" />
                    <div style="max-width:600px">
                        <h6 class="fw-bolder text-secondary h1">{{ $company->company_name }}</h6>
                        <h6 class="text-secondary h5">{{ $company->company_slogan }}</h6>
                        <h6 class="fw-bolder text-secondary h5">{{ $company->company_city }},
                            {{ $company->company_country }}</h6>
                    </div>
                </div>
            </div>

            @if($project->prj_photo)
            <div class="col-lg-12">
                <div style="height:300px" class="w-100 bg-secondary rounded mb-1" id="col_prj_photo">
                    <img id="prj_photo_preview" src="{{url('getimage/'.base64_encode($project->prj_photo))}}" class="w-100 rounded" style="object-fit: cover;height:300px" />
                </div> 
            </div>
            @endif

            <h1 class="text-secondary text-center fw-bolder my-3">{{$project->prj_name}}</h1>

            <div class="swiper-centered-slides-2 swiper-container position-relative"
                style="overflow: hidden;height:fit-content">

                <div class="swiper-wrapper align-items-center" id="image-swiper" style="">
                    @foreach ($project->products as $item)
                        @php
                            $product = $item->product;
                        @endphp
                        <div class="swiper-slide" style="width:fit-content" onclick="">
                            @php
                                $url = url('getimage/' . base64_encode($product->pr_main_photo));
                            @endphp
                            <img src="{{ $url }}" style="height:200px;object-fit:contain;background:#7F7F7F"
                                class="rounded" />
                        </div>
                    @endforeach
                </div>

                <div class="swiper-button-next text-white bg-primary rounded-circle" tabindex="0" role="button"
                    aria-label="Next slide" style="top:150px;width:40px;height:40px"><i class="bx bx-chevron-right"></i>
                </div>
            </div>

            @include('tenders.project-desc')
            @include('tenders.item-detail')
            @include('tenders.footer')
        </div>

        <div class="mt-5">
            @include('home.tenders')
        </div>

        {{-- @include('home.testimony') --}}
        {{-- @include('home.post-a-project') --}}
    </div>

@endsection

@section('js_section')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css" />

    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
    <script>
        var select = $('.select2')

        var mySwiperOpt2 = new Swiper('.swiper-centered-slides-2', {
            slidesPerView: 'auto',
            initialSlide: 0,
            spaceBetween: 20,
            centeredSlides: true,
            slideToClickedSlide: true,
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            }
        });


        const gallery = new Viewer(document.getElementById('image-swiper'));

        function startBidding(){
            $('#frmbox').modal('show')
        }
    </script>

    @include('tenders.modal-appliance')
@endsection
