

<hr class="my-2"/>
<div class="row mt-3" style="gap:10px 0">
    <div class="col-lg-3 text-secondary fw-bolder h5 mb-0" style="max-width:250px">
        Project Description
    </div>
    <div class="col-lg text-secondary">
        {{$project->prj_description}}
    </div>
</div>
<div class="row mt-2" style="gap:10px 0">
    <div class="col-lg-3 text-secondary fw-bolder h5 mb-0" style="max-width:250px">
        Items Needed
    </div>
    <div class="col-lg text-secondary">
        <div class="d-flex flex-column" style="gap:10px">
            @foreach ($project->products as $item)
                @php
                    $product = $item->product;
                @endphp
                <div class="d-flex align-items-center" style="gap:10px">
                    <i class="bx bxs-circle"></i>
                    {{$product->pr_quantity}} {{$product->pr_name}}
                </div> 
            @endforeach
        </div>
    </div>
</div>