<div class="row justify-content-between align-items-end mt-3" style="gap:10px 0">
    <div class="col-lg-auto">
        <div class="d-flex align-items-center" style="gap:10px">
            <img src="{{ url('getimage/' . base64_encode($project->category->category_photo ?? '-')) }}" class="w-100"
                style="max-width:100px" />
            <div>
                <h2 class="text-secondary fw-bolder">Category</h2>
                <h2 class="text-muted fw-bolder h3">{{ $project->category->category_name ?? '-' }}</h2>
            </div>
        </div>
        <h6 class="mb-0 mt-2 text-secondary fw-bolder">
            Tags: {{ $project->prj_tags }}
        </h6>
    </div>
    <div class="col-lg-auto">
        <div class="d-flex align-items-center flex-wrap" style="gap:10px 30px">
            <div class="d-flex align-items-center text-secondary" style="gap:10px">
                <i class="bx bx-calendar" style="font-size:28px"></i>
                <div class="text-secondary">
                    <div class="fw-bolder">Starts On</div>
                    <div class="text-muted fw-bolder">{{ $project->prj_tender_start }}</div>
                </div>
            </div>
            <div class="d-flex align-items-center text-secondary" style="gap:10px">
                <i class="bx bx-calendar" style="font-size:28px"></i>
                <div class="text-secondary">
                    <div class="fw-bolder">Ends On</div>
                    <div class="text-muted fw-bolder">{{ $project->prj_tender_end }}</div>
                </div>
            </div>
            <div class="d-flex align-items-center text-secondary" style="gap:10px">
                <i class="bx bx-calendar" style="font-size:28px"></i>
                <div class="text-secondary">
                    <div class="fw-bolder">Completion Target</div>
                    <div class="text-muted fw-bolder">{{ $project->prj_expected_completion_date }}</div>
                </div>
            </div>

            @php
                $date_now = date('Y-m-d');
            @endphp

            @if ($date_now > $item->prj_tender_end)
                <button class="btn btn-muted">
                    Tender Expired
                </button>
            @elseif(session()->get('login'))
                <button class="btn btn-primary" style="border-radius: unset !important" onclick="startBidding()">
                    Start Bidding
                </button>
            @else
                <button class="btn btn-primary" style="border-radius: unset !important" disabled>
                    Start Bidding
                </button>
            @endif
        </div>
    </div>
</div>
