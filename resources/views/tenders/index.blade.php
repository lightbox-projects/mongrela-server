@extends('layouts.layout1')

@section('css_section')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.5/pagination.css">

    <style>
        /* .select2 {
                            border: 1px solid #F26122 !important;
                            border-radius: 0 !important
                        }  */

        .select2-container--classic .select2-selection--single,
        .select2-container--default .select2-selection--single {
            /* border: 1px solid #F26122; */
            border-top: 1px solid #F26122;
            border-bottom: 1px solid #F26122;
            border-left: 1px solid #F26122;
            border-radius: 0 !important;
        }

        .select2,
        .select2-selection__rendered {
            color: #F26122 !important;
        }
    </style>
    <style>
        .paginationjs .paginationjs-pages li>a {
            border-radius: 5px !important;
            margin: 0 5px;
            height: 38px !important;
            width: 38px !important;
            display: flex;
            align-items: center;
            justify-content: center;
            font-weight: 700;
            font-size: 18px
        }

        .paginationjs .paginationjs-pages li {
            border: unset !important
        }

        .paginationjs .paginationjs-pages li.active>a {
            border: 1px solid #F26122 !important;
            background: unset !important;
            color: #F26122 !important
        }
    </style>
@endsection

@section('page_title')
    Browse Tenders
@endsection

@section('sidebar-size', 'collapsed')
@section('url_back', url('/'))

@section('content')
    <div class="pb-3">

        <div class="d-flex mt-3 mb-2">
            <div class="mx-auto">
                <h1 class="text-secondary fw-bolder d-flex align-items-center" style="gap:10px">
                    <i class="bx bx-search"></i>
                    Search Tenders
                </h1>
            </div>
        </div>
        {{-- @include('home.search-tender')  --}}

        <div class="d-flex">
            <div class="m-auto w-100" style="max-width: 800px">
                @include('home.search-tender')
            </div>
        </div>

        <hr class="mt-2 mb-2" />

        @include('home.tenders')

        <div class="mt-3">
            @include('home.how-it-works')
        </div>
    </div>

    {{-- <div class="bg-primary rounded my-4" style="height:3px">
                    </div> --}}


    <div style="" class="rounded mt-3">
        <div class="container-lg px-0">
            <h1 class="text-center fw-bolder text-secondary mb-2">Tender List</h1>
            <div class="row" style="gap:20px 0;" id="data-container">
            </div>
            <div class="d-flex mt-3">
                <div class="mx-auto" id="pagination-container"></div>
            </div>
        </div>
    </div>

    <div class="container-lg px-0 mb-2">
        @include('home.testimony')
        @include('home.post-a-project')
    </div>

    <input type="hidden" id="total_active" value="{{$total_active}}" />
@endsection

@section('js_section')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.5/pagination.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment-with-locales.min.js" integrity="sha512-42PE0rd+wZ2hNXftlM78BSehIGzezNeQuzihiBCvUEB3CVxHvsShF86wBWwQORNxNINlBPuq7rG4WWhNiTVHFg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        var select = $('.select2')

        $('#pagination-container').pagination({
            dataSource: Array(parseInt($('#total_active').val())).fill(0).map((_, i) => i * i),
            pageSize: 9,
            callback: function(data, pagination) {
                $('#data-container').html('');
                let page = pagination.pageNumber
                let pageRange = pagination.pageSize

                let formData = new FormData()
                formData.append('page', page)
                formData.append('pageRange', pageRange)
                formData.append('offsetLatest', true)
                formData.append('category', $("#category-filter").val())
                formData.append('location', $("#location-filter").val())
                formData.append('tags', $("#tags-filter").val())
                formData.append('start_date', $("#start_date-filter").val())
                formData.append('end_date', $("#end_date-filter").val())

                $.ajax({
                    url: '{{ url('api/tender-management/list') }}',
                    type: 'post',
                    data: formData,
                    contentType: false, //untuk upload image
                    processData: false, //untuk upload image
                    timeout: 300000, // sets timeout to 3 seconds
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(e) {
                        let data = e.data
                        data.map(item => {
                            let start = moment(item.prj_tender_start)
                            let end = moment(item.prj_tender_end)

                            let status = 'upcoming'
                            if(moment().diff(end, 'days') > 0) {
                                status = 'expired'
                            } else if(moment().diff(start, 'days') > 0) {
                                status = 'ongoing'
                            }

                            var html = `
                                <div class="col-lg-4">
                                    <div class="card mb-0">
                                        <div class="card-body ">
                                            ${item.owner.company ? `
                                                <div class="d-flex align-items-center" style="gap:10px">
                                                    <img src="${`{{ url('getimage') }}/` + btoa(item.owner.company.company_photo)}" 
                                                        class="w-100" style="max-height:30px;max-width:50px;object-fit:contain" />
                                                    <h3 class="mb-0 fw-bolder text-secondary">${item.owner.company.company_name}</h3>
                                                </div>
                                            ` : ''}
                                            <div class="mt-2">
                                                <h1 class="fw-bolder text-secondary h3">${item.prj_name}</h1>
                                                <h6 style="
                                                    display: -webkit-box;
                                                    -webkit-line-clamp: 4;
                                                    -webkit-box-orient: vertical;
                                                    overflow: hidden;
                                                ">
                                                    ${item.prj_description}
                                                </h6>
                                                <h6 class="mb-0 mt-2 text-secondary">
                                                    Tags: ${item.prj_tags}
                                                </h6>
                                            </div>

                                            <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                                                <i class="bx bx-package" style="font-size:30px"></i>
                                                <div>
                                                    <h6 style="margin-bottom: .25em" class="fw-bolder">Category</h6>
                                                    <div class="badge bg-primary">
                                                        ${item.category.category_name}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                                                        <i class="bx bx-calendar" style="font-size:30px"></i>
                                                        <div>
                                                            <h6 style="margin-bottom: .25em" class="fw-bolder">Starts on</h6>
                                                            <h6 class="fw-bolder text-dark">
                                                                ${item.prj_tender_start}
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                                                        <i class="bx bx-calendar" style="font-size:30px"></i>
                                                        <div>
                                                            <h6 style="margin-bottom: .25em" class="fw-bolder">Ends on</h6>
                                                            <h6 class="fw-bolder text-dark">
                                                                ${item.prj_tender_end}
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @php
                                                $date_now = date("Y-m-d");
                                            @endphp

                                            <div class="d-flex flex-wrap align-items-center mt-1" style="gap:10px">
                                                ${status == 'ongoing' ? `
                                                    <button class="btn btn-success">
                                                        Tender Ongoing
                                                    </button>
                                                ` : ''}
                                                ${status == 'expired' ? `
                                                    <button class="btn btn-muted">
                                                        Tender Expired
                                                    </button>
                                                ` : ''}
                                                ${status == 'upcoming' ? `
                                                    <button class="btn btn-muted">
                                                        Tender Upcoming
                                                    </button>
                                                ` : ''}
                                                <a class="btn btn-primary" href="{{url('tenders/')}}/${item.prj_id}">
                                                    View
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                            $('#data-container').append(html);
                        })
                    }
                });

            }
        })
    </script>
@endsection
