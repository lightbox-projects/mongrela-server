<div class="row mt-2" style="gap:10px 0">
    <div class="col-lg-3 text-secondary fw-bolder h5 mb-0" style="max-width:250px">
        Items Details
    </div>
    <div class="col-lg text-secondary">
        <div style="max-height:800px;overflow-y:scroll">
            <div class="accordion" id="accordionExample">
                @foreach ($project->products as $key => $item)
                    @php
                        $product = $item->product;
                    @endphp
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading{{$key}}">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
                                {{$product->pr_name}}
                            </button>
                        </h2>
                        <div id="collapse{{$key}}" class="accordion-collapse collapse" aria-labelledby="heading{{$key}}"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table class="table border-primary" style="border-width: 1px !important">
                                    <tr class="bg-primary">
                                        <th class="text-white" colspan="2">{{$product->pr_name}}</th>
                                    </tr>
                                    <tr>
                                        <td class="text-primary fw-bolder" style="border-bottom: 1px solid #F26122"
                                            colspan="2">Product Required</td>
                                    </tr>
                                    <tbody>
                                        <tr>
                                            <td style="border-right: 1px solid #F26122;width:150px">Product Type</td>
                                            <td>{{$product->pr_application}}</td>
                                        </tr>
                                        <tr>
                                            <td style="border-right: 1px solid #F26122">Brand</td>
                                            <td>{{$product->pr_brand}}</td>
                                        </tr>
                                        <tr>
                                            <td style="border-right: 1px solid #F26122">Model</td>
                                            <td>{{$product->pr_model}}</td>
                                        </tr>
                                        <tr>
                                            <td style="border-right: 1px solid #F26122">Photo</td>
                                            <td>
                                                <img src="{{url('getimage/' . base64_encode($product->pr_main_photo))}}"
                                                    class="w-100"
                                                    style="max-width:100px;max-height:100px;object-fit:cover" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-right: 1px solid #F26122">Materials</td>
                                            <td>{{$product->pr_materials}}</td>
                                        </tr>
                                        <tr>
                                            <td style="border-right: 1px solid #F26122">Dimension</td>
                                            <td>{{$product->pr_dimension}}</td>
                                        </tr>
                                        <tr>
                                            <td style="border-right: 1px solid #F26122">Accessories</td>
                                            <td>{{$product->pr_accessories}}</td>
                                        </tr>
                                        <tr>
                                            <td style="border-right: 1px solid #F26122">Notes</td>
                                            <td>{{$product->pr_note}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
