<div class="modal" id="frmbox" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Terms And Condition</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h1 class="fw-bolder text-secondary h3">{{$project->prj_name}}</h1>

                <h5 class="fw-bolder text-secondary">Description</h5>
                <h6>
                    {{$project->prj_description}}
                </h6>

                <h5 class="fw-bolder text-secondary">Terms And Condition</h5>
                <div style="max-height:400px;overflow-y:scroll">
                    {!! $terms_and_condition->setting_value ?? '' !!}
                </div>

                <div class="d-flex align-items-center mt-2" style="gap:10px 20px">
                    <i class="bx bx-package" style="font-size:30px"></i>
                    <div>
                        <h6 style="margin-bottom: .25em" class="fw-bolder">Category</h6>
                        <div class="badge bg-primary">
                            {{$project->category->category_name ?? '-'}}
                        </div>
                    </div>
                </div>
    
                <div class="row" style="gap:10px 0">
                    <div class="col-lg-6">
                        <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                            <i class="bx bx-calendar" style="font-size:30px"></i>
                            <div>
                                <h6 style="margin-bottom: .25em" class="fw-bolder">Starts on</h6>
                                <h6 class="fw-bolder text-dark">
                                    {{$project->prj_tender_start}}
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                            <i class="bx bx-calendar" style="font-size:30px"></i>
                            <div>
                                <h6 style="margin-bottom: .25em" class="fw-bolder">Ends on</h6>
                                <h6 class="fw-bolder text-dark">
                                    {{$project->prj_tender_end}}
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                            <i class="bx bx-calendar" style="font-size:30px"></i>
                            <div>
                                <h6 style="margin-bottom: .25em" class="fw-bolder">Expected Deliveries</h6>
                                <h6 class="fw-bolder text-dark">
                                    {{$project->prj_expected_completion_date}}
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="d-flex align-items-center mt-1" style="gap:10px 20px">
                            <i class="bx bx-package" style="font-size:30px"></i>
                            <div>
                                <h6 style="margin-bottom: .25em" class="fw-bolder">Total Products</h6>
                                <h6 class="fw-bolder text-dark">
                                    {{count($project->products)}} Items
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <a type="button" class="btn btn-primary" href="{{url('tenders/'.$id.'/bid')}}">Apply Now!</a>
            </div>
        </div>
    </div>
</div>