<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $mailData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['data'] = $this->mailData;
        $data['url'] = env('LIGHTBOXLIGHTING_URL').'?url='.url('tender/submission-form/'.$this->mailData->stage->ps_id.'/'.$this->mailData->pst_id);
        // $data['url'] = url('tender/submission-form/'.$this->mailData->stage->ps_id.'/'.$this->mailData->pst_id);
        return $this->subject('Credentials to Access Submission Form (Lightbox)')->view('email.send-credentials', $data);
    }
}
