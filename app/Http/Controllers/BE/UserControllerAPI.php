<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Masterdata\MsUsers;
use App\Models\Masterdata\MsCompany;
use App\Http\Traits\FileTrait;

class UserControllerAPI extends Controller
{

    use FileTrait;

    public function dt()
    {
        $data = MsUsers::with('role')->get();
        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                $action = '
                    <a class="dropdown-item d-flex align-items-center text-secondary" style="gap:5px" href="'.url("/users/profile/".$db->user_id).'">
                        <i style="font-size:18px"  class="bx bx-user " ></i>
                        <span>Profile</span>
                    </a>
                    <a class="dropdown-item d-flex align-items-center text-secondary" style="gap:5px" href="javascript:edit(\''.$db->user_id.'\')">
                        <i style="font-size:18px"  class="bx bx-edit " ></i>
                        <span>Edit</span>
                    </a>
                    <a class="dropdown-item d-flex align-items-center text-secondary" style="gap:5px" href="javascript:del(\''.$db->user_id.'\')">
                        <i style="font-size:18px"  class="bx bx-trash " ></i>
                        <span>Delete</span>
                    </a>
                '; 
                return '
                    <div class="btn-group dropend" style="">
                        <button type="button" class="btn btn-action rounded-pill btn-icon" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu" style="">
                            '.$action.'
                        </div>
                    </div>
                '; 
            })
            ->editColumn('user_role', function($db){
                return $db->role->role_name ?? '-';
            })
            ->rawColumns(['action'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = MsUsers::find($request->id) ?? new MsUsers();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if(isset($inp['user_raw_password'])){
                $pass = '$BSSVPRNHGB$' . substr(md5(md5($inp['user_raw_password'])), 0, 50);
                $dbs->user_password = $pass;
            }
            $dbs->save();

            if($request->file('user_photo')){
                $dbs->user_photo = $this->upload_file($request->file('user_photo'), 'public\\User\\'.$dbs->user_id);
            }

            if($request->id == session()->get('userId')){
                Session::put('userFullname', $dbs->user_name);
                Session::put('userPhoto', $dbs->user_photo);
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        return MsUsers::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            MsUsers::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //-----------------------------------------------------------------------

    public function getByIdCompany($id)
    {
        $data = MsCompany::where('user_id',$id)->first();
        if($data){
            return $data->toJson();
        } else {
            return json_encode([]);
        }
    }

    public function saveCompany(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = MsCompany::where('user_id', $request->user_id)->first() ?? new MsCompany();
            if(!$dbs->user_id) $dbs->user_id = $request->user_id;

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            } 
            $dbs->save();

            if($request->file('company_photo')){
                $dbs->company_photo = $this->upload_file($request->file('company_photo'), 'public\\Company\\'.$dbs->user_id);
            }

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

}
