<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

use App\Models\Products;  

class ProductsAPI extends Controller
{
    public function check_empty_product(){
        $data = Products::whereNull('pr_name')
            ->whereNull('pr_application')->whereNull('pr_brand')
            ->get();

        foreach($data as $item){
            $this->delF($item->pr_id);
        }
    } 
    
    public function list(Request $request)
    {  
        $this->check_empty_product();
        $uid = session()->get('userId');
         
        $all_products = $request->all_products;
        if($all_products && $all_products == 'true'){
            $data = Products::with('owner');
        } else
            $data = Products::with('owner')->where('user_id', $uid);
        $data = $data->orderBy('pr_application')->orderBy('pr_brand');

        return datatables($data) 
            ->addColumn('checkbox', function ($db) {
                return '
                    <input type="checkbox" class="form-check-input" name="selected_products[]" value="'.$db->pr_id.'">
                ';
            })
            ->addColumn('action', function ($db) {
                $action = '
                    <a class="dropdown-item d-flex align-items-center text-secondary" style="gap:5px" href="javascript:edit(\''.$db->pr_id.'\')">
                        <i style="font-size:18px" class="bx bx-edit"></i>
                        <span>Edit</span>
                    </a>
                    <a class="dropdown-item d-flex align-items-center text-secondary" style="gap:5px" href="javascript:delF(\''.$db->pr_id.'\')">
                        <i style="font-size:18px" class="bx bx-trash"></i>
                        <span>Delete</span>
                    </a>
                '; 

                return '
                    <div class="btn-group dropend" style="">
                        <button type="button" class="btn btn-action rounded-pill btn-icon" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu" style="">
                            '.$action.'
                        </div>
                    </div>
                ';
            }) 
            ->editColumn('pr_name', function($db) use($uid){
                $url = url('getimage/'.base64_encode($db->pr_main_photo));
                $is_owner = $uid == $db->user_id;
                return '
                <div class="d-flex align-items-center" style="gap:10px">
                    <img src="'.$url.'" alt="image" class="rounded" style="height:80px;width:80px;object-fit:contain">
                    <div>
                        <a href="'.url('masterdata/products/'.$db->pr_id.'/form').'" class="text-primary fw-bolder"><strong>'.($db->pr_name ?? null).'</strong>
                            '.(!$is_owner ? '
                                <div class="badge bg-primary">
                                    '.$db->owner->user_name.' is the Owner
                                </div>
                            ' : '').'
                        </a>
                        <div>'.$db->pr_model.'</div>
                    </div>
                </div>
                ';
            })
            ->rawColumns(['action', 'pr_name', 'checkbox'])->toJson();
    }

    public function getById($id)
    {
        return Products::find($id)->toJson();
    }

    public function uploadImage(Request $request){
        $data = $request->image;
        $key = $request->key;
        $id = $request->id;

        $dbs = Products::find($request->id);

        $image_array_1 = explode(";", $data);
        $image_array_2 = explode(",", $image_array_1[1]);
        $data = base64_decode($image_array_2[1]);

        $type = [
            'pr_main_photo' => 'main',
            'pr_dimension_photo' => 'dimension',
            'pr_accessories_photo' => 'accessories',
        ];

        $path = 'public\\Product\\'.$id.'.'.$type[$key].'.png';
        Storage::disk('local')->put($path, $data);
        $dbs[$key] = $path;
        $dbs->save();

        return url('getimage/'.base64_encode($path));
    }

    public function saveF(Request $request)
    {
        try {
            $inp = $request->inp;
            $uid = session()->get('userId');
            $dbs = Products::find($request->id) ?? new Products();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }
            if(!$dbs->user_id){
                $dbs->user_id = $uid;
            }
            $dbs->save(); 

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }


    public function delF($id)
    {
        try {
            $data = Products::find($id);  

            // delete data
            $data->delete();


            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    } 
}
