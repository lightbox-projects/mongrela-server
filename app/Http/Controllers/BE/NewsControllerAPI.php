<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\News;
use App\Http\Traits\FileTrait;

class NewsControllerAPI extends Controller
{

    use FileTrait;

    public function dt()
    {
        $data = News::all();
        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                $action = ' 
                    <a class="dropdown-item d-flex align-items-center text-secondary" style="gap:5px" href="javascript:edit(\''.$db->news_id.'\')">
                        <i style="font-size:18px"  class="bx bx-edit " ></i>
                        <span>Edit</span>
                    </a>
                    <a class="dropdown-item d-flex align-items-center text-secondary" style="gap:5px" href="javascript:del(\''.$db->news_id.'\')">
                        <i style="font-size:18px"  class="bx bx-trash " ></i>
                        <span>Delete</span>
                    </a>
                '; 
                return '
                    <div class="btn-group dropend" style="">
                        <button type="button" class="btn btn-action rounded-pill btn-icon" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu" style="">
                            '.$action.'
                        </div>
                    </div>
                '; 
            }) 
            ->rawColumns(['action'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = News::find($request->id) ?? new News();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            } 
            $dbs->news_date = date('d-m-Y');
            $dbs->save();

            if($request->file('news_photo')){
                $dbs->news_photo = $this->upload_file($request->file('news_photo'), 'public\\News\\'.$dbs->news_id);
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        return News::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            News::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
