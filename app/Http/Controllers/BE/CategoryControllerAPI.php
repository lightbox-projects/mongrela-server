<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Masterdata\Category;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class CategoryControllerAPI extends Controller
{
    public function dt()
    {
        $data = Category::all();
        return datatables($data)
            ->addIndexColumn()
            ->addColumn('action', function ($db) {
                $action = ' 
                    <a class="dropdown-item d-flex align-items-center text-secondary" style="gap:5px" href="javascript:edit(\''.$db->category_id.'\')">
                        <i style="font-size:18px"  class="bx bx-edit " ></i>
                        <span>Edit</span>
                    </a>
                    <a class="dropdown-item d-flex align-items-center text-secondary" style="gap:5px" href="javascript:del(\''.$db->category_id.'\')">
                        <i style="font-size:18px"  class="bx bx-trash " ></i>
                        <span>Delete</span>
                    </a>
                '; 
                return '
                    <div class="btn-group dropend" style="">
                        <button type="button" class="btn btn-action rounded-pill btn-icon" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu" style="">
                            '.$action.'
                        </div>
                    </div>
                '; 
            })
            ->editColumn('category_name', function($db){
                $url = url('getimage/'.base64_encode($db->category_photo));
                return '
                    <div class="d-flex align-items-center" style="gap:10px">
                        <img src="'.$url.'" style="height:80px;width:80px;object-fit:scale-down" />
                        <div>
                            '.$db->category_name.'
                        </div>
                    </div>
                ';
            })
            ->rawColumns(['action', 'category_name'])->toJson();
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Category::find($request->id) ?? new Category();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            $dbs->save(); 

            if($request->hasFile('category_photo')){
                $file = $request->file('category_photo');
                $extension = $file->getClientOriginalExtension();
                $path = 'public\\Category\\'.$dbs->category_id.'.'.$extension;
                Storage::disk('local')->put($path, file_get_contents($file));
                $dbs->category_photo = $path;
                $dbs->save(); 
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        return Category::find($id)->toJson();
    }

    public function delete($id)
    {
        try {
            Category::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
