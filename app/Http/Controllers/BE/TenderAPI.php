<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

use App\Models\Masterdata\Tags;  
use App\Models\Project;  
use App\Models\ProjectProducts;  
use App\Models\Products;  
use App\Models\Bids;  
use App\Models\BidProduct;  
use App\Models\ProductProjectAssigned;  

class TenderAPI extends Controller
{
    public function check_empty_project(){
        $data = Project::whereNull('prj_name')->get();

        foreach($data as $item){
            $data = Project::find($item->prj_id);  
            $data->forceDelete();
        }
    } 
    
    public function list(Request $request)
    {  
        $this->check_empty_project();
        $uid = session()->get('userId');

        // dd($request);

        if(isset($request->tender_bid)){
            $prj_ids = Bids::where('user_id', $uid)->pluck('prj_id', 'prj_id');
            $data = Project::with('products', 'owner.company','bids','category')->whereIn('prj_id', $prj_ids)->orderBy('prj_published_date', 'desc');
        } else if(isset($request->offsetLatest)){
            $data = Project::with('products', 'owner.company','bids','category')->where('prj_visibility', 'public')->orderBy('prj_published_date', 'DESC')->offset(3 + (($request->page-1) * $request->pageRange))
                ->limit($request->pageRange); 
        } else {
            $data = Project::with('products','bids','category')->where('user_id', $uid)->orderBy('prj_id', 'DESC'); 
        }

        if($request->category) {
            $data->where('prj_category', 'like', '%'.$request->category.'%');
        }
        if($request->tags) {
            $data->where('prj_tags', 'like', '%'.$request->tags.'%');
        }
        if($request->start_date) {
            $data->whereDate('prj_tender_start', '<=', $request->start_date);
        }
        if($request->end_date) {
            $data->where('prj_tender_end', '<=', $request->end_date);
        }
        if($request->location) {
        }

        return datatables($data)  
            ->addColumn('action', function ($db) {
                $action = '
                    <a class="dropdown-item d-flex align-items-center text-secondary" style="gap:5px" href="javascript:edit(\''.$db->prj_id.'\')">
                        <i style="font-size:18px" class="bx bx-edit"></i>
                        <span>Edit</span>
                    </a> 
                '; 

                return '
                    <div class="btn-group dropend" style="">
                        <button type="button" class="btn btn-action rounded-pill btn-icon" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu" style="">
                            '.$action.'
                        </div>
                    </div>
                ';
            })  
            ->addColumn('items', function($db){
                return count($db->products) . ' items';
            })
            ->addColumn('bids', function($db){
                return '<a class="text-primary" href="'.url('tender-management/'.$db->prj_id.'/form/bids').'">'.count($db->bids) . ' bids</a>';
            })
            ->addColumn('periode', function($db){
                return date('d F Y', strtotime($db->prj_tender_start)) . ' - ' . date('d F Y', strtotime($db->prj_tender_end));
            })
            ->editColumn('prj_expected_completion_date', function($db){
                return date('d F Y', strtotime($db->prj_expected_completion_date));
            })
            ->editColumn('prj_name', function($db){
                return '<a class="text-primary" href="'.url('tender-management/'.$db->prj_id.'/form').'">'.$db->prj_name.'</a>';
            })
            ->rawColumns(['action','periode','items','prj_name','bids'])->toJson();
    }

    public function getById($id)
    {
        return Project::find($id)->toJson();
    } 

    public function saveF(Request $request)
    {
        try {
            $inp = $request->inp;
            $uid = session()->get('userId');
            $dbs = Project::find($request->id) ?? new Project();

            if(isset($request->tags) && $request->tags){
                $tag = '';
                foreach($request->tags as $item){
                    $val = strtolower($item);
                    $tag .= '#'.$val . ' ';
                    $data = Tags::firstOrCreate([
                        'tag_name' => $val
                    ]);
                }
                $dbs->prj_tags = $tag;
            }

            if($inp)
            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }

            if(!$dbs->user_id){
                $dbs->user_id = $uid;
            }

            if(isset($inp['prj_visibility'])){
                if(!$dbs->prj_published_date){
                    $dbs->prj_published_date = date('Y-m-d');
                }
            }
            
            if(isset($inp['winner_bid_id'])){ 
                $dbs->winner_elected_date = date('Y-m-d');
                $dbs->prj_visibility = 'closed';
            }

            $dbs->save(); 
            
            if($request->hasFile('prj_photo')){
                $file = $request->file('prj_photo');
                $extension = $file->getClientOriginalExtension();
                $path = 'public\\Project\\'.$dbs->prj_id.'.'.$extension;
                Storage::disk('local')->put($path, file_get_contents($file));
                $dbs->prj_photo = $path;
                $dbs->save(); 
            } 

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }


    public function delF($id)
    {
        try {
            $data = Project::find($id);  
            // delete data
            $data->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    } 

    public function assignProduct(Request $request)
    {
        try {
            $prj_id = $request->prj_id;
            $pr_ids = explode(',', $request->pr_ids);

            foreach($pr_ids as $id){
                $product = Products::find($id)->toArray();
                $offered = new ProductProjectAssigned();
                foreach($product as $key => $val){
                    if($key != 'pr_id')
                    $offered[$key] = $val;
                }
                $offered->pr_quantity = 1;
                $offered->pr_id_original = $id;
                $offered->save();

                $dbs = new ProjectProducts();
                $dbs->prj_id = $prj_id;
                $dbs->pr_id = $offered->pr_id;
                $dbs->save();
            }

            $dbs->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menyimpan data',
                'id' => $dbs->ps_id
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function saveFProdukOffered(Request $request)
    {
        try {
            $inp = $request->inp;
            $id = $request->id;
            $dbs = ProductProjectAssigned::find($request->id) ?? new ProductProjectAssigned();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }
            $dbs->save();

            $type = [
                'pr_main_photo' => 'main',
                'pr_dimension_photo' => 'dimension',
                'pr_accessories_photo' => 'accessories',
            ];

            foreach($request->file() as $key => $file){
                // upload to storage
                $extension = $file->getClientOriginalExtension();
                $path = 'public\\Offered\\'.$id.'.'.$type[$key].'.'.$extension;
                Storage::disk('local')->put($path, file_get_contents($file));
                $dbs[$key] = $path;
            }

            $dbs->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }


    public function delFProdukOffered($id)
    {
        try {

            $psp = ProjectProducts::where('pr_id', $id)->first();
            $data = ProductProjectAssigned::find($id);

            $psp->delete();
            $data->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Gagal menghapus data',
        ]);
    }

    public function calcScore($prj_id, $uid){
        $bid = Bids::where('user_id', $uid)->where('prj_id', $prj_id)->first();
        $product = BidProduct::where('bidder_id', $uid)->where('bid_id', $bid->bid_id)->sum('evaluation_score');
        $bid->bid_score = $product;
        $bid->save();
    }

    public function evaluateBids(Request $request){
        $prj_id = $request->prj_id;
        $pr_id = $request->pr_id;

        foreach($request->checked as $id => $item){
            $len = count($item);
            $val = json_encode($item);
            $bid = Bids::where('user_id', $id)->where('prj_id', $prj_id)->first();
            $product = BidProduct::where('bidder_id', $id)->where('bid_id', $bid->bid_id)->where('pr_id_original', $pr_id)->first();
            $product->evaluation_raw = $val;
            $product->evaluation_score = $len;
            $product->save();

            $this->calcScore($prj_id, $id);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menghapus data',
        ]);
    }
}
