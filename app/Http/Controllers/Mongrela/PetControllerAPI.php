<?php

namespace App\Http\Controllers\Mongrela;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\Pets;

use App\Http\Traits\FileTrait;
use Carbon\Carbon;

class PetControllerAPI extends Controller
{ 

    use FileTrait;

    public function getPetList(Request $request){
        $user_id = $request->user_id;
        $datas = Pets::where('user_id', $user_id)->get();
        foreach($datas as $data){
            $data->pet_picture = url('getimage/'.base64_encode($data->pet_picture));
        }

        return $datas;
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Pets::find($request->id) ?? new Pets();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            $dbs->save(); 

            if($request->hasFile('pet_picture')){
                $file = $request->file('pet_picture'); 
                $filename = 'public\\Mongrela\\PetPicture\\'.$dbs->pet_id;
                $path = $this->upload_file($file, $filename);
                $dbs->pet_picture = $path;
                $dbs->save(); 
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        $data = Pets::with(['owner', 'walker.note', 'walker.schedules' => function($q){
            $q->whereDate('ws_date', '>=', Carbon::now())->orderBy('ws_date');
        }])->find($id);
        $data->pet_picture = url('getimage/'.base64_encode($data->pet_picture));

        $schedules = $data->walker->schedules;
        $lists = [];
        foreach($schedules as $item){
            $item_t = $item->toArray();
            $lists[$item_t['ws_date']][] = $item->toArray();
        }

        $data->schedules_ = $lists;

        return $data;
    }

    public function delete($id)
    {
        try {
            Pets::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
