<?php

namespace App\Http\Controllers\Mongrela;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request; 

use App\Http\Traits\FileTrait;
use Carbon\Carbon;

use App\Models\Mongrela\Trainer;
use App\Models\Mongrela\Shelter;
use App\Models\Mongrela\Store;

class ApprovalController extends Controller
{ 

    use FileTrait;
    
    public function trainer(){
        $data = [];
        return view('_mongrela.trainer-approval', $data);
    } 

    public function trainer_dt(){
        $data = Trainer::with('owner')->get();

        return datatables($data)
            ->addColumn('action', function ($db) {
                $action = '';
                if($db->is_approved == 'n')
                    $action = '
                        <button class="dropdown-item d-flex align-items-center text-success" style="gap:5px" onclick="change_approval(\''.$db->trainer_id.'\', \'y\')">
                            <i style="font-size:18px" class="bx bx-check"></i>
                            <span>Approve</span>
                        </button> 
                    '; 
                else
                    $action = '
                        <a class="dropdown-item d-flex align-items-center text-danger" style="gap:5px" href="javascript:change_approval(\''.$db->trainer_id.'\', \'n\')">
                            <i style="font-size:18px" class="bx bx-check"></i>
                            <span>Cancel Permission</span>
                        </a> 
                    '; 

                return '
                    <div class="btn-group dropend" style="">
                        <button type="button" class="btn btn-action rounded-pill btn-icon" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu" style="">
                            '.$action.'
                        </div>
                    </div>
                ';
            }) 
            ->addColumn('trainer_name', function($db){
                return $db->owner->user_name;
            })
            ->editColumn('is_approved', function($db){
                if($db->is_approved == 'y'){
                    return '<div class="badge bg-success">Approved</div>';
                } else {
                    return '<div class="badge bg-danger">Waiting For Approval</div>';
                }
            })
            ->rawColumns(['is_approved', 'action'])->toJson();
    }

    public function trainer_approval(Request $request){
        $id = $request->id;
        $approval = $request->approval;

        
        $data = Trainer::find($id);
        $data->is_approved = $approval;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Success to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // SHELTER
    //----------------------------------------------------------------------- 
    
    public function shelter(){
        $data = [];
        return view('_mongrela.shelter-approval', $data);
    } 

    public function shelter_dt(){
        $data = Shelter::all();

        return datatables($data)
            ->addColumn('action', function ($db) {
                $action = '';
                if($db->is_approved == 'n')
                    $action = '
                        <button class="dropdown-item d-flex align-items-center text-success" style="gap:5px" onclick="change_approval(\''.$db->shelter_id.'\', \'y\')">
                            <i style="font-size:18px" class="bx bx-check"></i>
                            <span>Approve</span>
                        </button> 
                    '; 
                else
                    $action = '
                        <a class="dropdown-item d-flex align-items-center text-danger" style="gap:5px" href="javascript:change_approval(\''.$db->shelter_id.'\', \'n\')">
                            <i style="font-size:18px" class="bx bx-check"></i>
                            <span>Cancel Permission</span>
                        </a> 
                    '; 

                return '
                    <div class="btn-group dropend" style="">
                        <button type="button" class="btn btn-action rounded-pill btn-icon" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu" style="">
                            '.$action.'
                        </div>
                    </div>
                ';
            }) 
            ->editColumn('is_approved', function($db){
                if($db->is_approved == 'y'){
                    return '<div class="badge bg-success">Approved</div>';
                } else {
                    return '<div class="badge bg-danger">Waiting For Approval</div>';
                }
            })
            ->rawColumns(['is_approved', 'action'])->toJson();
    }

    public function shelter_approval(Request $request){
        $id = $request->id;
        $approval = $request->approval;

        
        $data = Shelter::find($id);
        $data->is_approved = $approval;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Success to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // PET SHOP
    //----------------------------------------------------------------------- 

    public function store(){
        $data = [];
        return view('_mongrela.store-approval', $data);
    } 

    public function store_dt(){
        $data = Store::all();

        return datatables($data)
            ->addColumn('action', function ($db) {
                $action = '';
                if($db->is_approved == 'n')
                    $action = '
                        <button class="dropdown-item d-flex align-items-center text-success" style="gap:5px" onclick="change_approval(\''.$db->store_id.'\', \'y\')">
                            <i style="font-size:18px" class="bx bx-check"></i>
                            <span>Approve</span>
                        </button> 
                    '; 
                else
                    $action = '
                        <a class="dropdown-item d-flex align-items-center text-danger" style="gap:5px" href="javascript:change_approval(\''.$db->store_id.'\', \'n\')">
                            <i style="font-size:18px" class="bx bx-check"></i>
                            <span>Cancel Permission</span>
                        </a> 
                    '; 

                return '
                    <div class="btn-group dropend" style="">
                        <button type="button" class="btn btn-action rounded-pill btn-icon" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu" style="">
                            '.$action.'
                        </div>
                    </div>
                ';
            }) 
            ->editColumn('is_approved', function($db){
                if($db->is_approved == 'y'){
                    return '<div class="badge bg-success">Approved</div>';
                } else {
                    return '<div class="badge bg-danger">Waiting For Approval</div>';
                }
            })
            ->rawColumns(['is_approved', 'action'])->toJson();
    }

    public function store_approval(Request $request){
        $id = $request->id;
        $approval = $request->approval;

        
        $data = Store::find($id);
        $data->is_approved = $approval;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Success to save data',
        ]);
    }
}
