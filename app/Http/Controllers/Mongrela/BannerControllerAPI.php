<?php

namespace App\Http\Controllers\Mongrela;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\Banners;
use App\Models\Mongrela\Ads;

use App\Http\Traits\FileTrait;
use Carbon\Carbon;

class BannerControllerAPI extends Controller
{

    use FileTrait;

    public function banner()
    {
        $data = [];
        return view('_mongrela.banners', $data);
    }
    public function get_banners()
    {
        $datas = Banners::where('is_active', 'y')->get();
        foreach($datas as $item){
            $item->banner_picture = url('getimage/'.base64_encode($item->banner_picture));
        }
        return $datas;
    }

    public function banner_dt()
    {
        $datas = Banners::all();
        return datatables($datas)
            ->addColumn('action', function ($db) {
                $action = '
                    <button class="dropdown-item d-flex align-items-center text-danger" style="gap:5px" onclick="del(\'' . $db->banner_id . '\')">
                        <i style="font-size:18px" class="bx bx-trash"></i>
                        <span>Delete</span>
                    </button> 
                ';

                if($db->is_active == 'n')
                    $action .= '
                        <button class="dropdown-item d-flex align-items-center text-success" style="gap:5px" onclick="change_approval(\''.$db->banner_id.'\', \'y\')">
                            <i style="font-size:18px" class="bx bx-check"></i>
                            <span>Activate</span>
                        </button> 
                    '; 
                else
                    $action .= '
                        <a class="dropdown-item d-flex align-items-center text-danger" style="gap:5px" href="javascript:change_approval(\''.$db->banner_id.'\', \'n\')">
                            <i style="font-size:18px" class="bx bx-check"></i>
                            <span>Diactivate</span>
                        </a> 
                    '; 

                return '
                <div class="btn-group dropend" style="">
                    <button type="button" class="btn btn-action rounded-pill btn-icon" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bx bx-dots-vertical-rounded"></i>
                    </button>
                    <div class="dropdown-menu" style="">
                        ' . $action . '
                    </div>
                </div>
            ';
            })
            ->editColumn('banner_picture', function ($db) {
                $url = url('getimage/'.base64_encode($db->banner_picture));
                return '<img class="w-100 rounded" src="' . $url . '" style="max-height:150px;max-width:150px; object-fit:cover" />';
            })
            ->editColumn('is_active', function($db){
                if($db->is_active == 'y'){
                    return '<div class="badge bg-success"><i class="bx bx-check"></i></div>';
                } else {
                    return '<div class="badge bg-danger"><i class="bx bx-x"></i></div>';
                }
            })
            ->rawColumns(['banner_picture', 'action', 'is_active'])->toJson();
    }

    public function banner_save(Request $request)
    {
        // $banner_picture = $request->banner_picture;

        if($request->hasFile('banner_picture')){
            $data = new Banners();
            $file = $request->file('banner_picture'); 
            $filename = 'public\\Mongrela\\BannerFile\\'.$file->getClientOriginalName();
            $path = $this->upload_file($file, $filename);
            $data->banner_picture = $path;
            $data->save();
        } 

        // $data = new Banners();
        // $data->banner_picture = $banner_picture;
        // $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Success to save data',
        ]);
    }
    
    public function banner_delete($id){
        $data = Banners::find($id)->delete();
        
        return response()->json([
            'status' => 'success',
            'message' => 'Success to save data',
        ]);
    }

    public function banner_active(Request $request){
        $id = $request->id;
        $active = $request->approval;

        $data = Banners::find($id);
        $data->is_active = $active;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Success to save data',
        ]);
    }

    public function ads()
    {
        $data = [];
        return view('_mongrela.ads', $data);
    }

    public function get_ads()
    {
        $datas = Ads::where('is_active', 'y')->get();
        foreach($datas as $item){
            $item->ads_picture = url('getimage/'.base64_encode($item->ads_picture));
        }
        return $datas;
    }

    public function ads_dt()
    {
        $datas = Ads::all();
        return datatables($datas)
            ->addColumn('action', function ($db) {
                $action = '
                    <button class="dropdown-item d-flex align-items-center text-danger" style="gap:5px" onclick="del(\'' . $db->ads_id . '\')">
                        <i style="font-size:18px" class="bx bx-trash"></i>
                        <span>Delete</span>
                    </button> 
                ';

                if($db->is_active == 'n')
                    $action .= '
                        <button class="dropdown-item d-flex align-items-center text-success" style="gap:5px" onclick="change_approval(\''.$db->ads_id.'\', \'y\')">
                            <i style="font-size:18px" class="bx bx-check"></i>
                            <span>Activate</span>
                        </button> 
                    '; 
                else
                    $action .= '
                        <a class="dropdown-item d-flex align-items-center text-danger" style="gap:5px" href="javascript:change_approval(\''.$db->ads_id.'\', \'n\')">
                            <i style="font-size:18px" class="bx bx-check"></i>
                            <span>Diactivate</span>
                        </a> 
                    '; 

                return '
                <div class="btn-group dropend" style="">
                    <button type="button" class="btn btn-action rounded-pill btn-icon" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bx bx-dots-vertical-rounded"></i>
                    </button>
                    <div class="dropdown-menu" style="">
                        ' . $action . '
                    </div>
                </div>
            ';
            })
            ->editColumn('ads_picture', function ($db) {
                $url = url('getimage/'.base64_encode($db->ads_picture));
                return '<img class="w-100 rounded" src="' . $url . '" style="max-height:150px;max-width:150px; object-fit:cover" />';
            })
            ->editColumn('is_active', function($db){
                if($db->is_active == 'y'){
                    return '<div class="badge bg-success"><i class="bx bx-check"></i></div>';
                } else {
                    return '<div class="badge bg-danger"><i class="bx bx-x"></i></div>';
                }
            })
            ->rawColumns(['ads_picture', 'action', 'is_active'])->toJson();
    }

    public function ads_save(Request $request)
    {
        // $ads_picture = $request->ads_picture;

        if($request->hasFile('ads_picture')){
            $data = new Ads();
            $file = $request->file('ads_picture'); 
            $filename = 'public\\Mongrela\\AdsFile\\'.$file->getClientOriginalName();
            $path = $this->upload_file($file, $filename);
            $data->ads_picture = $path;
            $data->save();
        } 
 

        return response()->json([
            'status' => 'success',
            'message' => 'Success to save data',
        ]);
    }
    
    public function ads_delete($id){
        $data = Ads::find($id)->delete();
        
        return response()->json([
            'status' => 'success',
            'message' => 'Success to save data',
        ]);
    }

    public function ads_active(Request $request){
        $id = $request->id;
        $active = $request->approval;

        $data = Ads::find($id);
        $data->is_active = $active;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Success to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}