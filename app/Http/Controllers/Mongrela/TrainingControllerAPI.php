<?php

namespace App\Http\Controllers\Mongrela;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\Training;
use App\Models\Mongrela\TrainingSchedule;
use App\Models\Mongrela\TrainingBooked;
use App\Models\Mongrela\Chat;

use App\Http\Traits\FileTrait;
use Carbon\Carbon;

class TrainingControllerAPI extends Controller
{ 

    use FileTrait;

    public function list(Request $request){
        $page = $request->page ?? 1;
        $limit = $request->limit ?? 10;
        $offset = ($page - 1) * $limit;
        
        $user_id = $request->user_id;

        $datas = Training::with('owner', 'schedules');

        if($user_id){
            $datas = $datas->where('user_id', $user_id);
        }

        $datas = $datas->skip($offset)->take($limit)->get();
        return $datas;
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Training::find($request->id) ?? new Training();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        $data = Training::with(['owner.trainer', 'schedules' => function($q){
            $q->whereDate('trs_date', '>=', Carbon::now());
        }])->find($id);
        $schedules = $data->schedules;

        if($data->owner->user_picture)
        $data->owner->user_picture = url('getimage/'.base64_encode($data->owner->user_picture));
        
        $lists = [];
        foreach($schedules as $item){
            $item_t = $item->toArray();
            $lists[$item_t['trs_date']][] = $item->toArray();
        }

        $data->schedules_ = $lists;
        return $data;
    }

    public function delete($id)
    {
        try {
            Training::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // SCHEDULES
    //----------------------------------------------------------------------- 
    
    public function getSchedule($id)
    {
        $data = TrainingSchedule::with('training.owner')->find($id); 
        return $data;
    }

    public function saveSchedule(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = TrainingSchedule::find($request->id) ?? new TrainingSchedule();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function deleteSchedule($id)
    {
        try {
            TrainingSchedule::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Book Training
    //----------------------------------------------------------------------- 

    public function getInvoice($id)
    {
        $data = TrainingBooked::with('schedule.training', 'pet')->find($id); 
        return $data;
    }

    public function bookTraining(Request $request){
        try {
            $inp = $request->inp;
            $dbs = TrainingBooked::find($request->id) ?? new TrainingBooked();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            
            $dbs->save();

            $id = $dbs->trb_id;
            $data = TrainingBooked::with('schedule.training', 'pet')->find($id);

            $chat = new Chat();
            $chat->user_id = $inp['trb_user_id'];
            $chat->chat_user_target = $data->schedule->training->user_id;
            $chat->chat_text = 'Training '.$data->schedule->training->tr_name. ' Booked for '
                .$data->pet->pet_name.' on '.$data->schedule->trs_date. ' '.$data->schedule->trs_time
                .'.';
            $chat->save();
            
            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
                'id' => $dbs->trb_id
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }


    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
