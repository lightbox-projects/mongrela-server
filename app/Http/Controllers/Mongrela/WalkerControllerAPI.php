<?php

namespace App\Http\Controllers\Mongrela;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\WalkerPetsAvailable;
use App\Models\Mongrela\WalkerNote;
use App\Models\Mongrela\WalkerSchedule;
use App\Models\Mongrela\WalkerBooked;
use App\Models\Mongrela\Chat;

use App\Http\Traits\FileTrait;

class WalkerControllerAPI extends Controller
{ 

    use FileTrait;
 
    public function list_pets(Request $request){
        $page = $request->page ?? 1;
        $limit = $request->limit ?? 10;
        $offset = ($page - 1) * $limit;
        
        $datas = WalkerPetsAvailable::with('pet.owner')->where('availability', 'y');
        $datas = $datas->skip($offset)->take($limit)->get();
        
        foreach($datas as $item){
            if($item->pet->pet_picture){
                $item->pet->pet_picture = url('getimage/'.base64_encode($item->pet->pet_picture));
            }
        }

        return $datas;
    }

    public function save_walker(Request $request){
        try {
            $inp = $request->inp;
            $dbs = WalkerPetsAvailable::firstOrCreate([
                'pet_id' => $inp['pet_id']
            ]);

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function get_walker($id)
    {
        $data = WalkerPetsAvailable::with('note', 'pet.owner', 'schedules')->find($id); 
         
        return $data;
    }
 
    //-----------------------------------------------------------------------
    // NOTE
    //----------------------------------------------------------------------- 

    public function list_note(Request $request){
        $datas = WalkerNote::where('wpa_id', $request->id)->get();
        return $datas;
    }

    public function save_note(Request $request){
        try {
            $inp = $request->inp;
            $dbs = WalkerNote::find($request->id) ?? new WalkerNote();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function delete_note($id)
    {
        try {
            WalkerNote::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // SCHEDULES
    //----------------------------------------------------------------------- 
    
    public function get_schedule($id)
    {
        $data = WalkerSchedule::with('walker.pet', 'walker.note')->find($id); 
        return $data;
    }

    public function save_schedule(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = WalkerSchedule::find($request->id) ?? new WalkerSchedule();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function delete_schedule($id)
    {
        try {
            WalkerSchedule::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 


    public function get_invoice($id)
    {
        $data = WalkerBooked::with('walker.pet', 'walker.note', 'schedule')->find($id); 
        return $data;
    }

    public function book_walker(Request $request){
        try {
            $inp = $request->inp;
            $dbs = WalkerBooked::find($request->id) ?? new WalkerBooked();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  

            if ($dbs->save()) {

                $data = WalkerBooked::with('walker.pet', 'walker.note', 'schedule')->find($dbs->wb_id);

                $chat = new Chat();
                $chat->user_id = $inp['user_id'];
                $chat->chat_user_target = $data->walker->pet->user_id;
                $chat->chat_text = 'Pet '.$data->walker->pet->pet_name. ' Booked by '
                    .$data->booked_by->user_name.' for a walk on '.$data->schedule->ws_date. ' '.$data->schedule->ws_time
                    .'.';
                $chat->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                    'id' => $dbs->wb_id
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
