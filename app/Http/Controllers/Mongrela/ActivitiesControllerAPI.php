<?php

namespace App\Http\Controllers\Mongrela;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\Users;
use App\Models\Mongrela\Trainer;
use App\Models\Mongrela\TrainingBooked;
use App\Models\Mongrela\WalkerBooked;
use App\Models\Mongrela\ShelterVolunteerApplicant;

use App\Http\Traits\FileTrait;
use Carbon\Carbon;

class ActivitiesControllerAPI extends Controller
{ 

    use FileTrait;

    public function get_my_activities(Request $request){
        $user_id = $request->user_id;

        $user = Users::with('trainer')->find($user_id);
        
        $activities = [];

        // TRAINING
        if($user->trainer){
            $trainings = TrainingBooked::with('schedule.training', 'pet')
            ->whereHas('schedule.training', function($q) use($user_id){
                return $q->where('user_id', $user_id);
            })
            ->whereHas('schedule', function($q){
                return $q->whereDate('trs_date', '>=', Carbon::now());
            })
            ->get();
            foreach($trainings as $item){
                $activities[$item->schedule->trs_date][] = [
                    'type' => 'training',
                    'data' => $item
                ];
            }
        }
        // END TRAINING

        // WALKER
        $walks = WalkerBooked::with('walker.pet', 'walker.note', 'schedule', 'booked_by')
        ->whereHas('schedule', function($q){
            return $q->whereDate('ws_date', '>=', Carbon::now());
        })
        ->where('user_id', $user_id)->get();
        
        foreach($walks as $item){
            $activities[$item->schedule->ws_date][] = [
                'type' => 'walker',
                'data' => $item
            ];
        }
        // END WALKER

        // VOLUNTEER
        $shelter_volunteer = ShelterVolunteerApplicant::with('volunteer_event.shelter')->where('user_id', $user_id)->get();
        foreach($shelter_volunteer as $item){
            $item->volunteer_event->shv_picture = url('getimage/'.base64_encode($item->volunteer_event->shv_picture));
            $activities[$item->volunteer_event->shv_date][] = [
                'type' => 'volunteer',
                'data' => $item
            ];
        }
        
        ksort($activities);

        return $activities;
    }

    public function get_pet_activities(Request $request){
        $user_id = $request->user_id;

        $user = Users::with('pets')->find($user_id);
        $pet_id = $user->pets->pluck('pet_id', 'pet_id');

        $activities = [];

        // TRAINING
        $trainings = TrainingBooked::with('schedule.training', 'pet') 
        ->whereHas('schedule', function($q){
            return $q->whereDate('trs_date', '>=', Carbon::now());
        })
        ->where('trb_user_id', $user->user_id)
        ->get();

        if(count($trainings) > 0){
            foreach($trainings as $item){
                $activities[$item->schedule->trs_date][] = [
                    'type' => 'training',
                    'data' => $item
                ];
            }
        }

        // WALKER
        $walks = WalkerBooked::with('walker.pet', 'walker.note', 'schedule', 'booked_by')
        ->whereHas('schedule', function($q){
            return $q->whereDate('ws_date', '>=', Carbon::now());
        })
        ->whereHas('walker', function($q) use($pet_id){
            return $q->whereIn('pet_id', $pet_id);
        })->get();
        
        foreach($walks as $item){
            $activities[$item->schedule->ws_date][] = [
                'type' => 'walker',
                'data' => $item
            ];
        }
        // END WALKER

        ksort($activities);

        return $activities;
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
