<?php

namespace App\Http\Controllers\Mongrela;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\Users;
use App\Models\Mongrela\Trainer;

use App\Http\Traits\FileTrait;
use Carbon\Carbon;

class UserControllerAPI extends Controller
{ 

    use FileTrait;

    public function login(Request $request){
        $email = $request->email;
        $password = $request->password;

        $data = Users::with('trainer', 'shelter', 'store_data', 'online_pets.shelter', 'online_pets.pet', 'applied_adoption')->where('user_email', $email)->where('user_password', $password)->first();
        if($data){
            $data->user_last_online = Carbon::now()->toDateTimeString();
            $data->save();

            if($data->user_picture)
            $data->user_picture = url('getimage/'.base64_encode($data->user_picture));

            if($data->store_data && $data->store_data->store_picture)
            $data->store_data->store_picture = url('getimage/'.base64_encode($data->store_data->store_picture));

            foreach($data->online_pets as $pet){
                if($pet->pet->sha_picture)
                    $pet->pet->sha_picture = url('getimage/'.base64_encode($pet->pet->sha_picture));
            }

            return $data;
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to save data',
            ]);
        }
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Users::find($request->id) ?? new Users();

            
            if(isset($request->is_register)){
                $email_registered = Users::where('user_email', $inp['user_email'])->first();
                if($email_registered)
                return response()->json([
                    'status' => 'error',
                    'message' => 'user already registered',
                ]);
            }

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            $dbs->save(); 

            if($request->hasFile('user_picture')){
                $file = $request->file('user_picture'); 
                $filename = 'public\\Mongrela\\UserPicture\\'.$dbs->pet_id;
                $path = $this->upload_file($file, $filename);
                $dbs->user_picture = $path;
                $dbs->save(); 
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        $data = Users::with('trainer', 'shelter', 'store_data', 'online_pets.shelter', 'online_pets.pet', 'applied_adoption')->find($id);
        $data->user_last_online = Carbon::now()->toDateTimeString();
        $data->save();
        
        if($data->user_picture)
        $data->user_picture = url('getimage/'.base64_encode($data->user_picture));
        
        if($data->store_data && $data->store_data->store_picture)
        $data->store_data->store_picture = url('getimage/'.base64_encode($data->store_data->store_picture));
        
        // dd($data);
        foreach($data->online_pets as $pet){
            if($pet->pet->sha_picture)
                $pet->pet->sha_picture = url('getimage/'.base64_encode($pet->pet->sha_picture));
        }

        return $data;
    }

    public function delete($id)
    {
        try {
            Users::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function registerAsTrainer(Request $request){
        try {
            $id = $request->id;
            $title = $request->title;
            $description = $request->description;
            
            $user = Users::find($id);
            $trainer = Trainer::firstOrCreate([
                'user_id' => $id
            ]);
            
            $trainer->trainer_title = $title;
            $trainer->trainer_description = $description;
            $trainer->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]); 
    }

    public function trainerList(Request $request){
        $source = $request->user_id;

        $user = Trainer::with('owner')->where('is_approved', 'y')->where('user_id', '!=', $source)->get();
        foreach($user as $item){
            if($item->owner->user_picture)
                $item->owner->user_picture = url('getimage/'.base64_encode($item->owner->user_picture));
        }

        return $user;
    }

    public function getTrainer($id){
        $source = $id;

        $user = Trainer::with('owner', 'trainings')->where('user_id', $source)->first();
        if($user->owner->user_picture)
            $user->owner->user_picture = url('getimage/'.base64_encode($user->owner->user_picture));

        return $user;
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
