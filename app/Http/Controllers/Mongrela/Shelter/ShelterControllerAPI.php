<?php

namespace App\Http\Controllers\Mongrela\Shelter;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\Shelter;

use App\Http\Traits\FileTrait;

class ShelterControllerAPI extends Controller
{ 

    use FileTrait;

    public function list(Request $request){
        $page = $request->page ?? 1;
        $limit = $request->limit ?? 10;
        $offset = ($page - 1) * $limit;
        
        $datas = Shelter::where('is_approved', 'y')->skip($offset)->take($limit)->get();
        foreach($datas as $item){
            if($item->shelter_picture){
                $item->shelter_picture = url('getimage/'.base64_encode($item->shelter_picture));
            }
        }
        
        return $datas;
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Shelter::find($request->id) ?? new Shelter();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            $dbs->save(); 

            if($request->hasFile('shelter_picture')){
                $file = $request->file('shelter_picture'); 
                $filename = 'public\\Mongrela\\ShelterPicture\\'.$dbs->shelter_id;
                $path = $this->upload_file($file, $filename);
                $dbs->shelter_picture = $path;
                $dbs->save(); 
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        $item = Shelter::with(['adoption', 'donation', 'donators' => function($q){
            $q->orderBy('created_at', "DESC");
        } ,'donators.donation_event', 'donators.user', 'gallery', 'volunteer', 'volunteers' => function($q){
            $q->orderBy('created_at', "DESC");
        } ,'volunteers.user', 'volunteers.volunteer_event'])->find($id);

        if($item->shelter_picture){
            $item->shelter_picture = url('getimage/'.base64_encode($item->shelter_picture));
        }
        foreach($item->adoption as $pict){
            $pict->sha_picture = url('getimage/'.base64_encode($pict->sha_picture));
        }
        foreach($item->donation as $pict){
            $pict->shd_picture = url('getimage/'.base64_encode($pict->shd_picture));
        }
        foreach($item->donators as $pict){
            $pict->user->user_picture = url('getimage/'.base64_encode($pict->user->user_picture));
        }
        foreach($item->gallery as $pict){
            $pict->shg_picture = url('getimage/'.base64_encode($pict->shg_picture));
        }
        foreach($item->volunteer as $pict){
            $pict->shv_picture = url('getimage/'.base64_encode($pict->shv_picture));
        }
        foreach($item->volunteers as $pict){
            $pict->user->user_picture = url('getimage/'.base64_encode($pict->user->user_picture));
        }

        return $item;
    }

    public function delete($id)
    {
        try {
            Shelter::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
