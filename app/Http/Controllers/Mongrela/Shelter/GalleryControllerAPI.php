<?php

namespace App\Http\Controllers\Mongrela\Shelter;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\ShelterGallery;

use App\Http\Traits\FileTrait;

class GalleryControllerAPI extends Controller
{ 

    use FileTrait;

    public function list(Request $request){
        $page = $request->page ?? 1;
        $limit = $request->limit ?? 10;
        $offset = ($page - 1) * $limit;
        $shelter_id = $request->shelter_id;

        $datas = ShelterGallery::query();

        if($shelter_id){
            $datas = $datas->where('shelter_id', $shelter_id);
        }

        $datas = $datas->skip($offset)->take($limit)->get();
        foreach($datas as $item){
            if($item->shg_picture){
                $item->shg_picture = url('getimage/'.base64_encode($item->shg_picture));
            }
        }

        return $datas;
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = ShelterGallery::find($request->id) ?? new ShelterGallery();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            $dbs->save(); 

            if($request->hasFile('shg_picture')){
                $file = $request->file('shg_picture'); 
                $filename = 'public\\Mongrela\\ShelterGalleryPicture\\'.$dbs->shg_id;
                $path = $this->upload_file($file, $filename);
                $dbs->shg_picture = $path;
                $dbs->save(); 
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        $item = ShelterGallery::find($id);
        if($item->shg_picture){
            $item->shg_picture = url('getimage/'.base64_encode($item->shg_picture));
        }
        return $item;
    }

    public function delete($id)
    {
        try {
            ShelterGallery::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
