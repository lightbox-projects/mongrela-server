<?php

namespace App\Http\Controllers\Mongrela\Shelter;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\Chat;
use App\Models\Mongrela\ShelterDonation;
use App\Models\Mongrela\ShelterDonationApplicant;

use App\Http\Traits\FileTrait;

class DonationControllerAPI extends Controller
{ 

    use FileTrait;

    public function list(Request $request){
        $page = $request->page ?? 1;
        $limit = $request->limit ?? 10;
        $offset = ($page - 1) * $limit;
        $shelter_id = $request->shelter_id;

        $datas = ShelterDonation::query();

        if($shelter_id){
            $datas = $datas->where('shelter_id', $shelter_id);
        }

        $datas = $datas->skip($offset)->take($limit)->get();
        foreach($datas as $item){
            if($item->shd_picture){
                $item->shd_picture = url('getimage/'.base64_encode($item->shd_picture));
            }
        }

        return $datas;
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = ShelterDonation::find($request->id) ?? new ShelterDonation();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            $dbs->save(); 

            if($request->hasFile('shd_picture')){
                $file = $request->file('shd_picture'); 
                $filename = 'public\\Mongrela\\ShelterDonationPicture\\'.$dbs->shd_id;
                $path = $this->upload_file($file, $filename);
                $dbs->shd_picture = $path;
                $dbs->save(); 
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        $item = ShelterDonation::find($id);
        if($item->shd_picture){
            $item->shd_picture = url('getimage/'.base64_encode($item->shd_picture));
        }
        return $item;
    }

    public function delete($id)
    {
        try {
            ShelterDonation::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

    public function apply_donation(Request $request)
    {
        try {
            $shd_id = $request->shd_id;
            $user_id = $request->user_id;
            $shelter_id = $request->shelter_id;
            $nominal = $request->nominal;

            $dbs = ShelterDonationApplicant::firstOrCreate([
                'user_id' => $user_id, 
                'shd_id' => $shd_id, 
                'shelter_id' => $shelter_id, 
            ]); 
            
            $dbs->nominal = $nominal;
            $dbs->save();

            $donation = ShelterDonation::with('shelter')->find($shd_id);

            $chat = new Chat();
            $chat->user_id = $user_id;
            $chat->chat_user_target = $donation->shelter->user_id;
            $chat->chat_text = 'Donation on '.$donation->shd_title. ' for '
                .$nominal
                .'.';
            $chat->save();

            $total = 0;
            $donations = ShelterDonationApplicant::where('shd_id', $shd_id)->get();
            foreach($donations as $item){
                if($item->nominal){
                    $total += $item->nominal;
                }
            }
            $donation->shd_raised = $total;
            $donation->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }
}
