<?php

namespace App\Http\Controllers\Mongrela\Shelter;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\ShelterVolunteer;
use App\Models\Mongrela\ShelterVolunteerApplicant;

use App\Http\Traits\FileTrait;

class VolunteerControllerAPI extends Controller
{ 

    use FileTrait;

    public function list(Request $request){
        $page = $request->page ?? 1;
        $limit = $request->limit ?? 10;
        $offset = ($page - 1) * $limit;
        $shelter_id = $request->shelter_id;

        $datas = ShelterVolunteer::query();

        if($shelter_id){
            $datas = $datas->where('shelter_id', $shelter_id);
        }

        $datas = $datas->skip($offset)->take($limit)->get();

        foreach($datas as $item){
            if($item->shv_picture){
                $item->shv_picture = url('getimage/'.base64_encode($item->shv_picture));
            }
        }

        return $datas;
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = ShelterVolunteer::find($request->id) ?? new ShelterVolunteer();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            $dbs->save(); 

            if($request->hasFile('shv_picture')){
                $file = $request->file('shv_picture'); 
                $filename = 'public\\Mongrela\\ShelterVolunteerPicture\\'.$dbs->shv_id;
                $path = $this->upload_file($file, $filename);
                $dbs->shv_picture = $path;
                $dbs->save(); 
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        $item = ShelterVolunteer::find($id);
        if($item->shv_picture){
            $item->shv_picture = url('getimage/'.base64_encode($item->shv_picture));
        }
        return $item;
    }

    public function delete($id)
    {
        try {
            ShelterVolunteer::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

    public function apply_volunteer(Request $request)
    {
        try {
            $shv_id = $request->shv_id;
            $user_id = $request->user_id;
            $shelter_id = $request->shelter_id;

            $dbs = ShelterVolunteerApplicant::firstOrCreate([
                'user_id' => $user_id, 
                'shv_id' => $shv_id, 
                'shelter_id' => $shelter_id, 
            ]); 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }
}
