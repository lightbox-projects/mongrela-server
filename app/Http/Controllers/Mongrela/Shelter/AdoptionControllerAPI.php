<?php

namespace App\Http\Controllers\Mongrela\Shelter;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\ShelterAdoption;
use App\Models\Mongrela\ShelterOnlineParents;
use App\Models\Mongrela\ShelterAdopters;
use App\Models\Mongrela\Chat;

use App\Http\Traits\FileTrait;

class AdoptionControllerAPI extends Controller
{ 

    use FileTrait;

    public function list(Request $request){
        $page = $request->page ?? 1;
        $limit = $request->limit ?? 10;
        $offset = ($page - 1) * $limit;

        $shelter_id = $request->shelter_id;
        
        $datas = ShelterAdoption::query();

        if($shelter_id){
            $datas = $datas->where('shelter_id', $shelter_id);
        }

        $datas = $datas->skip($offset)->take($limit)->get();
        foreach($datas as $item){
            if($item->sha_picture){
                $item->sha_picture = url('getimage/'.base64_encode($item->sha_picture));
            }
        }

        return $datas;
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = ShelterAdoption::find($request->id) ?? new ShelterAdoption();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            $dbs->save(); 

            if($request->hasFile('sha_picture')){
                $file = $request->file('sha_picture'); 
                $filename = 'public\\Mongrela\\ShelterAdoptionPicture\\'.$dbs->sha_id;
                $path = $this->upload_file($file, $filename);
                $dbs->sha_picture = $path;
                $dbs->save(); 
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        $item = ShelterAdoption::with('online_parents.user')->find($id);
        if($item->sha_picture){
            $item->sha_picture = url('getimage/'.base64_encode($item->sha_picture));
        }
        foreach($item->online_parents as $user){
            if($user->user_picture){
                $user->user_picture = url('getimage/'.base64_encode($user->user_picture));
            }
        }

        return $item;
    }

    public function delete($id)
    {
        try {
            ShelterAdoption::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

    public function apply_online_parent(Request $request)
    {
        try {
            $sha_id = $request->sha_id;
            $user_id = $request->user_id;
            $shelter_id = $request->shelter_id;
            $nominal = $request->nominal;

            $dbs = ShelterOnlineParents::firstOrCreate([
                'user_id' => $user_id, 
                'sha_id' => $sha_id, 
                'shelter_id' => $shelter_id, 
            ]); 
            
            $dbs->nominal = $nominal;

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }
    
    public function request_adoption(Request $request)
    {
        try {
            $sha_id = $request->sha_id;
            $user_id = $request->user_id;
            $shelter_id = $request->shelter_id;
            $datas = $request->datas;

            $dbs = ShelterAdopters::firstOrCreate([
                'user_id' => $user_id, 
                'sha_id' => $sha_id, 
                'shelter_id' => $shelter_id, 
            ]); 
            
            $dbs->datas = $datas;

            if ($dbs->save()) {

                $adoption = ShelterAdoption::with('shelter')->find($sha_id);

                $chat = new Chat();
                $chat->user_id = $user_id;
                $chat->chat_user_target = $adoption->shelter->user_id;
                $chat->chat_text = 'Request Adoption for '.$adoption->sha_name. '.';
                $chat->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }
}
