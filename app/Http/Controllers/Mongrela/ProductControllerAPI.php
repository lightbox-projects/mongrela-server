<?php

namespace App\Http\Controllers\Mongrela;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\Products;

use App\Http\Traits\FileTrait;

class ProductControllerAPI extends Controller
{ 

    use FileTrait;

    public function list(Request $request){
        $page = $request->page ?? 1;
        $limit = $request->limit ?? 10;
        $offset = ($page - 1) * $limit;
        
        $datas = Products::with('store_data')->whereHas('store_data', function($q){
            $q->where('is_approved', 'y');
        })->skip($offset)->take($limit);
        
        $store_id = $request->store_id;
        if($store_id){
            $datas = $datas->where('store_id', $store_id);
        }

        $datas = $datas->get();
        foreach($datas as $item){
            if($item->pr_picture){
                $item->pr_picture = url('getimage/'.base64_encode($item->pr_picture));
            }
        }

        return $datas;
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Products::find($request->id) ?? new Products();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            $dbs->save(); 

            if($request->hasFile('pr_picture')){
                $file = $request->file('pr_picture'); 
                $filename = 'public\\Mongrela\\ProductPicture\\'.$dbs->pr_id;
                $path = $this->upload_file($file, $filename);
                $dbs->pr_picture = $path;
                $dbs->save(); 
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        return Products::with('store_data')->find($id);
    }

    public function delete($id)
    {
        try {
            Products::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
