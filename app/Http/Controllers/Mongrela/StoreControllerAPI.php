<?php

namespace App\Http\Controllers\Mongrela;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\Store;

use App\Http\Traits\FileTrait;

class StoreControllerAPI extends Controller
{ 

    use FileTrait;

    public function list(Request $request){
        $page = $request->page ?? 1;
        $limit = $request->limit ?? 10;
        $offset = ($page - 1) * $limit;
        
        $datas = Store::with('products')->where('is_approved', 'y')->skip($offset)->take($limit)->get();
        foreach($datas as $item){
            if($item->store_picture){
                $item->store_picture = url('getimage/'.base64_encode($item->store_picture));
            } 
            
            $item->products_d = $item->products->take(4); 

            if($item->products_d)
            foreach($item->products_d as $prod){
                if($prod->pr_picture){
                    $prod->pr_picture = url('getimage/'.base64_encode($prod->pr_picture));
                }
            }
        }
        
        return $datas;
    }

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = Store::find($request->id) ?? new Store();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }  
            $dbs->save(); 

            if($request->hasFile('store_picture')){
                $file = $request->file('store_picture'); 
                $filename = 'public\\Mongrela\\StorePicture\\'.$dbs->store_id;
                $path = $this->upload_file($file, $filename);
                $dbs->store_picture = $path;
                $dbs->save(); 
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getById($id)
    {
        $item = Store::with('owner')->find($id);
        if($item->store_picture){
            $item->store_picture = url('getimage/'.base64_encode($item->store_picture));
        }

        return $item;
    }

    public function delete($id)
    {
        try {
            Store::find($id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success to save data',
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
