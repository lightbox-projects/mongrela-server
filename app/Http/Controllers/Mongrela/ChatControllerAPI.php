<?php

namespace App\Http\Controllers\Mongrela;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTraits;
use Illuminate\Http\Request;

use App\Models\Mongrela\Chat; 
use App\Models\Mongrela\Users; 

use App\Http\Traits\FileTrait;

class ChatControllerAPI extends Controller
{  

    use FileTrait;

    public function save(Request $request)
    {
        try {
            $inp = $request->inp;
            $dbs = new Chat();

            foreach ($inp as $key => $value) {
                if ($value)
                    $dbs[$key] = $value;
            }   

            $dbs->save();

            if($request->hasFile('chat_file')){
                $file = $request->file('chat_file'); 
                $filename = 'public\\Mongrela\\ChatFile\\'.$dbs->chat_id;
                $path = $this->upload_file($file, $filename);
                $dbs->chat_file = $path;
            } 

            if ($dbs->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Success to save data',
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Failed to save data',
        ]);
    }

    public function getChatList(Request $request){
        $source = $request->source;

        $datas = Chat::where('user_id', $source)->pluck('chat_user_target', 'chat_user_target');
        $users = Users::whereIn('user_id', $datas)->get();
        foreach($users as $user){
            if($user->user_picture)
            $user->user_picture = url('getimage/'.base64_encode($user->user_picture));
            
            $target = $user->user_id;
            $last_message = Chat::where(function($q) use($source, $target){
                $q->where('user_id', $source)->where('chat_user_target', $target);
            })->orWhere(function($q) use($source, $target){
                $q->where('user_id', $target)->where('chat_user_target', $source);
            })->orderBy('created_at', 'desc')->first();
            $user->last_message = $last_message;
        }
        
        return $users;
    }

    public function getChatRoom(Request $request)
    {
        $source = $request->source;
        $target = $request->target;

        $target_u = Users::find($target);
        if($target_u->user_picture)
            $target_u->user_picture = url('getimage/'.base64_encode($target_u->user_picture));

        $datas = Chat::with('owner')
        ->where(function($q) use($source, $target){
            $q->where('user_id', $source)->where('chat_user_target', $target);
        })->orWhere(function($q) use($source, $target){
            $q->where('user_id', $target)->where('chat_user_target', $source);
        })->get();
        
        foreach($datas as $chat){
            if($chat->user_id == $source){
                $chat->is_me = true;
            } else {
                $chat->is_me = false;
            }

            if($chat->chat_file){
                $chat->chat_file = url('getimage/'.base64_encode($chat->chat_file));
            }
        }

        return [
            'chats' => $datas,
            'info' => $target_u
        ];

    } 

    //-----------------------------------------------------------------------
    // Custom Function Place HERE !
    //----------------------------------------------------------------------- 

}
