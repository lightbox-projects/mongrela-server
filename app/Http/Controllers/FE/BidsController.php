<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Project;
use App\Models\Bids;

class BidsController extends Controller
{
    public function index()
    {
        $data = [];
        $uid = session()->get('userId');

        $prj_ids = Bids::where('user_id', $uid)->pluck('prj_id', 'prj_id');

        $data['total_active'] = count(Project::whereIn('prj_id', $prj_ids)->orderBy('prj_published_date', 'desc')->get());

        return view('bids.index', $data);
    }  
}
