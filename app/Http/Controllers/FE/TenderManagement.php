<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Project;
use App\Models\ProjectProducts;
use App\Models\Masterdata\MsUsers;
use App\Models\Masterdata\Settings;
use App\Models\Masterdata\Category;
use App\Models\Masterdata\Tags;

class TenderManagement extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        $uid = session()->get('userId');
        $user = MsUsers::with('company')->find($uid);
        $data['company'] = $user->company;
        return view('tender-management.list', $data);
    }

    public function form(Request $request, $id){
        $data = [];
        $data['id'] = $id;
        $raw = Project::with('products.product.original.owner')->find($id);
        $data['project'] = $raw;

        $data['category'] = Category::all();
        $data['tags'] = Tags::all();
        return view('tender-management.form', $data);
    }
    
    public function new(Request $request){ 
        $data = new Project(); 
        $data->save();
        $data['id'] = $data->prj_id;
        $data['new'] = true;
        
        $data['category'] = Category::all();
        $data['tags'] = Tags::all();
        return view('tender-management.form', $data);
    }

    public function products($id){ 
        $raw = Project::with('products.product.original.owner')->find($id);
        $data['id'] = $id;
        $data['project'] = $raw;
        $data['products'] = $raw->products;
        return view('tender-management.products', $data);
    }

    public function publish($id){ 
        $raw = Project::with('products')->find($id);
        $data['id'] = $id; 
        $data['raw'] = $raw;
        $data['project'] = $raw;
        $data['terms_and_condition'] = Settings::where('setting_key', 'terms_and_condition')->first();
        return view('tender-management.publish', $data);
    }

    public function bids($id){ 
        $raw = Project::with('bids.owner')->find($id);
        $data['id'] = $id; 
        $data['project'] = $raw;
        return view('tender-management.bids', $data);
    }

    public function evaluation($id, $pr_id = null){ 
        $raw = Project::with('bids.owner', 'bids.products', 'owner.company', 'products.product')->find($id);
        $raw->bids = $raw->bids->where('bid_status', 'submited');
        $data['id'] = $id; 
        $data['project'] = $raw;

        if(!$pr_id)
            $active_product = $data['project']->products->first()->product ?? null;
        else 
            $active_product = $data['project']->products->where('pr_id', $pr_id)->first()->product ?? null;
            
        $data['pr_id'] = $active_product->pr_id;

        $idx = 0;
        foreach($data['project']->products as $key => $item){
            if($item->pr_id == $data['pr_id']) $idx = $key+1;
        }

        $data['next'] = null;
        if(isset($data['project']->products[$idx]))
            $data['next'] = $data['project']->products[$idx]->pr_id;

        $data['active_product'] = $active_product;

        return view('tender-management.evaluation', $data);
    }

    public function final($id){ 
        $raw = Project::with('bids.owner', 'winner_bid.owner')->find($id);
        $raw->bids = $raw->bids->where('bid_status', 'submited');
        $data['id'] = $id; 
        $data['project'] = $raw;
        return view('tender-management.final', $data);
    }

}
