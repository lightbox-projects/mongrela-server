<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Project;
use App\Models\Masterdata\Testimony;
use App\Models\Masterdata\Category;

class LoginController extends Controller
{
    public function index()
    {
        // if (session()->get('login')) {
        //     return redirect(url('/users'));
        // } else {
        //     return redirect(url('/login'));
        // }

        $data['latest'] = Project::with('owner.company','category')->where('prj_visibility', 'public')->orderBy('prj_published_date', 'desc')->limit(3)->get();
        $data['testimony'] = Testimony::all();
        $data['categories'] = Category::all();

        return view('home.index', $data);
    }

    public function login()
    {
        return view('auth.login');
    }

    public function register(){
        return view('auth.register');
    }
}
