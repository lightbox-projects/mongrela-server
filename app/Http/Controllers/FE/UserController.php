<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Masterdata\MsRoles;
use App\Models\Masterdata\MsUsers;

class UserController extends Controller
{
    public function index()
    {
        $data['roles'] = MsRoles::with('users')->get();
        return view('masterdata.users.index', $data);
    } 

    public function profile($id = null){
        if(!$id){
            $id = session()->get('userId');
        }

        $data['id'] = $id; 
        $data['user'] = MsUsers::with('company')->find($id);
        return view('masterdata.users.form', $data);
    }

    public function company($id = null){
        if(!$id){
            $id = session()->get('userId');
        }

        $data['id'] = $id;
        $data['user'] = MsUsers::with('company')->find($id);
        return view('masterdata.users.form-company', $data);
    }
}
