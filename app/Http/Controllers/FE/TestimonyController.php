<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Testimony;

class TestimonyController extends Controller
{
    public function index()
    {
        $data = []; 
        return view('masterdata.testimony.index', $data);
    }
}
