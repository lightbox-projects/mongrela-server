<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Products as ProductModel;
use App\Models\Masterdata\MsUsers;
use App\Models\Masterdata\ProductCategory;

class Products extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        $uid = session()->get('userId');
        $user = MsUsers::with('company')->find($uid);
        $data['company'] = $user->company;
        return view('products.list', $data);
    }

    public function form(Request $request, $id){
        $data = [];
        $data['id'] = $id;

        $data['category'] = ProductCategory::all();
        return view('products.form', $data);
    }
    
    public function new(Request $request){ 
        $data = new ProductModel(); 
        $data->save();
        $data['id'] = $data->pr_id;
        $data['new'] = true;
        
        $data['category'] = ProductCategory::all();
        return view('products.form', $data);
    }
}
