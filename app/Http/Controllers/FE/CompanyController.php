<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Masterdata\MsCompany;
use App\Models\News;
use App\Models\Masterdata\Testimony;
use App\Models\Project;

class CompanyController extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        $data['companies'] = MsCompany::all();
        $data['banner'] = News::all();
        $data['testimony'] = Testimony::all();

        $data['total_active'] = count(MsCompany::orderBy('company_id', 'desc')->get());

        $data['request'] = $request->query->all();

        return view('companies.index', $data);
    }  
    
    public function detail($id){
        $data = [];
        $data['id'] = $id;
        $data['company'] = MsCompany::where('company_id', $id)->first();
        $data['latest'] = Project::with('owner.company','category')->where('user_id', $data['company']->user_id)->where('prj_visibility', 'public')->orderBy('prj_published_date', 'desc')->limit(3)->get();

        $data['next'] = MsCompany::where('company_id', '>', $data['company']->company_id)->min('company_id');

        return view('companies.detail', $data);
    }

    public function getList(Request $request){
        $data = MsCompany::orderBy('company_id', 'DESC')->offset(($request->page-1) * $request->pageRange)
                ->limit($request->pageRange);

        if($request->location) {
        }
        if($request->speciality) {
        }
        if($request->tags) {
        }

        return $data->get();
    }
}
