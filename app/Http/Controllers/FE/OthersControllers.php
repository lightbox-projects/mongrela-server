<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Faq;

class OthersControllers extends Controller
{
    public function about()
    {
        return view('others.about-us');
    }  
    public function faq()
    {
        $data['faq'] = Faq::all();
        return view('others.faq', $data);
    }  
}
