<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ProductCategory;

class ProductCategoryController extends Controller
{
    public function index()
    {
        $data = []; 
        return view('masterdata.product-category.index', $data);
    }
}
