<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\News;

class NewsController extends Controller
{
    public function index()
    {
        $data = [];
        $data['news'] = News::all();
        return view('news.index', $data);
    }  

    public function list()
    {
        $data = [];
        return view('news.list', $data);
    }

    public function detail($id)
    {
        $data = [];
        $data['news'] = News::find($id);
        return view('news.detail', $data);
    }   
}
