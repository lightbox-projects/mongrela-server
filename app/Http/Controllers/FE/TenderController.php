<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Masterdata\MsCompany;
use App\Models\Project;
use App\Models\Masterdata\Category;
use App\Models\Masterdata\Testimony;
use App\Models\Masterdata\Settings;

class TenderController extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        $data['latest'] = Project::where('prj_visibility', 'public')->orderBy('prj_published_date', 'desc')->limit(3)->get();
        $data['total_active'] = count(Project::where('prj_visibility', 'public')->orderBy('prj_published_date', 'desc')->offset(3)->get());
        $data['testimony'] = Testimony::all();
        $data['categories'] = Category::all();

        $data['request'] = $request->query->all();

        return view('tenders.index', $data);
    }  
    
    public function detail($id){
        $data = [];
        $data['id'] = $id;
        $data['project'] = Project::with('owner.company', 'products.product','category')->find($id);
        $data['latest'] = Project::where('prj_visibility', 'public')->orderBy('prj_published_date', 'desc')->limit(3)->get();

        $data['company'] = $data['project']->owner->company;
        $data['next'] = Project::where('prj_id', '>', $data['project']->prj_id)->min('prj_id');
        $data['terms_and_condition'] = Settings::where('setting_key', 'terms_and_condition')->first();

        return view('tenders.detail', $data);
    }
}
