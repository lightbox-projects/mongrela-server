<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Project;
use App\Models\ProductProjectAssigned;
use App\Models\BidProduct;
use App\Models\Bids;
use App\Models\ProjectProducts;
use App\Models\Masterdata\Settings;

class BiddingController extends Controller
{
    public function index($id, $pr_id = null)
    {
        $data = [];
        $data['id'] = $id;
        $data['project'] = Project::with('owner.company', 'products.product', 'bids', 'category')->find($id);
        $data['terms_and_condition'] = Settings::where('setting_key', 'terms_and_condition')->first();

        $uid = session()->get('userId');

        $bid = Bids::with('products')->firstOrCreate([
            'user_id' => $uid,
            'prj_id' => $id
        ]);

        $data['bid_products'] = $bid->products->pluck('pr_id_original');

        if(!$pr_id)
            $active_product = $data['project']->products->first()->product ?? null;
        else 
            $active_product = $data['project']->products->where('pr_id',$pr_id)->first()->product ?? null;
            
        $data['pr_id'] = $active_product->pr_id;

        $idx = 0;
        foreach($data['project']->products as $key => $item){
            if($item->pr_id == $data['pr_id']) $idx = $key+1;
        }

        $data['next'] = null;
        if(isset($data['project']->products[$idx]))
            $data['next'] = $data['project']->products[$idx]->pr_id;


        $data['bid'] = $bid;
        $data['active_product'] = $active_product;
        

        return view('bidding.index', $data);
    }  

    public function saveF(Request $request){
        $pr_id = $request->pr_id_original;
        $bid_id = $request->bid_id;
        $bidder_id = session()->get('userId');

        $bid = Bids::firstOrCreate([
            'user_id' => $bidder_id,
            'prj_id' => $bid_id
        ]);

        $product = ProductProjectAssigned::where('pr_id', $pr_id)->first();

        $bid_product = BidProduct::firstOrCreate([
            'bidder_id' => $bidder_id,
            'bid_id' => $bid->bid_id,
            'pr_id_original' => $pr_id,
        ]);

        foreach($product->toArray() as $key => $val){
            if($key != 'pr_id' && $key != 'pr_id_original')
            $bid_product[$key] = $val;
        }
        
        $bid_product->type_specification = $request->type_specification;
        $inp = $request->inp;
        foreach($inp as $key => $val){
            $bid_product[$key] = $val;
        }

        $bid_product->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Data successfully saved',
        ]);
    }

    public function getData($id, $pr_id){
        $bidder_id = session()->get('userId');

        $bid = Bids::firstOrCreate([
            'user_id' => $bidder_id,
            'prj_id' => $id
        ]);

        $bid_product = BidProduct::where('bidder_id', $bidder_id)->where("bid_id", $bid->bid_id)->where('pr_id_original', $pr_id)->first();

        return $bid_product;
    }
    
    public function getDataOriginal($id, $pr_id){ 
        $bid_product = ProjectProducts::with('product')->where('pr_id', $pr_id)->where("prj_id", $id)->first();

        return $bid_product->product ?? null;
    }

    public function submitBid($id){
        $bidder_id = session()->get('userId');
        $bid = Bids::where('user_id', $bidder_id)->where('prj_id', $id)->first();

        $bid->bid_status = 'submited';
        $bid->submited_at = date('Y-m-d');
        $bid->save();
        
        return response()->json([
            'status' => 'success',
            'message' => 'Data successfully saved',
        ]);
    }

    public function viewBid($id, $uid, $pr_id = null)
    {
        $data = [];
        $data['id'] = $id;
        $data['uid'] = $uid;
        $data['project'] = Project::with('owner.company', 'products.product', 'bids')->find($id);
        $data['terms_and_condition'] = Settings::where('setting_key', 'terms_and_condition')->first();

        $bid = Bids::with('owner')->where('user_id', $uid)->where('prj_id', $id)->first();

        $data['bid_products'] = $bid->products->pluck('pr_id_original');

        if(!$pr_id)
            $active_product = $data['project']->products->first()->product ?? null;
        else 
            $active_product = $data['project']->products->where('pr_id',$pr_id)->first()->product ?? null;
            
        $data['pr_id'] = $active_product->pr_id;

        $idx = 0;
        foreach($data['project']->products as $key => $item){
            if($item->pr_id == $data['pr_id']) $idx = $key+1;
        }

        $data['next'] = null;
        if(isset($data['project']->products[$idx]))
            $data['next'] = $data['project']->products[$idx]->pr_id;


        $data['bid'] = $bid;
        $data['active_product'] = $active_product;
        $data['is_owner'] = true;

        return view('bidding.index', $data);
    }  
}
