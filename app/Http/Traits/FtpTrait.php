<?php

namespace App\Http\Traits;
use Illuminate\Support\Facades\Http;

trait FtpTrait
{
    public function ftp_upload($request, $path, $filename)
    {
        $file     = $request;
        $connect  = $this->connect();
        $fullpath = env('FTP_BASE_DIR').'/'.$path;
        $upload_file = $fullpath.'/'.$filename;
        $source_file = $file->getPathName();

        if($connect->status)
        {
            //check directory exist, if no mkdir
            if (ftp_nlist($connect->id, $fullpath) === false) {
                $this->makedirectory($connect->id, $path);
            }

            // upload the file
            $upload = ftp_put($connect->id, $upload_file, $source_file, FTP_BINARY); //var_dump($upload); die();

            // check upload status
            if (!$upload) {
                $response = ['status' => false, 'message' => 'There was a problem while uploading file to FTP Server!'];
            } else {
                $response = ['status' => true, 'file' => $filename, 'path' => $fullpath, 'path_filename' => $upload_file];
            }

            ftp_close($connect->id);

            return (object) $response;
        }
        else
        {
            return $connect;
        }
    }

    public function ftp_get_url($path_filename)
    {
        return env('FTP_URL').'/'.$path_filename;
    }

    public static function ftp_get_url_static($path_filename)
    {
        return env('FTP_URL').'/'.$path_filename;
    }

    private function connect()
    {
        $ftp_server    = env('FTP_HOST', '');
        $ftp_user_name = env('FTP_USERNAME', '');
        $ftp_user_pass = env('FTP_PASSWORD', '');
        $ftp_port      = '21';

        try {
            // set up basic connection
            $conn_id = ftp_connect($ftp_server, $ftp_port);

            // login with username and password
            $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

            // ftp passive cmd
            ftp_pasv($conn_id, true);

            // check connection
            if ((!$conn_id) || (!$login_result)) {
                $response = ['status' => false, 'message' => 'FTP connection has failed!'];
            } else {
                $response = ['status' => true, 'id' => $conn_id];
            }

            return (object) $response;
        } catch (\Exception $e) {
            $response = ['status' => false, 'message' => "Failure: " . $e->getMessage()];
            return (object) $response;
        }
    }

    private function makedirectory($connection, $path)
    {
        $basepath = env('FTP_BASE_DIR');

        @ftp_chdir($connection, $basepath);

        $paths = explode('/', $path); // 2013/06/11/username
        foreach ($paths as $p) {
            if (!@ftp_chdir($connection, $p)) {
                ftp_mkdir($connection, $p);
                ftp_chdir($connection, $p);
                //ftp_chmod($ftpcon, 0777, $part);
            }
        }
    }
}
?>
