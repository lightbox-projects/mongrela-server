<?php

namespace App\Http\Traits;
use Illuminate\Support\Facades\Http;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

trait FileTrait
{
    public function upload_file($file, $path){
        $extension = $file->getClientOriginalExtension();
        $path = $path.'.'.$extension;
        \Storage::disk('local')->put($path, file_get_contents($file));
        return $path;
    }
}