<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Users;
use App\Models\Mongrela\Products;

class Store extends Model
{
    protected $table = "mongrela.store";
    protected $primaryKey = 'store_id';
    public $timestamps = false;

    public function owner(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    } 

    public function products(){
        return $this->hasMany(Products::class, 'store_id', 'store_id');
    }
}
