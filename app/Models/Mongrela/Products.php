<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Users;
use App\Models\Mongrela\Store;

class Products extends Model
{
    protected $table = "mongrela.products";
    protected $primaryKey = 'pr_id';
    public $timestamps = false;
    
    public function owner(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    } 

    public function store_data(){
        return $this->belongsTo(Store::class, 'store_id', 'store_id');
    } 
}
