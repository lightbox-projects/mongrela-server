<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\ShelterOnlineParents;

class ShelterAdoption extends Model
{
    protected $table = "mongrela.shelter_adoption";
    protected $primaryKey = 'sha_id';
    public $timestamps = false;

    public function shelter(){
        return $this->belongsTo(Shelter::class, 'shelter_id', 'shelter_id');
    } 

    public function online_parents(){
        return $this->hasMany(ShelterOnlineParents::class, 'sha_id', 'sha_id');
    }
    
}
