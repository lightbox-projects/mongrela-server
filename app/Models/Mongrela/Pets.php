<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Users;
use App\Models\Mongrela\WalkerPetsAvailable;

class Pets extends Model
{
    protected $table = "mongrela.pets";
    protected $primaryKey = 'pet_id';
    public $timestamps = false;
    
    public function owner(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    } 

    public function walker(){
        return $this->belongsTo(WalkerPetsAvailable::class, 'pet_id', 'pet_id');
    }
}
