<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\ShelterDonation;
use App\Models\Mongrela\Users;

class ShelterDonationApplicant extends Model
{
    protected $table = "mongrela.shelter_donation_applicant";
    protected $primaryKey = 'shda_id';
    // public $timestamps = false;
    protected $guarded = ['shda_id'];

    public function donation_event(){
        return $this->belongsTo(ShelterDonation::class, 'shd_id', 'shd_id');
    }
    public function user(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    }
}
