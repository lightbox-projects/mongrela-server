<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Users;
use App\Models\Mongrela\Training;

class TrainingSchedule extends Model
{
    protected $table = "mongrela.training_schedule";
    protected $primaryKey = 'trs_id'; 
    public $timestamps = false;

    public function training(){
        return $this->belongsTo(Training::class, 'tr_id', 'tr_id');
    } 
}
