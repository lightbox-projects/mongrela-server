<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Users;
use App\Models\Mongrela\Training;

class Trainer extends Model
{
    protected $table = "mongrela.trainer";
    protected $primaryKey = 'trainer_id';
    protected $guarded = ['trainer_id'];
    public $timestamps = false;

    public function owner(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    } 

    public function trainings(){
        return $this->hasMany(Training::class, 'user_id', 'user_id');
    } 
}
