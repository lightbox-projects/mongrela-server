<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

class WalkerNote extends Model
{
    protected $table = "mongrela.walker_note";
    protected $primaryKey = 'wn_id';
    public $timestamps = false;
    
}
