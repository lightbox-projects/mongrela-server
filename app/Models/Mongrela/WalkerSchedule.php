<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\WalkerPetsAvailable;

class WalkerSchedule extends Model
{
    protected $table = "mongrela.walker_schedule";
    protected $primaryKey = 'ws_id';
    public $timestamps = false;
    
    public function walker(){
        return $this->belongsTo(WalkerPetsAvailable::class, 'wpa_id', 'wpa_id');
    } 
}
