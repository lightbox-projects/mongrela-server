<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;
use App\Models\Mongrela\ShelterAdoption;
use App\Models\Mongrela\Shelter;
use App\Models\Mongrela\Users;

class ShelterOnlineParents extends Model
{
    protected $table = "mongrela.shelter_online_parents";
    protected $primaryKey = 'sop_id';
    public $timestamps = false;
    protected $guarded = ['sop_id'];

    public function pet(){
        return $this->belongsTo(ShelterAdoption::class, 'sha_id', 'sha_id');
    }

    public function shelter(){
        return $this->belongsTo(Shelter::class, 'shelter_id', 'shelter_id');
    }

    public function user(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    }
}
