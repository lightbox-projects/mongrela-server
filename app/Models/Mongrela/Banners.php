<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    protected $table = "mongrela.banners";
    protected $primaryKey = 'banner_id';
    
}
