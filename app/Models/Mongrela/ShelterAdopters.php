<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Shelter;
use App\Models\Mongrela\Users;

class ShelterAdopters extends Model
{
    protected $table = "mongrela.shelter_adopters";
    protected $primaryKey = 'shad_id';
    public $timestamps = false;
    protected $guarded = ['shad_id'];
    
    public function shelter(){
        return $this->belongsTo(Shelter::class, 'shelter_id', 'shelter_id');
    } 
    
    public function user(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    } 
}
