<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Users;
use App\Models\Mongrela\TrainingSchedule;

class Training extends Model
{
    protected $table = "mongrela.training";
    protected $primaryKey = 'tr_id';
    public $timestamps = false;

    public function owner(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    } 

    public function schedules(){
        return $this->hasMany(TrainingSchedule::class, 'tr_id', 'tr_id');
    }
}
