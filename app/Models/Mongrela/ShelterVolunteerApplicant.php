<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\ShelterVolunteer;
use App\Models\Mongrela\Users;

class ShelterVolunteerApplicant extends Model
{
    protected $table = "mongrela.shelter_volunteer_applicant";
    protected $primaryKey = 'shva_id';
    // public $timestamps = false;
    protected $guarded = ['shva_id'];

    public function volunteer_event(){
        return $this->belongsTo(ShelterVolunteer::class, 'shv_id', 'shv_id');
    }
    public function user(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    }
}
