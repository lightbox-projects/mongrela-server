<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\WalkerPetsAvailable;
use App\Models\Mongrela\WalkerSchedule;
use App\Models\Mongrela\Users;

class WalkerBooked extends Model
{
    protected $table = "mongrela.walker_booked";
    protected $primaryKey = 'wb_id';
    public $timestamps = false;
    
    public function booked_by(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    } 
    
    public function walker(){
        return $this->belongsTo(WalkerPetsAvailable::class, 'wpa_id', 'wpa_id');
    } 
    
    public function schedule(){
        return $this->belongsTo(WalkerSchedule::class, 'ws_id', 'ws_id');
    }  
}
