<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Pets;
use App\Models\Mongrela\WalkerNote;
use App\Models\Mongrela\WalkerSchedule;

class WalkerPetsAvailable extends Model
{
    protected $table = "mongrela.walker_pets_available";
    protected $primaryKey = 'wpa_id';
    public $timestamps = false;
    protected $guarded = ['wpa_id'];
    
    public function pet(){
        return $this->belongsTo(Pets::class, 'pet_id', 'pet_id');
    } 
    
    public function note(){
        return $this->hasMany(WalkerNote::class, 'wpa_id', 'wpa_id');
    } 

    public function schedules(){
        return $this->hasMany(WalkerSchedule::class, 'wpa_id', 'wpa_id');
    }
}
