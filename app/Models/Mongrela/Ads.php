<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $table = "mongrela.sponsor_ads";
    protected $primaryKey = 'ads_id';
    
}
