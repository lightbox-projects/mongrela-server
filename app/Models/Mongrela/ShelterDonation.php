<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Shelter;
use App\Models\Mongrela\ShelterDonationApplicant;

class ShelterDonation extends Model
{
    protected $table = "mongrela.shelter_donation";
    protected $primaryKey = 'shd_id';
    public $timestamps = false;
    
    public function shelter(){
        return $this->belongsTo(Shelter::class, 'shelter_id', 'shelter_id');
    }

    public function donators(){
        return $this->hasMany(ShelterDonationApplicant::class, 'shd_id', 'shd_id');
    }
}
