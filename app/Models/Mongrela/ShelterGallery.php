<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

class ShelterGallery extends Model
{
    protected $table = "mongrela.shelter_gallery";
    protected $primaryKey = 'shg_id';
    public $timestamps = false;
    
}
