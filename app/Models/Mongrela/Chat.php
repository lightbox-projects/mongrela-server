<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Users;

class Chat extends Model
{
    protected $table = "mongrela.chat";
    protected $primaryKey = 'chat_id';
    
    public function owner(){
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    } 
}
