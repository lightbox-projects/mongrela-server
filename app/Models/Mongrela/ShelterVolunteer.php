<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Shelter;
use App\Models\Mongrela\ShelterVolunteerApplicant;

class ShelterVolunteer extends Model
{
    protected $table = "mongrela.shelter_volunteer";
    protected $primaryKey = 'shv_id';
    public $timestamps = false;

    public function shelter(){
        return $this->belongsTo(Shelter::class, 'shelter_id', 'shelter_id');
    }

    public function volunteers(){
        return $this->hasMany(ShelterVolunteerApplicant::class, 'shv_id', 'shv_id');
    }
    
}
