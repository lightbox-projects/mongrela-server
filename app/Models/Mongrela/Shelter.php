<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\ShelterAdoption;
use App\Models\Mongrela\ShelterDonation;
use App\Models\Mongrela\ShelterDonationApplicant;
use App\Models\Mongrela\ShelterGallery;
use App\Models\Mongrela\ShelterVolunteer;
use App\Models\Mongrela\ShelterVolunteerApplicant;

class Shelter extends Model
{
    protected $table = "mongrela.shelter";
    protected $primaryKey = 'shelter_id';
    public $timestamps = false;
    
    public function adoption(){
        return $this->hasMany(ShelterAdoption::class, 'shelter_id', 'shelter_id');
    }
    
    public function donation(){
        return $this->hasMany(ShelterDonation::class, 'shelter_id', 'shelter_id');
    }

    public function donators(){
        return $this->hasMany(ShelterDonationApplicant::class, 'shelter_id', 'shelter_id');
    }
    
    public function gallery(){
        return $this->hasMany(ShelterGallery::class, 'shelter_id', 'shelter_id');
    }
    
    public function volunteer(){
        return $this->hasMany(ShelterVolunteer::class, 'shelter_id', 'shelter_id');
    }

    public function volunteers(){
        return $this->hasMany(ShelterVolunteerApplicant::class, 'shelter_id', 'shelter_id');
    }
}
