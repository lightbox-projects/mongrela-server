<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Users;
use App\Models\Mongrela\TrainingSchedule;
use App\Models\Mongrela\Pets;

class TrainingBooked extends Model
{
    protected $table = "mongrela.training_booked";
    protected $primaryKey = 'trb_id';
    public $timestamps = false; 
    
    public function schedule(){
        return $this->belongsTo(TrainingSchedule::class, 'trs_id', 'trs_id');
    } 

    public function pet(){
        return $this->belongsTo(Pets::class, 'trb_pet_id', 'pet_id');
    } 
}
