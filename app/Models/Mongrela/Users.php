<?php

namespace App\Models\Mongrela;

use Illuminate\Database\Eloquent\Model;

use App\Models\Mongrela\Products;
use App\Models\Mongrela\Pets;
use App\Models\Mongrela\Trainer;
use App\Models\Mongrela\Training;
use App\Models\Mongrela\Shelter;
use App\Models\Mongrela\ShelterOnlineParents;
use App\Models\Mongrela\ShelterAdopters;
use App\Models\Mongrela\Store;

class Users extends Model
{
    protected $table = "mongrela.users";
    protected $primaryKey = 'user_id';
    public $timestamps = false;  

    public function products(){
        return $this->hasMany(Products::class, 'user_id', 'user_id');
    } 

    public function pets(){
        return $this->hasMany(Pets::class, 'user_id', 'user_id');
    } 

    public function trainer(){
        return $this->belongsTo(Trainer::class, 'user_id', 'user_id');
    } 

    public function trainings(){
        return $this->hasMany(Training::class, 'user_id', 'user_id');
    } 

    public function shelter(){
        return $this->belongsTo(Shelter::class, 'user_id', 'user_id');
    }

    public function online_pets(){
        return $this->hasMany(ShelterOnlineParents::class, 'user_id', 'user_id');
    } 

    public function applied_adoption(){
        return $this->hasMany(ShelterAdopters::class, 'user_id', 'user_id');
    }

    public function store_data(){
        return $this->belongsTo(Store::class, 'user_id', 'user_id');
    }
    
}
