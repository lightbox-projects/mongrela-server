<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{
    protected $connection = 'master-schema';
    protected $table = "testimony";
    protected $primaryKey = 'testimony_id';
    public $timestamps = true;
}
