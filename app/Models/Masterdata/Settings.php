<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $connection = 'master-schema';
    protected $table = "settings";
    protected $primaryKey = 'setting_id';
    public $timestamps = false;

}
