<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;

use App\Models\Masterdata\MsUsers;

class MsCompany extends Model
{
    protected $connection = 'master-schema';
    protected $table = "ms_company";
    protected $primaryKey = 'company_id';
    public $timestamps = false;

    public function owner(){
        return $this->belongsTo(MsUsers::class, 'user_id', 'user_id');
    }
}
