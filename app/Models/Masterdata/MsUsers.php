<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use App\Models\Masterdata\MsRoles;
use App\Models\Masterdata\MsCompany;

class MsUsers extends Model
{
    protected $connection = 'master-schema';
    protected $table = "ms_users";
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public function role(){
        return $this->belongsTo(MsRoles::class, 'user_role', 'role_code');
    }

    public function company(){
        return $this->hasOne(MsCompany::class, 'user_id', 'user_id');
    }

}
