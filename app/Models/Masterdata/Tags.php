<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $connection = 'master-schema';
    protected $table = "ms_tags";
    protected $primaryKey = 'tag_id';
    public $timestamps = true;

    protected $guarded = ['tag_id']; 

}
