<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $connection = 'master-schema';
    protected $table = "ms_product_category";
    protected $primaryKey = 'pr_category_id';
    public $timestamps = true;
}
