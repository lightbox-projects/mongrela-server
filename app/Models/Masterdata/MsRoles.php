<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;
use App\Models\Masterdata\MsUsers;

class MsRoles extends Model
{
    protected $connection = 'master-schema';
    protected $table = "ms_roles";
    protected $primaryKey = 'role_id';
    public $timestamps = false;

    public function users(){
        return $this->hasMany(MsUsers::class, 'user_role', 'role_code');
    }
}
