<?php

namespace App\Models\Masterdata;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $connection = 'master-schema';
    protected $table = "ms_category";
    protected $primaryKey = 'category_id';
    public $timestamps = true;
}
