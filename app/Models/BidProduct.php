<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Masterdata\MsUsers; 

class BidProduct extends Model
{
    protected $table = "bid_product";
    protected $primaryKey = 'pr_id';
    public $timestamps = false; 
    protected $guarded = ['pr_id']; 

    public function owner(){
        return $this->belongsTo(MsUsers::class, 'user_id', 'user_id');
    } 
}
