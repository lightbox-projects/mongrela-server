<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\ProductProjectAssigned; 

class ProjectProducts extends Model
{
    protected $table = "project_products";
    protected $primaryKey = 'pjp_id';
    public $timestamps = false;

    public function product(){
        return $this->belongsTo(ProductProjectAssigned::class, 'pr_id', 'pr_id');
    } 
}
