<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\ProjectProducts; 
use App\Models\Bids; 
use App\Models\Masterdata\MsUsers; 
use App\Models\Masterdata\Category; 

use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $table = "project";
    protected $primaryKey = 'prj_id';
    public $timestamps = false;

    public function bids(){
        return $this->hasMany(Bids::class, 'prj_id', 'prj_id');
    }  

    public function winner_bid(){
        return $this->belongsTo(Bids::class, 'winner_bid_id', 'bid_id');
    }  

    public function category(){
        return $this->belongsTo(Category::class, 'prj_category', 'category_id');
    }  

    public function products(){
        return $this->hasMany(ProjectProducts::class, 'prj_id', 'prj_id');
    }  

    public function owner(){
        return $this->belongsTo(MsUsers::class, 'user_id', 'user_id');
    } 
}
