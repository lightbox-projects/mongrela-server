<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Masterdata\MsUsers; 

class Products extends Model
{
    protected $table = "product";
    protected $primaryKey = 'pr_id';
    public $timestamps = false; 

    public function owner(){
        return $this->belongsTo(MsUsers::class, 'user_id', 'user_id');
    } 
}
