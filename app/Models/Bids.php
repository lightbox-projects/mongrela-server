<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Masterdata\MsUsers; 
use App\Models\Project; 
use App\Models\BidProduct; 

class Bids extends Model
{
    use SoftDeletes;

    protected $table = "bids";
    protected $primaryKey = 'bid_id';
    public $timestamps = true; 
    protected $guarded = ['bid_id']; 

    public function owner(){
        return $this->belongsTo(MsUsers::class, 'user_id', 'user_id');
    } 

    public function project(){
        return $this->belongsTo(Project::class, 'prj_id', 'prj_id');
    } 

    public function products(){
        return $this->hasMany(BidProduct::class, 'bid_id', 'bid_id');
    }  
}
