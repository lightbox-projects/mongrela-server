<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Products;

class ProductProjectAssigned extends Model
{
    protected $table = "product_project_assigned";
    protected $primaryKey = 'pr_id';
    public $timestamps = false;

    protected $guarded = ['pr_id']; 

    public function original(){
        return $this->belongsTo(Products::class, 'pr_id_original', 'pr_id');
    } 
}
